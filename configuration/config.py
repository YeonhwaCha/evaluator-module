import json
import yaml

class Config:
    def __init__(self, config_filepath):
        with open(config_filepath) as jf:
            cfg = json.load(jf)
        
        self.rotation_rpy           = [ 0.00, -0.80, 0.00]   # deg
        self.position               = [-1.80,  0.00, 1.22]   # meter
        self.fx                     = 549.33
        self.fy                     = 549.33
        self.cx                     = 255.72
        self.cy                     = 136.80

        def meta_constructor(loader, node):
            value = loader.construct_mapping(node)
            return value

        with open('/home/yeonhwa/phantom/phantom-data-pipeline/evaluator-module/configuration/WestCoast/front_center@genesis_1512.yaml', 'r') as file:
            yaml.add_constructor(u'tag:yaml.org,2002:opencv-matrix', meta_constructor)
            self.fc_distortion = yaml.load(file)

        # Camera topic of interest
        # How can I share this configuration efficiently.
        self.fl_topic               = '/csi_cam/front_left/image_raw'     # Default front left topic
        self.fc_topic               = '/csi_cam/front_center/image_raw'   # Default front center topic
        self.fr_topic               = '/csi_cam/front_right/image_raw'    # Default front right topic 
        self.rl_topic               = '/csi_cam/rear_left/image_raw'      # Default rear left topic   
        self.rc_topic               = '/csi_cam/rear_center/image_raw'    # Default rear center topic 
        self.rr_topic               = '/csi_cam/rear_right/image_raw'     # Default rear right topic
        
        self.vehicle_name           = cfg['vehicle_name']
        self.camera_name            = cfg['camera_name']
        self.input_img_size         = cfg['input_img_size']
        self.train_img_size         = cfg['train_img_size']
        self.annotation_path        = cfg['annotation_path']
        self.master_json_path       = cfg['master_json_path']
        self.simulation_target_date = cfg['simulation_target_date']

        self.lane_reference         = cfg['reference_path']['lane']
        self.object_reference       = cfg['reference_path']['object']
        self.point_cloud            = cfg['reference_path']['point_cloud']
        self.simulation             = cfg['reference_path']['simulation']

        self.center_interest_lines  = cfg['center_interest_lines']
        self.left_interest_lines    = cfg['left_interest_lines']
        self.right_interest_lines   = cfg['right_interest_lines']
 
        self.calibraion_yaml_file   = cfg['calibraion_yaml_file'] 
        
        with open(cfg['crop_matrix_front']) as f:
            self.front_center_crop_cam_matrix = json.loads(str(f.read()).replace('.,', ','))['cameraMatrix']
        with open(cfg['crop_matrix_rear']) as f:
            self.rear_center_crop_cam_matrix = json.loads(str(f.read()).replace('.,', ','))['cameraMatrix']
        with open(cfg['center_to_right_front']) as f:
            self.front_center_to_right_matrix = json.loads(str(f.read()).replace('.,', ','))['cameraMatrix']
        with open(cfg['center_to_left_front']) as f:
            self.front_center_to_left_matrix = json.loads(str(f.read()).replace('.,', ','))['cameraMatrix']
