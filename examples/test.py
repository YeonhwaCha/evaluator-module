import json
import multiprocessing as mp
import numpy as np
import os
import time

from datetime import datetime, timezone
from tqdm import tqdm

from configuration.config import (Config)
from convert_to_ford_gt_for_lane import (LaneGt)
from convert_to_ford_gt_for_obj import (ObjGt)
from data_center import (DataCenter)
from simulation import (Simulation)
from metric.metric_for_lane import (MetricForLane)
from metric.metric_for_bbox2 import (MetricForObj)
from metric.metric_for_freespace import (MetricForFreespace)
from results.ford_eval import (FordEvalResult)
from results.ford_viz import (FordVizResult)
from results.visualize import (Visualize)


def ford_evaluation(cfg, date, timestamps, on_create_gt=True):
    im_size          = cfg.input_img_size
    tr_im_size       = cfg.train_img_size
    src_path         = cfg.annotation_path
    master_gt_path   = cfg.master_json_path
    output_dir       = './out' 
    debug_img_path   = os.path.join(src_path, 'images')
    eval_json_dir    = os.path.join(output_dir, 'eval')

    data_center      = DataCenter(cfg)
    data_center.load_dataset(master_gt_path, choose_timestamps=timestamps)
    master_json_files      = data_center.master_json_files[date]
    location_infos         = data_center.location_info[date]
    
    front_left_ann_files   = data_center.fl_ann_files[date]
    front_center_ann_files = data_center.fc_ann_files[date]
    front_right_ann_files  = data_center.fr_ann_files[date]
    
    front_left_png_files   = data_center.fl_png_files[date]
    front_center_png_files = data_center.fc_png_files[date]
    front_right_png_files  = data_center.fr_png_files[date]

    rear_left_ann_files    = data_center.rl_ann_files[date]
    rear_center_ann_files  = data_center.rc_ann_files[date]
    rear_right_ann_files   = data_center.rr_ann_files[date]

    timestamps             = data_center.timestamps[date]
    timestamps.sort()

    if on_create_gt == True:
        
        try:
            phantom_lane_gt = LaneGt(cfg, front_left_ann_files, front_center_ann_files, front_right_ann_files)
            phantom_lane_gt.create_lane_gt(src_path,
                                           direction_of_cam='front',
                                           multiple=False,
                                           output_dir='{}/{}'.format(output_dir, date))
        except:
            print('..........Failed to make a front lane gt file')
        
        try:
            phantom_obj_gt = ObjGt(cfg, front_left_ann_files, front_center_ann_files, front_right_ann_files)
            phantom_obj_gt.create_gt_for_obj(src_path, 
                                             direction_of_cam='front',
                                             multiple=False,
                                             output_dir='{}/{}'.format(output_dir, date))
        except:
            print('.........Failed to make a front object gt file')
        '''
        try:
            phantom_lane_gt = LaneGt(cfg, rear_center_ann_files, rear_center_ann_files, rear_right_ann_files)
            phantom_lane_gt.create_lane_gt(src_path,
                                           direction_of_cam='rear',
                                           multiple=False,
                                           output_dir='{}/{}'.format(output_dir, date))
        except:
            print('..........Failed to make a rear lane gt file')
        
        try:
            # For single image
            phantom_obj_gt = ObjGt(cfg, rear_left_ann_files, rear_center_ann_files, rear_right_ann_files)
            phantom_obj_gt.create_gt_for_obj(src_path, 
                                             direction_of_cam='rear',
                                             multiple=False,
                                             output_dir='{}/{}'.format(output_dir, date))
        except:
            print('.........Failed to make a rear object gt file')
        '''
    print('preparing simulations for {}'.format(date))
    simulation = Simulation(cfg, date)
    print('..........STEP2 : EVALUATE DETECTION RATE AND ACCURACY')
    relation_ids = ['-1',  '1']
    
    try:
        with open('./out/{}/front_master_gt.json'.format(date), 'r') as f:
            gt_from_json = json.load(f)
            front_annotations = gt_from_json['annotations']
        
        metric_for_front_lane = MetricForLane(cfg, './out/{}/front_lane_gt.json'.format(date), simulation)
        metric_for_front_obj  = MetricForObj(cfg, './out/{}/front_master_gt.json'.format(date), simulation)
        cnt = 0 
        print('Total Target Files', len(timestamps))
        for idx in tqdm(range(0, len(timestamps))):
        #for idx in range(0, 100):
            timestamp = timestamps[idx]
            try:
                front_filenames = front_annotations[timestamp]['filenames']
                ford_eval_result = FordEvalResult(eval_json_dir)
                ford_viz_result = FordVizResult()
                visualize = Visualize(cfg, date, debug_img_path, area='west')
                visualize.img_timestamp = timestamp 
                visualize.load_images(front_filenames, 'front')
                metric_for_front_lane.evaluation_for_single_image(ford_eval_result, ford_viz_result, visualize, relation_ids, timestamp, 'front')
                metric_for_front_obj.evaluation_for_single_image(ford_eval_result, ford_viz_result, visualize, timestamp, 'front')
                
                try:  
                    ford_eval_result.result['location_info'] = location_infos[timestamp][0]
                except:
                    print('location_info does not exist in this case')
                eval_filename = os.path.basename(master_json_files[timestamp]).replace('json', 'eval')
                filepath = os.path.join(eval_json_dir, eval_filename)
                with open(filepath, 'w') as f:
                    f.write(json.dumps(ford_eval_result.result, indent=4, sort_keys=True))
                img_filename = os.path.basename(master_json_files[timestamp]).replace('json', 'jpg')
                visualize.show(ford_eval_result, ford_viz_result, timestamp, on_save=True, filename=img_filename)
            except:
                print('timestamp does not exist')
    except:
        print('..........Failed to Evaluate')
    
if __name__ == '__main__':
    config_file    = './ford_setting_westcoast.json'
    timestamp_file = '../../source_for_evaluation/timestamps/west-coast-combined_16898_timestamps.json'
    #config_file    = './ford_setting_eastcoast.json'
    #timestamp_file = '../../source_for_evaluation/timestamps/east-coast-mar-20-combined_30352_timestamps.json'
    #timestamp_file = './out/err_timestamps.json'

    with open(timestamp_file, 'r') as fs:
        timestamps = json.load(fs)
    # TEST 
    #timestamps             = ['1574111080431', '1574111197637', '1574123158263', '1574124453592', '1574124585800']
    #timestamps = ['1576759571283']
    #timestamps = ['1576774351978']
    #timestamps  = ['1576504161437', '1576504164370', '1576504167371', '1576504170371', '1576504173370', 
    #               '1576504176371', '1576504179371', '1576504182371', '1576504185438', '1576508032300']
    #timestamps = ['1576759502146']

    cfg = Config(config_file)
    timestamps = ['1574207186438']
    #timestamps = ['1574199227210']
    '''   
    timestamps = ['1574188919983', '1574199227210', '1574200240194', '1574207186438', '1574210324458',
                  '1574210667075', '1574211024891', '1574214826941', '1574269346532', '1574269674214',
                  '1574270019831', '1574270359515', '1574271904058', '1574274212505', '1574274909806', 
                  '1574275243423', '1574275901789', '1574276232405', '1574276878638', '1574278783543',
                  '1574280400156', '1574281088191', '1574281598082', '1574282247247', '1574283263164',
                  '1574284639698', '1574285300931', '1574285995164', '1574286328847', '1574286677463', 
                  '1574286999146', '1574287341831', '1574287711515', '1574288039131', '1574288375814',
                  '1574288718498', '1574289049114', '1574289397864', '1574290057563']
    '''
    #ford_evaluation(cfg, "2019-11-18", timestamps, True)
    ford_evaluation(cfg, "2019-11-19", timestamps, False)
    #ford_evaluation(cfg, "2019-11-20", timestamps, False)
    #ford_evaluation(cfg, "2019-12-16", timestamps, True)
    #ford_evaluation(cfg, "2019-12-17", timestamps, True)
    #ford_evaluation(cfg, "2019-12-18", timestamps, True)
    #ford_evaluation(cfg, "2019-12-19", timestamps, True)
    '''
    # Init multiprocessing.Pool()
    if len(cfg.simulation_target_date) < mp.cpu_count():
        pool = mp.Pool(mp.cpu_count())

    pool.starmap(ford_evaluation, [(cfg, date, timestamps, False) for date in cfg.simulation_target_date])

    pool.close()
    '''
