mkvirtualenv evaluator-module -p python3

pip install -r requirements.txt

validation_dir=../../source_for_evaluation
echo $validation_dir
if [ ! -d "$validation_dir" ]
then 
  echo "validation directory doesn't exist. Creating now"
  mkdir $validation_dir
  echo "validation directory create"
else
  echo "validation directory exists"
fi

collection_images=../../source_for_evaluation/collection_images
echo $collection_images
if [ ! -d "collection_images" ]
then 
  echo "collection_images doesn't exist. Creating now"
  mkdir $collection_images
  echo "collection_images create"
else
  echo "collection_images exists"
fi

sudo mount -t cifs //phantom0/data_export/validation/collection_images ../../source_for_evaluation/collection_images

rosbag_output_dir=../../source_for_evaluation/rosbag_output
echo rosbag_output_dir
if [ ! -d "rosbag_output" ]
then 
  echo "rosbag_output_dir doesn't exist. Creating now"
  mkdir $rosbag_output_dir
  echo "rosbag_output create"
else
  echo "rosbag_output exists"
fi

sudo mount -t cifs //phantom0/data_export/validation/rosbag_output ../../source_for_evaluation/rosbag_output

#sudo mount -t cifs //phantom0/data_export/validation/rosbag_output ./simulation

#add2virtualenv /home/yeonhwa/eval-vision/evaluator-module/data_center
#add2virtualenv /home/yeonhwa/eval-vision/evaluator-module

