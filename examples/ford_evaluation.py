import json
import multiprocessing as mp
import numpy as np
import os
import time

from datetime import datetime, timezone
from tqdm import tqdm

from configuration.config import (Config)
from convert_to_ford_gt_for_lane import (LaneGt)
from convert_to_ford_gt_for_obj import (ObjGt)
from data_center import (DataCenter)
from simulation import (Simulation)
from metric.metric_for_lane import (MetricForLane)
from metric.metric_for_bbox import (MetricForObj)
from metric.metric_for_freespace import (MetricForFreespace)
from results.ford_eval import (FordEvalResult)
from results.ford_viz import (FordVizResult)
from results.visualize import (Visualize)


def ford_evaluation(cfg, date, timestamps, on_create_gt=True):
    im_size          = cfg.input_img_size
    tr_im_size       = cfg.train_img_size
    src_path         = cfg.annotation_path
    master_gt_path   = cfg.master_json_path
    debug_img_path   = os.path.join(src_path, 'images')
    output_dir       = './out' 
    eval_json_dir    = os.path.join(output_dir, 'eval')

    data_center      = DataCenter(cfg)
    data_center.load_dataset(master_gt_path, choose_timestamps=timestamps)
    master_json_files      = data_center.master_json_files[date]
    location_infos         = data_center.location_info[date]
    front_left_ann_files   = data_center.fl_ann_files[date]
    front_center_ann_files = data_center.fc_ann_files[date]
    front_right_ann_files  = data_center.fr_ann_files[date]
    rear_left_ann_files    = data_center.rl_ann_files[date]
    rear_center_ann_files  = data_center.rc_ann_files[date]
    rear_right_ann_files   = data_center.rr_ann_files[date]
        
    timestamps             = data_center.timestamps[date]
    timestamps.sort()

    if on_create_gt == True:
        try:
            phantom_lane_gt = LaneGt(cfg, front_left_ann_files, front_center_ann_files, front_right_ann_files)
            phantom_lane_gt.create_lane_gt(src_path,
                                           direction_of_cam='front',
                                           multiple=True,
                                           output_dir='{}/{}'.format(output_dir, date))
        except:
            print('..........Failed to make a front lane gt file')
       
        try:
            phantom_obj_gt = ObjGt(cfg, front_left_ann_files, front_center_ann_files, front_right_ann_files)
            phantom_obj_gt.create_gt_for_obj(src_path, 
                                             direction_of_cam='front',
                                             multiple=True,
                                             output_dir='{}/{}'.format(output_dir, date))
        except:
            print('.........Failed to make a front lane gt file')
        
        try:
            phantom_lane_gt = LaneGt(cfg, rear_center_ann_files, rear_center_ann_files, rear_right_ann_files)
            phantom_lane_gt.create_lane_gt(src_path,
                                           direction_of_cam='rear',
                                           multiple=False,
                                           output_dir='{}/{}'.format(output_dir, date))
        except:
            print('..........Failed to make a front lane gt file')
        
        try:
            # For single image
            phantom_obj_gt = ObjGt(cfg, rear_left_ann_files, rear_center_ann_files, rear_right_ann_files)
            phantom_obj_gt.create_gt_for_obj(src_path, 
                                             direction_of_cam='rear',
                                             multiple=False,
                                             output_dir='{}/{}'.format(output_dir, date))
        except:
            print('.........Failed to make a rear object gt file')
    
    print('preparing simulations for {}'.format(date))
    simulation = Simulation(cfg, date)
    print('..........STEP2 : EVALUATE DETECTION RATE AND ACCURACY')
    relation_ids = ['-1',  '1']
    
    try:
        with open('./out/{}/front_master_gt.json'.format(date), 'r') as f:
            gt_from_json = json.load(f)
            front_annotations = gt_from_json['annotations']
        
        with open('./out/{}/rear_master_gt.json'.format(date), 'r') as f:
            gt_from_json = json.load(f)
            rear_annotations = gt_from_json['annotations']
        
        metric_for_front_lane = MetricForLane(cfg, './out/{}/front_lane_gt.json'.format(date), simulation)
        metric_for_rear_lane  = MetricForLane(cfg, './out/{}/rear_lane_gt.json'.format(date), simulation)
        metric_for_front_obj  = MetricForObj(cfg, './out/{}/front_master_gt.json'.format(date), simulation)
        metric_for_rear_obj   = MetricForObj(cfg, './out/{}/rear_master_gt.json'.format(date), simulation)
        metric_for_freespace  = MetricForFreespace(cfg, src_path, front_center_ann_files, simulation)
        cnt = 0 
        print('Total Target Files', len(timestamps))
        for idx in tqdm(range(0, len(timestamps))):
        #for idx in range(0, 100):
            timestamp = timestamps[idx]
            front_filenames = front_annotations[timestamp]['filenames']
            rear_filenames = rear_annotations[timestamp]['filenames']
            ford_eval_result = FordEvalResult(eval_json_dir)
            ford_viz_result = FordVizResult()
            visualize = Visualize(cfg, date, debug_img_path)
            visualize.img_timestamp = timestamp 
            visualize.load_images(front_filenames, 'front')
            visualize.load_images(rear_filenames, 'rear')
            
            metric_for_freespace.evaluation_for_single_image(ford_eval_result, visualize, timestamp) 
            metric_for_front_obj.evaluation_for_single_image(ford_eval_result, ford_viz_result, visualize, timestamp, 'front')
            metric_for_front_lane.evaluation_for_single_image(ford_eval_result, ford_viz_result, visualize, relation_ids, timestamp, 'front')
            metric_for_rear_obj.evaluation_for_single_image(ford_eval_result, ford_viz_result, visualize, timestamp, 'rear')
            metric_for_rear_lane.evaluation_for_single_image(ford_eval_result, ford_viz_result, visualize, relation_ids, timestamp, 'rear')
            
            try:  
                ford_eval_result.result['location_info'] = location_infos[timestamp][0]
            except:
                print('location_info does not exist in this case')
            
            eval_filename = os.path.basename(master_json_files[timestamp]).replace('json', 'eval')
            filepath = os.path.join(eval_json_dir, eval_filename)
            with open(filepath, 'w') as f:
                f.write(json.dumps(ford_eval_result.result, indent=4, sort_keys=True))
            '''
            img_filename = os.path.basename(master_json_files[timestamp]).replace('json', 'png')
            visualize.show(ford_eval_result, ford_viz_result, timestamp, on_save=True, filename=img_filename)
            '''
    except:
        print('..........Failed to Evaluate')
    

if __name__ == '__main__':
    config_file    = './ford_setting_westcoast.json'
    timestamp_file = '../../source_for_evaluation/timestamps/west-coast-combined_16898_timestamps.json'

    with open(timestamp_file, 'r') as fs:
        timestamps = json.load(fs)
    
    cfg = Config(config_file)
    
    #ford_evaluation(cfg, "2019-11-18", timestamps, False)
    
    # Init multiprocessing.Pool()
    if len(cfg.simulation_target_date) < mp.cpu_count():
        pool = mp.Pool(mp.cpu_count())

    pool.starmap(ford_evaluation, [(cfg, date, timestamps, True) for date in cfg.simulation_target_date])

    pool.close()
    
