
#include "camera_model.h"

using namespace phantom_ai;

int main(int argc, char **argv)
{
  std::string calib_file_name = "camera_calibration_list.yaml";
  std::string vehicle_name = "genesis_4402";
  std::vector<CameraID> cameras = {CAM_FRONT_CENTER, CAM_FRONT_CENTER_CROP};
  cv::Size image_size(960,540);

  auto camera_models = std::make_shared<CameraModelList>(NUM_CAM_DIRS);
  camera_models->Initialize(calib_file_name,
                            vehicle_name,
                            image_size,
                            cameras);

  const std::string wnd_name = "cam_window";
  for (auto cam : cameras)
  {
    auto model = camera_models->intrinsic(cam);
    if (model && model->Valid())
    {
      cv::imshow(wnd_name, model->Visualize());
      cv::waitKey(0);
      cv::destroyWindow(wnd_name);
    }
  }

  return 0;
}
