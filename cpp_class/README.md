# README #

### What is this repository for? ###

* This is a camera calibration class that models pin-hole and fish-eye lens distortion. More powerful generic models will be added soon.
* Version: 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Everything should be self-contained here and no need to download something else.
```
cd cython
python setup.py build_ext --inplace
```

### Contribution guidelines ###

Contact the repo owner.

### Who do I talk to? ###

* for cython :: Yeonhwa Cha (yeonhwa@phantom.ai)
* Myung Hwangbo (myung@phantom.ai)
