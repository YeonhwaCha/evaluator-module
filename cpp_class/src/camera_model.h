/*******************************************************************************
* @file    camera_model.h
* @date    04/17/2020
*
* @attention Copyright (c) 2017
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#ifndef __PHANTOMVISION2_CAMERA_MODEL_H__
#define __PHANTOMVISION2_CAMERA_MODEL_H__

#include "vision_constants.h"
#include "geometry_utils.h"
#include <map>
#include <opencv2/core.hpp>

namespace phantom_ai
{
  enum CameraCalibTypeEnum
  {
    CAMERA_CALIB_PINHOLE = 0,
    CAMERA_CALIB_FISHEYE_SCARAMUZZA,
    CAMERA_CALIB_INVALID
  };

  CameraCalibTypeEnum camera_calib_name(std::string name, bool warning = false);

  std::string camera_calib_name(CameraCalibTypeEnum type, bool warning = false);
  
  bool read_camera_model_params(std::string param_file,
                                std::string vehicle_name,
                                std::vector<CameraInfo>& camera_info_list);

  void print_camera_info(bool v, CameraID camera, const CameraInfo& info);
  
  class CameraModel
  {
  public:
    CameraModel();
    CameraModel(CameraID camera);

    ~CameraModel();

    bool Valid();

    bool Initialize(CameraInfo cam_info, cv::Size image_size = {});

    bool Initialize(cv::Mat K_cam, cv::Mat distortion,
                    cv::Mat skew, cv::Size resolution,
                    cv::Rect roi = {}, cv::Size size = {},
                    std::string name = "");

    void Log(int verbosity);

    std::string Name() const { return name_; }

    cv::Size ImageSize() const { return image_size_; }

    cv::Rect ImageRoi() const { return image_roi_; }

    //
    // modeling
    //
    cv::Point2f Project(const cv::Point2f& n) const;

    cv::Point2f Project(const cv::Point3f& W) const;

    cv::Point2f Project(const cv::Point3f& W, const cv::Mat& T_cam) const;

    cv::Mat ProjectMat(const cv::Mat& tracks, const cv::Mat& masks) const;

    cv::Point2f Distort(const cv::Point3f& q3) const;

    cv::Point2f Distort(const cv::Point2f& q, float sign = 1.0f) const;

    cv::Point2f Undistort(const cv::Point2f& p, bool interp = false) const;

    cv::Point2f Normalize(const cv::Point2f& p, bool interp = false) const;

    cv::Rect2f Normalize(const cv::Rect2f& p, bool interp = false) const;

    cv::Point3f NormalizeH(const cv::Point2f& p, bool interp = false) const;

    std::vector<cv::Point2f> Normalize(const std::vector<cv::Point2f>& p, bool interp = false) const;

    cv::Point2f NormalizeByTable(const cv::Point2f& p, bool interp = false) const;

    cv::Point2f NormalizeByTable(const cv::Point& p) const;

    void Normalize(const cv::Point2f& p, cv::Point2f& q, bool interp = false) const;

    bool Normalize(const cv::Mat& P, cv::Mat N, int num_points = -1, bool interp = false) const;

    virtual void BuildLut(float s) = 0;

    virtual cv::Point2f DistortBySolve(const cv::Point2f& q, float sign) const = 0;

    virtual cv::Point2f NormalizeBySolve(const cv::Point2f& p) const = 0;

    //
    // intrinsics
    //
    cv::Point2f PrincipalPoint() const;

    float FocalLength() const;

    float Fov(int i=0) const;

    float RadianPerPixel() const;


    //
    // extrinsics
    //
    void SetExtrinsic(const cv::Vec3f& rpy, const cv::Vec3f& pos);

    void SetRotation(const cv::Vec3f& rpy);

    void SetPosition(const cv::Vec3f& pos);

    void SetPitch(float pitch);

    float PitchFromY(float y) const;

    float FromPitch(float pitch) const;

    cv::Vec3f Rotation() const;

    cv::Mat Homography() const;

    cv::Mat HomographyFromPitch(float pitch) const;

    cv::Mat HomographyFromDeltaPitch(float delta_pitch) const;

    cv::Mat HomographyFromRmat(cv::Mat Rmat) const;

    cv::Mat TransformFromPitch(float pitch) const;

    cv::Mat TransformFromDeltaPitch(float pitch) const;

    // miscellaneous
    //cv::Mat Visualize(float scale = 1.0f, float border_ratio = 1.0f);

    bool Save(std::string yaml_fname);

  protected:
    CameraID camera_;
    CameraCalibTypeEnum calib_type_;

    bool initialized_;
    int verbosity_;
    std::string name_;

    cv::Size image_size_;
    cv::Rect image_roi_;

    // camera intrinsic
    cv::Mat K_cam_;
    cv::Mat K_cam_inv_;
    cv::Mat distortion_;
    cv::Mat skew_;
    cv::Mat skew_inv_;

    int w_;
    int h_;
    float fx_;
    float fy_;
    float cx_;
    float cy_;
    float resize_factor_;

    float fov_h_[2];
    float fov_v_[2];
    float fov_[2];
    float radian_per_pixel_;

    // camera rectification look-up table
    cv::Mat lut_;
    float* lut_ptr_;

    cv::Mat lut_distort_;
    float* lut_distort_ptr_;
    std::string lut_log_;

    // camera extrinsic
    cv::Vec3f rotation_rpy_;
    cv::Vec3f position_;
    cv::Mat I_3x3_;
    cv::Mat R_ext_;
    cv::Mat T_ext_;
  };

  using CameraModelS = std::shared_ptr<CameraModel>;


  class CameraModelPinhole : public CameraModel
  {
  public:
    CameraModelPinhole(CameraID camera);
    ~CameraModelPinhole();
    virtual void BuildLut(float s);
    virtual cv::Point2f DistortBySolve(const cv::Point2f& p, float sign) const;
    virtual cv::Point2f NormalizeBySolve(const cv::Point2f& p) const;

  protected:


  };


  class CameraModelScara : public CameraModel
  {
  public:
    CameraModelScara(CameraID camera);

    ~CameraModelScara();

    virtual void BuildLut(float s);

    virtual cv::Point2f DistortBySolve(const cv::Point2f& p, float sign) const;

    virtual cv::Point2f NormalizeBySolve(const cv::Point2f& p) const;

  protected:
    double FindRho(double d, int& dm) const;

  private:
    double d_rho_scale_;
    std::map<int, double> rho_map_;
  };


  /////////////////////////////////////////////////////////////////////////////
  // CameraModelList
  //
  class CameraModelList
  {
  public:
    CameraModelList();
    CameraModelList(int num_cameras);

    ~CameraModelList();
    
    bool IsReady(const std::vector<CameraID>& cameras);

    bool IsReady(const std::vector<std::vector<CameraID>>& cameras);

    bool Initialize(std::string param_file, std::string vehicle, 
                    std::pair<int, int> image_size, std::vector<int>& cameras);

    std::vector<int> camera_list;
    
    bool GetCropViewport(CameraID main, CameraID crop, cv::Rect2f& roi);
    std::vector<float> GetCoordFromWorld(int cam, float y, float width_m, float position_y, float range_m);
    //void GetBboxFromWorldCoord(int cam);
    //void Visualize(bool b_show = true);
    void Save();

    CameraModelS  intrinsic(int i) const { return models_[i]; }
    CameraModelS& intrinsic(int i)       { return models_[i]; }

    CameraModelS  extrinsic(int i) const { return models_[i]; }
    CameraModelS& extrinsic(int i)       { return models_[i]; }
  
  private:
    int num_cameras_;
    std::string config_file_;
    std::vector<std::shared_ptr<CameraModel>> models_;
  };

  using CameraModelListS = std::shared_ptr<CameraModelList>;
}

#endif
