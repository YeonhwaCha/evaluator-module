#include "camera_model.h"

#include "python_wrapper.h"
#include <opencv2/imgproc.hpp>


using namespace phantom_ai;

namespace phantom_ai
{
  ///////////////////////////////////////////////////////////////////////////
  // Camera Model Wrapper
  //
  CameraModelWrapper::CameraModelWrapper()
  {
  
  }
  
  CameraModelWrapper::~CameraModelWrapper()
  {
  
  }
  
  bool CameraModelWrapper::Initialize()
  {
    printf("Initialize....\n");
    std::string calib_file_name = "camera_calibration_list.yaml";
    std::string vehicle_name = "genesis_4402";
    std::vector<CameraID> cameras = {CAM_FRONT_CENTER, CAM_FRONT_CENTER_CROP};
    cv::Size image_size(960,540);
  
    auto camera_models = std::make_shared<CameraModelList>(NUM_CAM_DIRS);
    /*camera_models->Initialize(calib_file_name,
                              vehicle_name,
                              image_size,
                              cameras);*/
  
    //auto cam_model = camera_models->extrinsic(0);
  
    //return 0;
  }
};
