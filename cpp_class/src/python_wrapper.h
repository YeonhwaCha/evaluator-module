
#ifndef __PHANTOMVISION2_PYTHON_WRAPPER_H__
#define __PHANTOMVISION2_PYTHON_WRAPPER_H__

#include "camera_model.h"

#include "vision_constants.h"
#include <opencv2/core.hpp>

namespace phantom_ai
{
  class CameraModelWrapper
  {
  public:
    CameraModelWrapper();
    ~CameraModelWrapper();

    bool Initialize();
  };
}

#endif
