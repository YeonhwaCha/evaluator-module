/*******************************************************************************
* @file    camera_model.cpp
* @date    04/17/2020
*
* @attention Copyright (c) 2017
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#include "camera_model.h"
#include <opencv2/imgproc.hpp>

#define SKEW(i,j)       skew_.at<float>(i,j)
#define SKEW_INV(i,j)   skew_inv_.at<float>(i,j)

using namespace phantom_ai;

namespace phantom_ai
{
  CameraModel::CameraModel() {}
  CameraModel::CameraModel(CameraID camera) :
    camera_(camera),
    calib_type_(CAMERA_CALIB_INVALID),
    initialized_(false),
    verbosity_(0),
    name_("unknown"),
    resize_factor_(1.0f),
    lut_ptr_(nullptr),
    lut_distort_ptr_(nullptr),
    rotation_rpy_(0, 0, 0),
    position_(0, 0, 0),
    I_3x3_(cv::Mat::eye(3,3, CV_32F)),
    R_ext_(cv::Mat::eye(3,3, CV_32F)),
    T_ext_(cv::Mat::eye(4,4, CV_32F))
  {

  }


  CameraModel::~CameraModel()
  {

  }

  bool CameraModel::Valid()
  {
    return initialized_;
  }

  bool CameraModel::Initialize(CameraInfo cam_info, cv::Size image_size)
  {
    CameraCalibTypeEnum type = camera_calib_name(cam_info.calib_model_);
    if (type != calib_type_)
    {
      //PHANTOM_ERROR("Wrong camera model class for '{}' ({}, {}).",
      //    camera_name(camera_), type, calib_type_);
      return false;
    }

    bool success = Initialize(cam_info.camera_matrix_, cam_info.distortion_,
                              cam_info.skew_, cam_info.resolution_,
                              cam_info.crop_roi_, image_size,
                              cam_info.name_ + "@" + cam_info.vehicle_);

    SetExtrinsic(cv::Vec3f(cam_info.rotation_rad_.data()),
                 cv::Vec3f(cam_info.position_meter_.data()));

    return success;
  }


  bool CameraModel::Initialize(cv::Mat K_cam, cv::Mat distortion,
                               cv::Mat skew, cv::Size resolution,
                               cv::Rect roi, cv::Size size,
                               std::string name)
  {
    if (calib_type_ >= CAMERA_CALIB_INVALID)
    {
      //PHANTOM_ERROR("Invalid camera calib type, '{}'.", camera_calib_name(calib_type_));
      return false;
    }

    // resize or crop from the original image
    name_ = name;
    image_roi_ = (roi.width == 0 || roi.height == 0) ? cv::Rect(cv::Point(0,0), resolution) : roi;
    image_size_ = (size.width == 0 || size.height == 0) ? image_roi_.size() : size;

    float x0 = image_roi_.x;
    float y0 = image_roi_.y;
    float sx = (float)image_size_.width / image_roi_.width;
    float sy = (float)image_size_.height / image_roi_.height;
    if (sx != sy)
    {
      //PHANTOM_WARNING("Image aspect ratio changes in a new camera model, sx = {} sy = {}.", sx, sy);
    }

    cv::Mat K_t = (cv::Mat_<float>(3, 3) << 1,  0, -x0,   0, 1, -y0,   0, 0, 1);
    cv::Mat K_s = (cv::Mat_<float>(3, 3) << sx, 0,  0,    0, sy, 0,    0, 0, 1);
    K_cam_ = K_s * K_t * K_cam;
    K_cam_inv_ = K_cam_.inv();

    w_ = image_size_.width;
    h_ = image_size_.height;
    fx_ = K_cam_.at<float>(0,0);
    fy_ = K_cam_.at<float>(1,1);
    cx_ = K_cam_.at<float>(0,2);
    cy_ = K_cam_.at<float>(1,2);

    printf("fx :: %f, fy :: %f, cx :: %f, cy :: %f\n", fx_, fy_, cx_, cy_);

    skew_ = skew;
    skew_inv_ = skew.inv();

    // distortion coefficients:
    //  - pinhole uses (k1, k2, p1, p2[, k3[, k4, k5, k6]]) of 4, 5, or 8 elements
    //  - fisheye scaramuzza uses (a0, a1, a2, a3, a4)
    distortion.convertTo(distortion_, CV_64F);

    // build a precomputed undistortion table.
    BuildLut(sx);

    // compute FOVs
    fov_h_[0] = std::atan(Normalize(cv::Point2f(0,      h_ / 2)).x);
    fov_h_[1] = std::atan(Normalize(cv::Point2f(w_ - 1, h_ / 2)).x);
    fov_v_[0] = std::atan(Normalize(cv::Point2f(w_ / 2,      0)).y);
    fov_v_[1] = std::atan(Normalize(cv::Point2f(w_ / 2, h_ - 1)).y);
    fov_[0] = TWO_PI(fov_h_[1] - fov_h_[0]);
    fov_[1] = TWO_PI(fov_v_[1] - fov_v_[0]);
    radian_per_pixel_ = fov_[0] / image_size_.width;

    initialized_ = true;

    return initialized_;
  }


  cv::Point2f CameraModel::Project(const cv::Point2f& n) const
  {
    cv::Point2f q(fx_ * n.x + cx_, fy_ * n.y + cy_);
    return Distort(q);
  }


  cv::Point2f CameraModel::Project(const cv::Point3f& W) const
  {
    float sign = (W.z > 0.0f) ? 1.0f : -1.0f;
    cv::Point2f q(fx_ * (W.x/W.z) + cx_, fy_ * (W.y/W.z) + cy_);
    return Distort(q, sign);
  }


  cv::Point2f CameraModel::Project(const cv::Point3f& W, const cv::Mat& T_cam) const
  {
    return Project(geom::transform_point<float>(T_cam, W));
  }


  cv::Mat CameraModel::ProjectMat(const cv::Mat& tracks, const cv::Mat& masks) const
  {
    cv::Mat out = cv::Mat::zeros(tracks.size(), tracks.type());

    for (int i=0; i < tracks.rows; i++)
    {
      for (int k=0; k < tracks.cols; k++)
      {
        if (masks.at<uchar>(i,k))
        {
          out.at<cv::Vec2f>(i,k) = Project(tracks.at<cv::Vec2f>(i,k));
        }
      }
    }

    return out;
  }


  cv::Point2f CameraModel::Distort(const cv::Point3f& q3) const
  {
    float sign = (q3.z > 0.0f) ? 1.0f : -1.0f;
    if (std::abs(q3.z) < 1e-10)
    {
      //PHANTOM_WARNING("Divided by a very small number, q3.z = {}", q3.z);
      return Distort(cv::Point2f(0, 0));
    }
    else
    {
      cv::Point2f q(q3.x / q3.z, q3.y / q3.z);
      return Distort(q, sign);
    }
  }


  cv::Point2f CameraModel::Distort(const cv::Point2f& q, float sign) const
  {
    return DistortBySolve(q, sign);
  }


  cv::Point2f CameraModel::Undistort(const cv::Point2f& p, bool interp) const
  {
    cv::Point2f n = Normalize(p, interp);
    return cv::Point2f(fx_ * n.x + cx_, fy_ * n.y + cy_);
  }


  cv::Point2f CameraModel::Normalize(const cv::Point2f& p, bool interp) const
  {
    if (p.x < 0 || p.x >= w_-1 || p.y < 0 || p.y >= h_-1)
    {
      return NormalizeBySolve(p);
    }
    else
    {
      return NormalizeByTable(p, interp);
    }
  }


  cv::Rect2f CameraModel::Normalize(const cv::Rect2f& p, bool interp) const
  {
    cv::Point2f t = Normalize(cv::Point2f(p.x, p.y), interp);
    cv::Point2f b0 = Normalize(cv::Point2f(p.x, p.y + p.height), interp);
    cv::Point2f b1 = Normalize(cv::Point2f(p.x + p.width, p.y + p.height), interp);

    float w = b1.x - b0.x;
    float y = (b0.y + b1.y)/2;
    float h = y - t.y;

    return cv::Rect2f(b0.x, t.y, w, h);
  }


  cv::Point3f CameraModel::NormalizeH(const cv::Point2f& p, bool interp) const
  {
    cv::Point2f n = Normalize(p, interp);
    return cv::Point3f(n.x, n.y, 1.0f);
  }


  std::vector<cv::Point2f> CameraModel::Normalize(const std::vector<cv::Point2f>& p, bool interp) const
  {
    std::vector<cv::Point2f> n_pt(p.size());
    for (size_t i=0; i < p.size(); i++) n_pt[i] = Normalize(p[i], interp);
    return n_pt;
  }


  cv::Point2f CameraModel::NormalizeByTable(const cv::Point2f& p, bool interp) const
  {
    if (interp)
    {
      cv::Point p1(std::floor(p.x), std::floor(p.y));
      cv::Point p2(std::ceil(p.x), std::ceil(p.y));
      cv::Point2f n1 = NormalizeByTable(p1);
      cv::Point2f n2 = NormalizeByTable(p2);
      float dx = p2.x - p.x;
      float dy = p2.y - p.y;
      return cv::Point2f(dx*n1.x + (1-dx)*n2.x, dy*n1.y + (1-dy)*n2.y);
    }

    return NormalizeByTable(cv::Point(p));
  }


  cv::Point2f CameraModel::NormalizeByTable(const cv::Point& p) const
  {
    int index = p.y * w_ + p.x;
    return cv::Point2f(lut_ptr_[2*index], lut_ptr_[2*index+1]);
  }


  void CameraModel::Normalize(const cv::Point2f& p, cv::Point2f& n, bool interp) const
  {
    n = Normalize(p, interp);
  }


  bool CameraModel::Normalize(const cv::Mat& P, cv::Mat N, int num_points, bool interp) const
  {
    if (!(P.type() == CV_32FC2 && N.type() == CV_32FC2))
    {
      //PHANTOM_ERROR("cv::Mat type is not supported yet. type = {}", P.type());
      return false;
    }

    if (num_points < 0)
    {
      num_points = P.rows;
    }

    for (int i=0; i < num_points; i++)
    {
      auto p = reinterpret_cast<const cv::Point2f *>(P.ptr(i));
      auto n = reinterpret_cast<cv::Point2f *>(N.ptr(i));
      *n = Normalize(*p, interp);
    }

    return true;
  }


  /////////////////////////////////////////////////////////////////////////////
  // Camera intrinsics
  //
  cv::Point2f CameraModel::PrincipalPoint() const
  {
    return cv::Point2f(cx_, cy_);
  }


  float CameraModel::FocalLength() const
  {
    return (fx_ + fy_)/2;
  }


  float CameraModel::Fov(int i) const
  {
    return fov_[i];
  }


  float CameraModel::RadianPerPixel() const
  {
    return radian_per_pixel_;
  }

  /////////////////////////////////////////////////////////////////////////////
  // Camera extrinsics
  //
  void CameraModel::SetExtrinsic(const cv::Vec3f& rpy, const cv::Vec3f& pos)
  {
    rotation_rpy_ = rpy;
    position_ = pos;
    R_ext_ = geom::R_rpy<float>(rotation_rpy_);
    T_ext_ = geom::T_4x4<float>(rotation_rpy_, position_);
  }

  void CameraModel::SetRotation(const cv::Vec3f& rpy)
  {
    SetExtrinsic(rpy, position_);
  }


  void CameraModel::SetPosition(const cv::Vec3f& pos)
  {
    SetExtrinsic(rotation_rpy_, pos);
  }


  void CameraModel::SetPitch(float pitch)
  {
    rotation_rpy_[1] = pitch;
    SetExtrinsic(rotation_rpy_, position_);
  }


  float CameraModel::PitchFromY(float y) const
  {
    printf("PitchFromY\n");
    printf("DEBUG :: %f, %f, %f, %f\n", fx_, fy_, cx_, cy_);
    cv::Point2f n = Normalize(cv::Point2f(w_/2, y));
    return std::atan(-n.y);
  }


  float CameraModel::FromPitch(float pitch) const
  {
    cv::Point2f n(0.0f, std::tan(-pitch));
    return Project(n).y;
  }


  cv::Vec3f CameraModel::Rotation() const
  {
    return rotation_rpy_;
  }


  cv::Mat CameraModel::Homography() const
  {
    return geom::compute_homography<float>(I_3x3_, T_ext_);
  }


  cv::Mat CameraModel::HomographyFromPitch(float pitch) const
  {
    return geom::compute_homography<float>(I_3x3_,
      geom::T_4x4(cv::Vec3f(rotation_rpy_[0], pitch, rotation_rpy_[2]), position_));
  }


  cv::Mat CameraModel::HomographyFromDeltaPitch(float delta_pitch) const
  {
    return geom::compute_homography<float>(I_3x3_,
      geom::T_4x4(cv::Vec3f(rotation_rpy_[0], rotation_rpy_[1] + delta_pitch, rotation_rpy_[2]), position_));
  }


  cv::Mat CameraModel::HomographyFromRmat(cv::Mat Rmat) const
  {
    cv::Mat T_mat = (cv::Mat_<float>(4, 4) <<
                     Rmat.at<float>(0,0), Rmat.at<float>(0,1), Rmat.at<float>(0,2), position_[0],
                     Rmat.at<float>(1,0), Rmat.at<float>(1,1), Rmat.at<float>(1,2), position_[1],
                     Rmat.at<float>(2,0), Rmat.at<float>(2,1), Rmat.at<float>(2,2), position_[2],
                     0, 0, 0, 1);
    return geom::compute_homography<float>(I_3x3_, T_mat);
  }


  cv::Mat CameraModel::TransformFromPitch(float pitch) const
  {
    cv::Mat T_c = (cv::Mat_<float>(4, 4) << 0,-1,0,0,  0,0,-1,0,  1,0,0,0,  0,0,0,1);
    cv::Mat T_cam = geom::T_4x4<float>(cv::Vec3f(rotation_rpy_[0], pitch, rotation_rpy_[2]), position_);
    return T_c * T_cam.inv();
  }


  cv::Mat CameraModel::TransformFromDeltaPitch(float delta_pitch) const
  {
    cv::Mat T_c = (cv::Mat_<float>(4, 4) << 0,-1,0,0,  0,0,-1,0,  1,0,0,0,  0,0,0,1);
    cv::Mat T_cam = geom::T_4x4<float>(cv::Vec3f(rotation_rpy_[0], rotation_rpy_[1] + delta_pitch, rotation_rpy_[2]), position_);
    return T_c * T_cam.inv();
  }


  /*cv::Mat CameraModel::Visualize(float scale, float border_ratio)
  {
    int width_extra = w_ * (1 + border_ratio);
    int height_extra = h_ * (1 + border_ratio);
    cv::Point2f offset(w_ * border_ratio/2, h_ * border_ratio/2);

    cv::Mat disp = cv::Mat::zeros(cv::Size(scale * width_extra, scale * height_extra), CV_8UC3);
    int x_delta = w_/16;
    int y_delta = h_/16;
    int x_extra = (fov_[0] > M_PI/2) ? -2*x_delta : x_delta;
    int y_extra = (fov_[0] > M_PI/2) ? -2*y_delta : y_delta;

    for (int x=-x_extra; x <= w_ + x_extra; x+=x_delta)
    {
      cv::Point2f p0 = scale * (cv::Point2f(x, -y_extra) + offset);
      cv::Point2f p1 = scale * (cv::Point2f(x, h_ + y_extra) + offset);
      cv::line(disp, p0, p1, CV_GRAY40, 1);
    }

    for (int y=-y_extra; y <= h_ + y_extra; y+=y_delta)
    {
      cv::Point2f p0 = scale * (cv::Point2f(-x_extra, y) + offset);
      cv::Point2f p1 = scale * (cv::Point2f(w_ + x_extra, y) + offset);
      cv::line(disp, p0, p1, CV_GRAY40, 1);
    }

    cv::rectangle(disp, {offset, cv::Size(scale*w_, scale*h_)}, CV_RED, 2);

    for (int x=-x_extra; x <= w_ + x_extra; x+=x_delta)
    {
      for (int y=-y_extra; y <= h_ + y_extra; y+=y_delta)
      {
        cv::Point2f p(x,y);
#if 0
        cv::Point2f u = Undistort(p, true);
        cv::Point2f d = Distort(u);
#else
        cv::Point2f n = Normalize(p, true);
        cv::Point2f p1 = Project(n);
        cv::Point2f q = {fx_*n.x + cx_, fy_*n.y + cy_};
#endif
        cv::circle(disp, scale * (p1 + offset), 2, CV_MAGENTA);
        cv::circle(disp, scale * (q + offset), 4, CV_WHITE);
        cv::line(disp, scale * (p + offset), scale * (q + offset), CV_ORANGE, 1);
      }
    }

    cv::Point2f c(cx_, cy_);
    cv::drawMarker(disp, scale * (c + offset), CV_YELLOW, cv::MARKER_TILTED_CROSS, 10, 2);

    std::string title = name_ + " | " + camera_calib_name(calib_type_);
    cv::putText(disp, title, {10, 20}, CV_FONT_HERSHEY_PLAIN, 1.0f, CV_WHITE);

    return disp;
  }*/


  bool CameraModel::Save(std::string yaml_fname)
  {
    bool save_distortion = true;
    bool save_undistortion = false;

    cv::FileStorage fs(yaml_fname, cv::FileStorage::WRITE);

    fs << "calib_model_type" << camera_calib_name(calib_type_);
    fs << "image_size" << image_size_;
    fs << "image_roi" << image_roi_;
    fs << "fov_in_degree" << cv::Point2f(fov_[0] * R2D, fov_[1] * R2D);
    fs << "camera_matrix" << K_cam_;
    fs << "distortion" << distortion_;
    fs << "principal_point" << cv::Point2f(cx_, cy_);
    fs << "extrinsic_position" << position_;
    fs << "extrinsic_rotation_rpy" << rotation_rpy_;

    if (save_distortion)
    {
      fs << "undistortion_ray_lut" << lut_;
    }

    if (save_undistortion)
    {
      int w1 = w_/8;
      int h1 = h_/8;
      cv::Mat table = cv::Mat::zeros(h_ + 2*h1, w_ + 2*w1, CV_32FC2);

      for (int x=-w1; x < w_ + w1; x++)
      {
        for (int y=-h1; y < h_ + h1; y++)
        {
          *(cv::Point2f *)table.ptr<cv::Vec2f>(y+h1,x+w1) = Distort(cv::Point2f(x,y), 1.0f);
        }
      }

      fs << "extra_padding" << cv::Size(w1, h1);
      fs << "distortion_lut" << table;
    }

    return true;
  }


  void CameraModel::Log(int verbosity)
  {
    if (verbosity == 0) return;

    /*
    std::string log;
    VLOG(log, "{}", name_);
    VLOG(log, "  model_type   : {}", camera_calib_name(calib_type_));
    VLOG(log, "  image_size   : {} x {}", w_, h_);
    VLOG(log, "  image_roi    : {} {}, {} x {}",
              image_roi_.x, image_roi_.y, image_roi_.width, image_roi_.height);
    VLOG(log, "  K_mat        : {:.2f} {:.2f}, {:.2f} {:.2f}",
              K_cam_.at<float>(0,0), K_cam_.at<float>(1,1),
              K_cam_.at<float>(0,2), K_cam_.at<float>(1,2));
    VLOG(log, "  focal_length : {:.2f} {:.2f}", fx_, fy_);
    VLOG(log, "  center       : {:.2f} {:.2f}", cx_, cy_);
    VLOG(log, "  resize_factor: {:.3f}", resize_factor_);
    VLOG(log, "  distortion   : {:.3e} {:.3e} {:.3e} {:.3e} {:.3e}",
              distortion_.at<double>(0), distortion_.at<double>(1), distortion_.at<double>(2),
              distortion_.at<double>(3), distortion_.at<double>(4));
    VLOG(log, "  fov_h        : {:.2f} ~ {:.2f}, {:.2f} deg",
              fov_h_[0]*R2D, fov_h_[1]*R2D, fov_[0]*R2D);
    VLOG(log, "  fov_v        : {:.2f} ~ {:.2f}, {:.2f} deg",
              fov_v_[0]*R2D, fov_v_[1]*R2D, fov_[1]*R2D);
    VLOG(log, "  deg/pixel    : {:.4f} deg", radian_per_pixel_ * R2D);
    VLOG(log, "  extrinsic    : {:.2f} {:.2f}, {:.2f} meter, {:.2f} {:.2f} {:.2f} deg",
              position_[0], position_[1], position_[2],
              rotation_rpy_[0]*R2D, rotation_rpy_[1]*R2D, rotation_rpy_[2]*R2D);
    VLOG(log, "  look-up table: {}", lut_log_);
    VLOG(log, "");
    PHANTOM_LOG("{}", log);
    */
  }


  /////////////////////////////////////////////////////////////////////////////
  // Pinhole model
  //
  CameraModelPinhole::CameraModelPinhole(CameraID camera) :
    CameraModel(camera)
  {
    calib_type_ = CAMERA_CALIB_PINHOLE;
  }


  CameraModelPinhole::~CameraModelPinhole()
  {

  }


  void CameraModelPinhole::BuildLut(float scale)
  {
    resize_factor_ = scale;

    CLOCK(t0);
    cv::Mat grids = cv::Mat::zeros(w_ * h_, 1, CV_32FC2);
    for (int i=0; i < (int)grids.total(); i++)
    {
      grids.at<cv::Vec2f>(i,0)[0] = (float)(i % w_);
      grids.at<cv::Vec2f>(i,0)[1] = (float)(i / w_);
    }

    // Undistortion
    cv::undistortPoints(grids, lut_, K_cam_, distortion_);
    lut_ptr_ = reinterpret_cast<float *>(lut_.data);

    // Distortion
    CLOCK(t1);
    lut_distort_  = cv::Mat::zeros(w_ * h_, 1, CV_32FC2);
    lut_distort_ptr_ = reinterpret_cast<float *>(lut_distort_.data);
    for (int i=0; i < (int)grids.total(); i++)
    {
      const cv::Point2f* q = (const cv::Point2f *)grids.ptr<cv::Vec2f>(i);
      *(cv::Point2f *)lut_distort_.ptr<cv::Vec2f>(i) = DistortBySolve(*q, 1.0f);
    }

    CLOCK(t2);
    //lut_log_ = fmt::format("{:4.1f} + {:4.1f} msec", MSEC(t1-t0), MSEC(t2-t1));
  }


  cv::Point2f CameraModelPinhole::DistortBySolve(const cv::Point2f& q, float sign) const
  {
    (void)sign;

    const double& k1 = distortion_.at<double>(0);
    const double& k2 = distortion_.at<double>(1);
    const double& p1 = distortion_.at<double>(2);
    const double& p2 = distortion_.at<double>(3);
    const double& k3 = distortion_.at<double>(4);

    double x = (q.x - cx_) / fx_;
    double y = (q.y - cy_) / fy_;
    double r2 = x*x + y*y;

    double r_dist = (1 + r2 * (k1 + r2 * (k2 + k3*r2))); // radial distorsion
    double xd = x * r_dist;
    double yd = y * r_dist;

    xd = xd + (2 * p1*x*y + p2 * (r2 + 2*x*x)); // tangential distorsion
    yd = yd + (p1 * (r2 + 2*y*y) + 2 * p2*x*y);

    return cv::Point2f(fx_ * xd + cx_, fy_ * yd + cy_);
  }


  cv::Point2f CameraModelPinhole::NormalizeBySolve(const cv::Point2f& p) const
  {
    const int max_iterations = 20;
    const double& k1 = distortion_.at<double>(0);
    const double& k2 = distortion_.at<double>(1);
    const double& p1 = distortion_.at<double>(2);
    const double& p2 = distortion_.at<double>(3);
    const double& k3 = distortion_.at<double>(4);

    double x0, y0, x, y;
    x = x0 = (p.x - cx_) / fx_;
    y = y0 = (p.y - cy_) / fy_;

    // compensate distortion iteratively
    for (int i = 0; i < max_iterations; i++)
    {
      double r2 = x*x + y*y;
      double icdist = 1 / (1 + ((k3 * r2 + k2)*r2 + k1)*r2);
      double deltaX = 2 * p1*x*y + p2 * (r2 + 2*x*x);
      double deltaY = p1 * (r2 + 2*y*y) + 2 * p2*x*y;
      x = (x0 - deltaX) * icdist;
      y = (y0 - deltaY) * icdist;
    }

    return cv::Point2f(x, y);
  }


  /////////////////////////////////////////////////////////////////////////////
  // Fisheye Scaramuzza model
  //
  CameraModelScara::CameraModelScara(CameraID camera) :
    CameraModel(camera)
  {
    calib_type_ = CAMERA_CALIB_FISHEYE_SCARAMUZZA;
    d_rho_scale_ = 10.0;
  }


  CameraModelScara::~CameraModelScara()
  {

  }


  void CameraModelScara::BuildLut(float scale)
  {
    resize_factor_ = scale;
    CLOCK(t0);
    cv::Mat grids = cv::Mat::zeros(w_ * h_, 1, CV_32FC2);
    for (int i=0; i < (int)grids.total(); i++)
    {
      grids.at<cv::Vec2f>(i,0)[0] = (float)(i % w_);
      grids.at<cv::Vec2f>(i,0)[1] = (float)(i / w_);
    }

    // Undistortion
    lut_ = cv::Mat(grids.size(), grids.type());
    lut_ptr_ = reinterpret_cast<float *>(lut_.data);
    for (int i=0; i < (int)grids.total(); i++)
    {
      const cv::Point2f* p = (const cv::Point2f *)grids.ptr<cv::Vec2f>(i);
      *(cv::Point2f *)lut_.ptr<cv::Vec2f>(i) = NormalizeBySolve(*p);
    }

    // Distortion
    CLOCK(t1);
    int dm = 0;
    for (double d = 0.0; d < 1.5*std::hypot(w_/2, h_/2); d += 0.1)
    {
      double rho = FindRho(d, dm);
      rho_map_[dm] = rho;
    }

    CLOCK(t2);
    //lut_log_ = fmt::format("{:4.1f} + {:4.1f} msec, rho map size = {:.2f} percent.",
    //                    MSEC(t1-t0), MSEC(t2-t1), 100.0*rho_map_.size()/(w_*h_));
  }


  cv::Point2f CameraModelScara::DistortBySolve(const cv::Point2f& q, float sign) const
  {
    double u = (q.x - cx_);
    double v = (q.y - cy_);
    double d = sign * std::sqrt(u*u + v*v);
    int dm = std::round(d_rho_scale_ * d);

    double rho = std::nan("");
    auto im = rho_map_.find(dm);
    if (im != rho_map_.end())
    {
      rho = im->second;
    }
    else
    {
      rho = FindRho(d, dm);
    }

    if (std::isnan(rho))
    {
      //PHANTOM_WARNING("rho at ({:.2f}, {:.2f}}) is nan, d = {}", q.x, q.y, d);
      return cv::Point2f(0,0);
    }

    // consider the resize factor since rho & f(rho) are defined in the original image resolution.
    double f_rho = rho / d * resize_factor_;
    double px = u * f_rho;
    double py = v * f_rho;

    return cv::Point2f(SKEW(0,0)*px + SKEW(0,1)*py + cx_,
                       SKEW(1,0)*px + SKEW(1,1)*py + cy_);
  }


  cv::Point2f CameraModelScara::NormalizeBySolve(const cv::Point2f& p) const
  {
    // recover the pixel (x, y) in an original image space where rho & f_rho are defined.
    double x = (SKEW_INV(0,0) * (p.x - cx_) + SKEW_INV(0,1) * (p.y - cx_)) / resize_factor_;
    double y = (SKEW_INV(1,0) * (p.x - cy_) + SKEW_INV(1,1) * (p.y - cy_)) / resize_factor_;
    double rho = std::hypot(x, y);
    double f_rho = 0.0;
    for (int i = 0; i < (int)distortion_.total(); i++)
    {
      f_rho += distortion_.at<double>(i) * std::pow(rho, i);
    }

    // camera ray = (x, y, f_rho) is found in the original image resolution.
    return cv::Point2f(x / f_rho, y / f_rho);
  }


  double CameraModelScara::FindRho(double d, int& dm) const
  {
    const double EPS = 1e-10;
    const int max_iters = 100; // default 300

    double rho = std::nan("");
    dm = std::round(d_rho_scale_ * d);
    if (dm == 0)
    {
      return 0.0;
    }
    else
    {
      cv::Mat coeff = distortion_.clone();
      double m = (dm / d_rho_scale_) / fx_;
      coeff.at<double>(1) = -1.0/m;
      std::vector<cv::Complex<double>> roots;
      cv::solvePoly(coeff, roots, max_iters);  // solve for rho s.t. f(rho) - rho/m = 0

      for (int i=0; i < (int)roots.size(); i++)
      {
        if (std::abs(roots[i].im) < EPS && roots[i].re > 0.0)
        {
          if (std::isnan(rho)) rho = roots[i].re;
          else rho = std::min(rho, roots[i].re);
        }
      }

      return rho;
    }
  }


  /////////////////////////////////////////////////////////////////////////////
  // CameraModelList
  //
  CameraModelList::CameraModelList () {}

  CameraModelList::CameraModelList(int num_cameras) :
    num_cameras_(num_cameras),
    models_(num_cameras, nullptr)
  {

  }


  CameraModelList::~CameraModelList()
  {

  }


  bool CameraModelList::IsReady(const std::vector<CameraID>& cameras)
  {
    for (const auto& cam : cameras)
    {
      if (!models_[cam] || !models_[cam]->Valid()) return false;
    }
    return true;
  }


  bool CameraModelList::IsReady(const std::vector<std::vector<CameraID>>& cameras)
  {
    for (const auto& cams : cameras)
    {
      if (!IsReady(cams)) return false;
    }
    return true;
  }


  bool CameraModelList::Initialize(std::string param_file, std::string vehicle,
                                   std::pair<int, int> image_size, std::vector<int>& cameras)
  {
    printf("Initialize.....\n");
    cv::Size image_size_ = cv::Size(image_size.first, image_size.second);

    std::vector<CameraID> cameras_;
    cameras_.reserve(cameras.size());
    std::transform(cameras.begin(), cameras.end(), std::back_inserter(cameras_),
                   [](int n) { return static_cast<CameraID>(n); });
  
    std::vector<CameraInfo> camera_info;
    if (read_camera_model_params(param_file, vehicle, camera_info))
    {
      for (const auto cam : cameras_)
      {
        std::shared_ptr<CameraModel> model;
        
        printf("cam id : %d \n", cam);       

        CameraCalibTypeEnum type = camera_calib_name(camera_info[cam].calib_model_);
        if (type == CAMERA_CALIB_PINHOLE)
        {
          model = std::make_shared<CameraModelPinhole>(cam);
        }
        else if (type == CAMERA_CALIB_FISHEYE_SCARAMUZZA)
        {
          model = std::make_shared<CameraModelScara>(cam);
        }
        else
        {
          continue;
        }

        model->Initialize(camera_info[cam], image_size_);
        model->Log(1);
        camera_list.push_back(cam);
        models_.push_back(model);
      }
    }

    return IsReady(cameras_);
  }

  
  std::vector<float> CameraModelList::GetCoordFromWorld(int cam, float y, float width_m, float position_y, float range_m)
  {
    printf("CAM ID :: %d, Y :: %f\n", cam, y);
    float pitch = models_[cam]->PitchFromY(y);
    auto T_cam = models_[cam]->TransformFromPitch(pitch);
    
    float w = width_m;
    float h = w * 0.8;
    float angle = std::asin(position_y / range_m);
    cv::Point3f m(range_m * std::cos(angle), position_y, 0.0f);
    
    auto p0 = models_[cam]->Project({m.x, m.y + w/2, m.z    }, T_cam);
    auto p1 = models_[cam]->Project({m.x, m.y + w/2, m.z + h}, T_cam);
    auto p2 = models_[cam]->Project({m.x, m.y - w/2, m.z + h}, T_cam);
    auto p3 = models_[cam]->Project({m.x, m.y - w/2, m.z    }, T_cam);

    std::vector<float> box = {p1.x, p1.y, p3.x, p3.y};
    return box;
  }

  bool CameraModelList::GetCropViewport(CameraID main, CameraID crop, cv::Rect2f& roi)
  {
    if (!IN_RANGE(main, 0, (int)models_.size()) || !IN_RANGE(crop, 0, (int)models_.size()))
    {
      return false;
    }

    if (models_[main] && models_[crop] && is_main_crop_camera(main, crop))
    {
      cv::Size2f s0 = models_[main]->ImageSize();
      cv::Rect2f r0 = models_[main]->ImageRoi();
      cv::Rect2f r1 = models_[crop]->ImageRoi();
      float sx = s0.width / r0.width;
      float sy = s0.height / r0.height;

      roi = cv::Rect2f((r1.x - r0.x) * sx, (r1.y - r0.y) * sy, r1.width * sx, r1.height * sy);
      return true;
    }
    else
    {
      return false;
    }
  }


  /*void CameraModelList::Visualize(bool b_show)
  {
    const std::string wnd_name = "camera_calib_models";
    for (const auto& model : models_)
    {
      if (b_show && model && model->Valid())
      {
        cv::imshow(wnd_name, model->Visualize());
        cv::waitKey(0);
        cv::destroyWindow(wnd_name);
      }
    }
  }*/

  
  void CameraModelList::Save()
  {
    for (const auto& model : models_)
    {
      if (model && model->Valid())
      {
        //model->Save(fmt::format("{}.yaml", model->Name()));
        model->Save("test.yaml");
      }
    }
  }

  //
  //
  //
  CameraCalibTypeEnum camera_calib_name(std::string name, bool warning)
  {
    if      (name == "pinhole")             return CAMERA_CALIB_PINHOLE           ;
    else if (name == "fisheye_scaramuzza")  return CAMERA_CALIB_FISHEYE_SCARAMUZZA;
    else if (name == "fisheye")             return CAMERA_CALIB_FISHEYE_SCARAMUZZA;
    else if (name == "invalid")             return CAMERA_CALIB_INVALID           ;
    else {
      //PHANTOM_ERROR_IF(warning, "Invalid camera model type name: {}", name);
      return CAMERA_CALIB_INVALID;
    }
  }


  std::string camera_calib_name(CameraCalibTypeEnum type, bool warning)
  {
    switch (type)
    {
      case CAMERA_CALIB_PINHOLE            :  return "pinhole";
      case CAMERA_CALIB_FISHEYE_SCARAMUZZA :  return "fisheye_scaramuzza";
      case CAMERA_CALIB_INVALID            :  return "invalid";
      default                          :
        //PHANTOM_ERROR_IF(warning, "Invalid camera model type: {}", type);
        return "unknown";
    }
  }


  bool read_camera_model_params(std::string param_file, std::string vehicle_name,
                                std::vector<CameraInfo>& camera_info_list)
  {
    camera_info_list.clear();
    camera_info_list.resize(NUM_CAM_DIRS);

    //PHANTOM_ERROR("{}\n", param_file);
    //PHANTOM_ERROR("{}\n", vehicle_name);

    cv::FileStorage fs;
    fs.open(param_file, cv::FileStorage::READ);
    if (!fs.isOpened())
    {
      //PHANTOM_ERROR("Cannot open '{}'.\n", param_file);
      return false;
    }

    cv::FileNode veh_node = fs[vehicle_name];
    if (veh_node.empty())
    {
      //PHANTOM_ERROR("Cannot find a vehicle '{}'.\n", vehicle_name);
      return false;
    }

    for (int cam = 0; cam < NUM_CAM_DIRS; cam++)
    {
      std::string cam_name = camera_name(CameraID(cam));
      cv::FileNode node = veh_node[cam_name];
      if (node.empty())
      {
        continue;
      }

      CameraInfo info;
      info.name_ = cam_name;
      info.serial_number_ = node["serial_number"].string();
      info.sensor_type_ = node["sensor_type"].string();
      info.calib_model_ = node["calib_model"].string();

      info.vehicle_ = node["vehicle"].string();
      info.location_ = node["location"].string();
      info.comment_ = node["comment"].string();
      info.horizontal_flip_ = node["horizontal_flip"].string() == "true";
      info.lens_fov_ = node["lens_fov"].real();
      
      #define READ_ARRAY(node, y)  { int i=0; for (auto x : node) y[i++] = x.real(); }
      #define READ_VECTOR(node, y) { int i=0; for (auto x : node) y.push_back(x.real()); }
      #define READ_RECT(node, y)   { y = cv::Rect(node[0].real(), node[1].real(), node[2].real(), node[3].real()); }

      READ_VECTOR(node["position_meter"], info.position_meter_)
      READ_VECTOR(node["rotation_deg"], info.rotation_rad_)
      std::for_each(info.rotation_rad_.begin(), info.rotation_rad_.end(), [](float &x){ x *= (M_PI/180); });

      std::vector<float> m;
      std::vector<float> d;
      std::vector<float> s;
      std::vector<int> r;
      std::vector<int> c;
      READ_VECTOR(node["camera_matrix"], m)
      READ_VECTOR(node["distortion"], d)
      READ_VECTOR(node["skew"], s)
      READ_VECTOR(node["resolution"], r)
      READ_VECTOR(node["crop_roi"], c)

      if (m.size() == 9 && s.size() == 4 && r.size() == 2 && c.size() == 4)
      {
        info.camera_matrix_ = cv::Mat(3, 3, CV_32F, m.data()).clone();
        info.distortion_ = cv::Mat(1, d.size(), CV_32F, d.data()).clone();
        info.skew_ = cv::Mat(2, 2, CV_32F, s.data()).clone();
        info.resolution_ = cv::Size(r[0], r[1]);
        info.crop_roi_ = cv::Rect(c[0], c[1], c[2], c[3]);
      }
      else
      {
        //PHANTOM_ERROR("cam{}: {}, param size does not match.", cam, info.name_);
      }

      camera_info_list[cam] = info;
      PHANTOM_LOG_IF(0, "cam{}: {}", cam, info.name_);

      //
      // add the secondary crop region as an additional virtual camera
      //
      std::vector<cv::Rect> second_crop_roi;
      for (auto n : node["second_crop_roi"])
      {
        cv::Rect roi;
        READ_RECT(n, roi);
        second_crop_roi.push_back(roi);
      }

      if (second_crop_roi.size() == 2)
      {
        if (second_crop_roi[0].area() > 0)
        {
          info.name_ = cam_name + "_crop";
          info.location_ = info.location_ + "_crop";
          info.crop_roi_ = second_crop_roi[0];
          info.crop_roi_extra_ = second_crop_roi[1];

          auto crop_cam = camera_name(info.name_);
          if (crop_cam < NUM_CAM_DIRS)
          {
            camera_info_list[crop_cam] = info;
            PHANTOM_LOG_IF(0, "cam{}: {}", crop_cam, info.name_);
          }
        }
      }
    }

    return true;
  }


}
