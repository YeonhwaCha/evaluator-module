/*******************************************************************************
* @file    geometry_utils.h
* @date    04/17/2020
*
* @attention Copyright (c) 2017
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#ifndef __PHANTOMVISION2_GEOMETRY_UTILS_H__
#define __PHANTOMVISION2_GEOMETRY_UTILS_H__

#include "vision_constants.h"
#include <numeric> // std::accumulate

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>


#ifndef R2D
#define R2D 57.2957795131f
#endif

#ifndef D2R
#define D2R 0.01745329251f
#endif

#define F_PI  3.14159265358979323846f

#define TWO_PI(x)   ((x) < 0) ? (x) + 2*F_PI : (x)
#define PI2PI(x)    (((x) < -F_PI) ? (x) + 2*F_PI : ((x) > F_PI) ? (x) - 2*F_PI : (x))

#define SQR(x)    ((x)*(x))
#define SQRT(x)   std::sqrt(x)

namespace phantom_ai {
namespace geom {

  template <class T>
  inline void print_mat(const std::string& str, cv::Mat_<T> M) //, int width = 14, int precision = 8)
  {
    fmt::print("{}, {} x {}, depth = {}, ch = {} \n",
      str, M.rows, M.cols, CV_MAT_DEPTH(M.type()), CV_MAT_CN(M.type()));
    //std::string f = fmt::format("{:{}.{}f}", width, precision);
    for (int i = 0; i< M.size().height; i++)
    {
      for (int j = 0; j < M.size().width; j++)
      {
        fmt::print("{:12.8f}", M.template at<T>(i, j));
      }
      fmt::print(";\n");
    }
  }


  template <class T>
  T intersection_of_union(const cv::Rect_<T>& a, const cv::Rect_<T>& b)
  {
    T w = std::min(a.x + a.width, b.x + b.width) - std::max(a.x, b.x);
    T h = std::min(a.y + a.height, b.y + b.height) - std::max(a.y, b.y);
    T c = w * h;

    return (w <= T(0) || h <= T(0)) ? T(0) : (c / (a.area() + b.area() - c));
  }


  template <class T>
  inline cv::Rect_<T> transform_rect(const cv::Rect_<T>& r,
                                    const cv::Size& img_size,
                                    const cv::Rect_<T>& c_roi)
  {
    //  +----------+   img_size = [w,h]
    //  |   +----+ |   r_in: coordinates in [0,0,w,h]
    //  |   | r  | |   r_out: coordinates in c_roi after scaling up [0,0,w,h]
    //  |   +----+ |
    //  +----------+
    float sx = img_size.width / c_roi.width;
    float sy = img_size.height / c_roi.height;
    return cv::Rect_<T>((r.x - c_roi.x)* sx, (r.y - c_roi.y) * sy,
                          r.width * sx, r.height * sy);
  }

  template <class T>
  inline cv::Rect_<T> transform_rect(const cv::Rect_<T>& r,
                                    const cv::Rect_<T>& c_roi,
                                    const cv::Size& img_size)
  {
    float sx = c_roi.width / img_size.width;
    float sy = c_roi.height / img_size.height;

    return cv::Rect_<T>(r.x * sx + c_roi.x, r.y * sy + c_roi.y,
                        r.width * sx, r.height * sy);
  }


  template <class T>
  inline cv::Point_<T> corner(const cv::Rect_<T>& r, int i)
  {
    return cv::Point_<T>(r.x + (i % 2)*r.width, r.y + (i/2)*r.height);
  }

  template <class T>
  inline cv::Point_<T> center(const cv::Rect_<T>& r)
  {
    return cv::Point_<T>(r.x + r.width/2, r.y + r.height/2);
  }

  template <class T>
  inline cv::Point_<T> bottom_center(const cv::Rect_<T>& r)
  {
    return cv::Point_<T>(r.x + r.width/2, r.y + r.height);
  }

  template <class T>
  inline cv::Point_<T> bottom_left(const cv::Rect_<T>& r)
  {
    return cv::Point_<T>(r.x, r.y + r.height);
  }

  template <class T>
  inline cv::Point_<T> bottom_right(const cv::Rect_<T>& r)
  {
    return cv::Point_<T>(r.x + r.width, r.y + r.height);
  }

  template <class T>
  inline cv::Point_<T> middle_left(const cv::Rect_<T>& r)
  {
    return cv::Point_<T>(r.x, r.y + r.height/2);
  }

  template <class T>
  inline cv::Point_<T> middle_right(const cv::Rect_<T>& r)
  {
    return cv::Point_<T>(r.x + r.width, r.y + r.height/2);
  }

  template <class T>
  inline cv::Point_<T> top_center(const cv::Rect_<T>& r)
  {
    return cv::Point_<T>(r.x + r.width/2, r.y);
  }

  template <class T>
  inline cv::Point_<T> top_left(const cv::Rect_<T>& r)
  {
    return cv::Point_<T>(r.x, r.y);
  }

  template <class T>
  inline cv::Point_<T> top_right(const cv::Rect_<T>& r)
  {
    return cv::Point_<T>(r.x + r.width, r.y);
  }

  template <class T>
  inline std::vector<cv::Point_<T>> bottom_corners(const cv::Rect_<T>& r)
  {
    return {{r.x,           r.y + r.height},
            {r.x + r.width, r.y + r.height}};
  }

  inline cv::Rect rect_padded(const cv::Rect2f& r, const cv::Size& size, float s)
  {
    return cv::Rect(cv::Point(std::max((int)(r.x - r.width*s),      0),
                              std::max((int)(r.y - r.height*s),     0)),
                    cv::Point(std::min((int)(r.x + r.width*(1+s)),  size.width-1),
                              std::min((int)(r.y + r.height*(1+s)), size.height-1)));
  }

  inline cv::Rect rect_scaled(const cv::Size& m, float s)
  {
    int w = (1-s)/2*m.width;
    int h = (1-s)/2*m.height;
    return cv::Rect(w, h, m.width - 2*w, m.height - 2*h);
  }


  inline bool copy_mat(const cv::Mat& src, cv::Mat dst)
  {
    if (src.empty()) return false;
    src.copyTo(dst);
    return true;
  }


  inline int mat_mean_stdev(const cv::Mat& A,
                            float confidence_level,
                            float& mean, float& stdev,
                            cv::Mat mask = cv::Mat())
  {
    cv::Scalar m, sd;
    cv::meanStdDev(A, m, sd, mask);

    cv::Mat mask_c = cv::abs(A - m.val[0]) < confidence_level * sd.val[0];
    if (!mask.empty()) mask_c = mask & mask_c;

    cv::meanStdDev(A, m, sd, mask_c);
    mean = m.val[0];
    stdev = sd.val[0];

    return cv::countNonZero(mask_c);
  }


  inline int mat_mean_stdev(const cv::Mat& A,
                            float& mean,
                            float& stdev,
                            cv::Mat mask = cv::Mat())
  {
    cv::Scalar m, sd;
    cv::meanStdDev(A, m, sd, mask);
    mean = m.val[0];
    stdev = sd.val[0];

    return A.rows;
  }


  template <class T>
  inline int compute_mean_stdev(const T* v, int size, T& mean, T& stdev)
  {
    if (size > 0)
    {
      T sum = std::accumulate(v, v + size, T(0));
      mean = sum / size;

      std::vector<T> e(size);
      std::transform(v, v + size, e.begin(), [mean](const float& x) { return (x - mean); });
      stdev = std::sqrt(std::inner_product(e.begin(), e.end(), e.begin(), T(0)) / e.size());
      return size;
    }

    return 0;
  }


  template <class T>
  inline int compute_mean_stdev(const std::vector<T>& v, T& mean, T& stdev)
  {
    if (!v.empty())
    {
      T sum = std::accumulate(v.begin(), v.end(), T(0));
      mean = sum / v.size();

      std::vector<T> e(v.size());
      std::transform(v.begin(), v.end(), e.begin(), [mean](const float& x) { return (x - mean); });
      stdev = std::sqrt(std::inner_product(e.begin(), e.end(), e.begin(), T(0)) / e.size());
    }

    return (int)v.size();
  }


  template <class T>
  inline int compute_mean_stdev(const std::vector<T>& v, T confidence_level, T& mean, T& stdev)
  {
    if (compute_mean_stdev<T>(v, mean, stdev))
    {
      T sigma = confidence_level * stdev;
      std::vector<T> w;
      for (const auto& a : v)
      {
        if (std::abs(a - mean) < sigma) w.push_back(a);
      }
      return compute_mean_stdev<T>(w, mean, stdev);
    }

    return (int)v.size();
  }



  template <class T>
  inline bool compute_line_y_intercept_stdev(cv::Mat_<T> data, T m, T b, T& stdev)
  {
    // https://www.chem.utoronto.ca/coursenotes/analsci/stats/ErrRegr.html
    // http://www.chem.utoronto.ca/coursenotes/analsci/LinRegr2b.pdf
    // data = [x, y] and y = m * x + b
    int n = data.rows;
    if (n > 2)
    {
#if 0
      cv::Scalar mean_value, std_value;
      cv::meanStdDev(data.col(0), mean_value, std_value);

      // y = m*x + b, err = y - y_hat = y - m*x - b
      // s_yx = sqrt(err_sqsum/())
      cv::Mat Err = data * (cv::Mat_<T>(2,1) << -m, T(1)) - b;
      T err_sqsum = Err.dot(Err);

      T xm = mean_value[0];
      T SSxx = SQR(std_value[0]) * n;
      T Syx = T(1)/(n-2) * err_sqsum;
      stdev = SQR(Syx)*(T(1)/n + xm/SSxx);
#else
      cv::Scalar mean_value, std_value;
      cv::meanStdDev(data.col(0), mean_value, std_value);

      // y = m*x + b, err = y - y_hat = y - m*x - b
      // s_yx = sqrt(err_sqsum/())
      cv::Mat Y_err = data * (cv::Mat_<T>(2,1) << -m, T(1)) - b;
      T S_yx = std::sqrt(Y_err.dot(Y_err)/(n-2));
      T X_var = T(1)/n*(1 + SQR(mean_value[0]/std_value[0]));
      stdev = S_yx * std::sqrt(X_var);
#endif
      return true;
    }
    return false;
  }

  /////////////////////////////////////////////////////////////////////////////
  // Transformation matrix and coordinate
  //
  template <class T>
  inline cv::Vec<T,3> rot_rpy(const cv::Mat_<T> R)
  {
    return cv::Vec<T,3>(std::atan2(R.template at<T>(2,1), R.template at<T>(2,2)),
                        std::asin(-R.template at<T>(2,0)),
                        std::atan2(R.template at<T>(1,0), R.template at<T>(0,0)));
  }


  template <class T>
  inline cv::Mat R_rpy(const T* rpy)
  {
    T c1 = std::cos(rpy[0]), s1 = std::sin(rpy[0]);
    T c2 = std::cos(rpy[1]), s2 = std::sin(rpy[1]);
    T c3 = std::cos(rpy[2]), s3 = std::sin(rpy[2]);

    /// R = Rz(3) * Ry(2) * Rx(1)
    /// http://planning.cs.uiuc.edu/node102.html
    cv::Mat_<T> R_mat = (cv::Mat_<T>(3, 3) <<
                     c2*c3, -c1*s3 + s1*s2*c3,  s1*s3 + c1*s2*c3,
                     c2*s3,  c1*c3 + s1*s2*s3, -s1*c3 + c1*s2*s3,
                     -s2,    s1*c2,             c1*c2);
    return R_mat;
  }


  template <class T>
  inline cv::Mat R_rpy(const std::vector<T>& rpy)
  {
    return R_rpy<T>(rpy.data());
  }


  template <class T>
  inline cv::Mat R_rpy(const cv::Vec<T,3>& rpy)
  {
    return R_rpy<T>(&rpy[0]);
  }


  template <class T>
  inline cv::Mat T_4x4(const T* rpy, const T* pos)
  {
    T c1 = std::cos(rpy[0]), s1 = std::sin(rpy[0]);
    T c2 = std::cos(rpy[1]), s2 = std::sin(rpy[1]);
    T c3 = std::cos(rpy[2]), s3 = std::sin(rpy[2]);

    cv::Mat T_mat = (cv::Mat_<T>(4, 4) <<
                     c2*c3, -c1*s3 + s1*s2*c3,  s1*s3 + c1*s2*c3, pos[0],
                     c2*s3,  c1*c3 + s1*s2*s3, -s1*c3 + c1*s2*s3, pos[1],
                     -s2,    s1*c2,             c1*c2,            pos[2],
                     0, 0, 0, 1);
    return T_mat;
  }


  template <class T>
  inline cv::Mat T_4x4(const cv::Vec<T,3>& rpy, const cv::Vec<T,3>& pos)
  {
    return T_4x4<T>(&rpy[0], &pos[0]);
  }


  template <class T>
  inline cv::Mat compute_homography(const cv::Mat_<T>& KK, const cv::Mat_<T>& T_cam, int verbosity = 0)
  {
    // A series of coordinate transformations
    //
    //  {C} ----> {N} ----> {B} ----> {R} ----> {W}
    //   xc        xn        xb        xr        xw
    //   yc        yn        yb        yr        yw
    //   1         1         zb        zr        zw
    //
    //  H_a2b means a transformation from a point in {A} to that in {B}; reading subscripts from right to left
    //  [xg yg 1] = H_w2c * [xc yc 1]

    T x_cam = T_cam.template at<T>(0,3);
    T y_cam = T_cam.template at<T>(1,3);
    T h_cam = T_cam.template at<T>(2,3);

    cv::Mat H_w2r = (cv::Mat_<T>(3, 3) << 1,0,-x_cam/h_cam,  0,1,-y_cam/h_cam,  0,0,-1/h_cam);
    cv::Mat H_r2b = T_cam(cv::Rect(0,0,3,3));
    cv::Mat H_b2n = (cv::Mat_<T>(3, 3) << 0,0,1,  -1,0,0,  0,-1,0);
    cv::Mat H_n2c = KK.inv();

    cv::Mat H_w2c = H_w2r * H_r2b * H_b2n * H_n2c;

    /*if (verbosity > 0)
    {
      print_mat<T>("H_w2r = ", H_w2r);
      print_mat<T>("H_r2b = ", H_r2b);
      print_mat<T>("H_b2n = ", H_b2n);
      print_mat<T>("H_n2c = ", H_n2c);
      print_mat<T>("H_w2c = ", H_w2c);
    }*/

    return H_w2c;
  }


  template <class T>
  inline void transform_point(const cv::Mat_<T>& T_4x4, const cv::Point3f& p, cv::Point3f& q)
  {
    q.x = T_4x4.template at<T>(0,0) * p.x + T_4x4.template at<T>(0,1) * p.y +
          T_4x4.template at<T>(0,2) * p.z + T_4x4.template at<T>(0,3);
    q.y = T_4x4.template at<T>(1,0) * p.x + T_4x4.template at<T>(1,1) * p.y +
          T_4x4.template at<T>(1,2) * p.z + T_4x4.template at<T>(1,3);
    q.z = T_4x4.template at<T>(2,0) * p.x + T_4x4.template at<T>(2,1) * p.y +
          T_4x4.template at<T>(2,2) * p.z + T_4x4.template at<T>(2,3);
  }


  template <class T>
  inline cv::Point3f transform_point(const cv::Mat_<T>& T_4x4, const cv::Point3f& p)
  {
    return cv::Point3f(
        T_4x4.template at<T>(0,0) * p.x + T_4x4.template at<T>(0,1) * p.y +
        T_4x4.template at<T>(0,2) * p.z + T_4x4.template at<T>(0,3),
        T_4x4.template at<T>(1,0) * p.x + T_4x4.template at<T>(1,1) * p.y +
        T_4x4.template at<T>(1,2) * p.z + T_4x4.template at<T>(1,3),
        T_4x4.template at<T>(2,0) * p.x + T_4x4.template at<T>(2,1) * p.y +
        T_4x4.template at<T>(2,2) * p.z + T_4x4.template at<T>(2,3));
  }


  template <class T>
  inline void warp_perspective(const cv::Mat_<T>& H, const cv::Point_<T>& p, cv::Point_<T>& q)
  {
    T x = H.template at<T>(0,0) * p.x + H.template at<T>(0,1) * p.y + H.template at<T>(0,2);
    T y = H.template at<T>(1,0) * p.x + H.template at<T>(1,1) * p.y + H.template at<T>(1,2);
    T z = H.template at<T>(2,0) * p.x + H.template at<T>(2,1) * p.y + H.template at<T>(2,2);

    if (std::abs(z) < 1e-10f) {
      q.x = q.y = 0.0f;
    }
    else {
      q.x = x/z;  q.y = y/z;
    }
  }


  template <class T>
  inline cv::Point_<T> warp_perspective(const cv::Mat_<T>& H, const cv::Point_<T>& p)
  {
    T x = H.template at<T>(0,0) * p.x + H.template at<T>(0,1) * p.y + H.template at<T>(0,2);
    T y = H.template at<T>(1,0) * p.x + H.template at<T>(1,1) * p.y + H.template at<T>(1,2);
    T z = H.template at<T>(2,0) * p.x + H.template at<T>(2,1) * p.y + H.template at<T>(2,2);

    if (std::abs(z) < 1e-10f)  cv::Point_<T>(T(0), T(0));
    return cv::Point_<T>(x/z, y/z);
  }


  template <class T>
  inline std::vector<cv::Point_<T>> warp_perspective(const cv::Mat_<T>& H, const std::vector<cv::Point_<T>>& p)
  {
    std::vector<cv::Point_<T>> q(p.size());
    for (size_t i=0; i < p.size(); i++) q[i] = warp_perspective<T>(H, p[i]);
    return q;
  }


  template <class T>
  inline cv::Point3_<T> warp_perspective(const cv::Mat_<T>& H, const cv::Point3_<T>& p)
  {
    T x = H.template at<T>(0,0) * p.x + H.template at<T>(0,1) * p.y + H.template at<T>(0,2);
    T y = H.template at<T>(1,0) * p.x + H.template at<T>(1,1) * p.y + H.template at<T>(1,2);
    T z = H.template at<T>(2,0) * p.x + H.template at<T>(2,1) * p.y + H.template at<T>(2,2);

    return cv::Point3_<T>(x, y, z);
  }


  template <class T>
  inline std::vector<cv::Point3_<T>> warp_perspective(const cv::Mat_<T>& H, const std::vector<cv::Point3_<T>>& p)
  {
    std::vector<cv::Point3_<T>> q(p.size());
    for (size_t i=0; i < p.size(); i++) q[i] = warp_perspective<T>(H, p[i]);
    return q;
  }


  inline void warp_perspective(const cv::Mat& H, const cv::Mat& r_pts, cv::Mat& w_pts, int num_points = -1)
  {
    if (num_points > 0 && num_points < r_pts.rows)
    {
      cv::Mat r = r_pts.rowRange(0, num_points);
      cv::Mat w = w_pts.rowRange(0, num_points);
      cv::perspectiveTransform(r, w, H);
    }
    else
    {
      cv::perspectiveTransform(r_pts, w_pts, H);
    }
  }


  inline void warp_line(const cv::Mat& H, const cv::Vec3f& line_eqn, cv::Vec3f& w_line_eqn)
  {
    cv::Mat W(w_line_eqn, false);
    W = H.inv().t() * cv::Mat(line_eqn, false);
  }


  inline cv::Moments line_moments(cv::Mat M)
  {
    cv::Moments m;
    if (M.channels() != 1)
    {
      vprint(1, "cv::Mat {} is not supported yet.\n", cv_mat_type_name(M));
      return m;
    }

    for (int k=0; k < M.cols/2; k++)
    {
      cv::Mat D = M.colRange(2*k, 2*(k+1));
      cv::Mat D2 = D.t() * D;

      cv::Mat S(1, 2, D.type());
      cv::reduce(D, S, 0, CV_REDUCE_SUM); // '0' reduces to a single row, '1' to column.

      m.m00 += D.rows;
      m.m10 += S.at<float>(0,0);
      m.m01 += S.at<float>(0,1);

      m.m20 += D2.at<float>(0,0);
      m.m02 += D2.at<float>(1,1);
      m.m11 += D2.at<float>(0,1);
    }

    m.mu20 = m.m20 - m.m10 * m.m10 / m.m00;
    m.mu11 = m.m11 - m.m10 * m.m01 / m.m00;
    m.mu02 = m.m02 - m.m01 * m.m01 / m.m00;

    return m;
  }


  inline void line_moments_fit(cv::Mat M, cv::Vec3f& line_eqn, cv::Size2f& line_axis,
                               cv::Point2f& line_center, cv::Point2f offset = {0,0})
  {
    cv::Moments m = line_moments(M);

    cv::Point2f mass_center(m.m10 / m.m00, m.m01 / m.m00);
    cv::Mat cov = (cv::Mat_<float>(2,2) << m.mu20, m.mu11, m.mu11, m.mu02) / m.m00;
    cv::Mat eigen_val, eigen_vec;
    cv::eigen(cov, eigen_val, eigen_vec);

    float chi = 4.3f;
    line_axis = cv::Size2f(chi * sqrt(eigen_val.at<float>(0)), chi * sqrt(eigen_val.at<float>(1)));
    line_center = mass_center + offset;

    float aa =  eigen_vec.at<float>(0,1);
    float bb = -eigen_vec.at<float>(0,0);
    line_eqn[0] = aa;
    line_eqn[1] = bb;
    line_eqn[2] = -aa * line_center.x - bb * line_center.y;
  }


  template <class T>
  inline cv::Point2f vanishing_point(const cv::Mat_<T>& H_c2w) // x_c = H_c2w * x_w
  {
    float vx = H_c2w.template at<T>(0,0)/H_c2w.template at<T>(2,0);
    float vy = H_c2w.template at<T>(1,0)/H_c2w.template at<T>(2,0);
    return cv::Point2f(vx, vy);
  }


} // namespace geom
} // namespace phantom_ai

#endif
