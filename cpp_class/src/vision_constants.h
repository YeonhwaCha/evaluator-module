/*******************************************************************************
* @file    vision_constants.h
* @date    02/12/2020
*
* @attention Copyright (c) 2017
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#ifndef __PHANTOMVISION2_VISION_CONSTANTS_H__
#define __PHANTOMVISION2_VISION_CONSTANTS_H__

//#include "color_names.h"
#include <fmt/core.h>

#include <cstdint>
#include <chrono>
#include <opencv2/core.hpp>

#ifndef ATOMIC_MUTEX
#define ATOMIC_MUTEX(a) \
  for (auto lock = std::unique_lock<std::decay<decltype(a)>::type>(a); lock; lock.unlock())
#endif

//
// Extra additions to the original header
//
#define PHANTOM_ERROR(str, ...)       vprint(1, str, ##__VA_ARGS__)
#define PHANTOM_ERROR_IF(v, str, ...) vprint(v, str, ##__VA_ARGS__)
#define PHANTOM_WARNING               PHANTOM_ERROR
#define PHANTOM_WARNING_IF            PHANTOM_ERROR_IF
#define PHANTOM_LOG                   PHANTOM_ERROR
#define PHANTOM_LOG_IF                PHANTOM_ERROR_IF
// End of extra additions

#define vprint(v, str, ...) \
  do { if (v) fmt::print(str, ##__VA_ARGS__); } while(0);

#define VLOG(s, str, ...) \
  do { s += "\n" + fmt::format(str, ##__VA_ARGS__); } while(0);

#define OX(v) \
  ((v) ? "O" : "X")

#define CLAMP(x, low, high) \
  ((x) < (low)) ? (low) : ((high) < (x)) ? (high) : (x)

#define CLAMP_ABS(x, high) \
  ((x) < -(high)) ? -(high) : ((high) < (x)) ? (high) : (x)

#define IN_RANGE(x, low, high) \
  ((low) <= (x) && (x) < (high))

#define VALID_MAT(A)  (!A.empty())

#define RETURN_IF(cond) \
    if (cond) return;

#define RETURN_TRUE_IF(cond) \
    if (cond) return false;

#define RETURN_FALSE_IF(cond) \
    if (cond) return false;

#define RETURN_VALUE_IF(cond, ret) \
    if (cond) return ret;

#define GET_MAP_VALUE(m, key, val) \
    (m.find(key) == m.end() ? val : m.at(key))

#define MAKE_ROLLING_COUNTER(x) \
  do { x = x % 0x3FFFFFFF; } while(0);

#define INVALID_BLOB_ID             (-1)
#define INVALID_GROUP_ID            (-1)
#define INVALID_TRACKER_ID          (-1)

#define CAMERA_FLAG(cam)            (0x1 << cam)
#define T_NOW_SEC                   (0.001*std::chrono::duration_cast<std::chrono::milliseconds> \
                                      (std::chrono::system_clock::now().time_since_epoch()).count())
#define MSEC(t)                     ((t) * 1e3)
#define CLOCK(t)                    double t = T_NOW_SEC;
#define CLOCK_SUM(t0, t1, ts)       double t1 = T_NOW_SEC; ts += (t1 - t0);

#define SLEEP_MSEC_PER_TASK_CYCLE   (4)
#define SLEEP_MSEC_MAIN_TASK_CYCLE  (10)
#define MAX_REWINDED_TIME_SEC       (-2.0)

#define SEG_NAME_HEADER           "s_"
#define SEG_PARSER_HEADER         "segment"

#define SEG_BACKGROUND            0
#define SEG_LANE_DASHED           1
#define SEG_LANE_SOLID            2
#define SEG_ROAD_EDGE             3
#define SEG_VEHICLE               4
#define SEG_WHEEL                 5
#define SEG_WHEEL_FRONT           6
#define SEG_WHEEL_REAR            7
#define SEG_CYCLIST               8
#define SEG_PEDESTRIAN            9
#define SEG_FREESPACE             10
#define SEG_BOUNDARY              11
#define SEG_CART                  12
#define SEG_STOPPER               13
#define SEG_INVALID               14
#define SEG_CLASSES_NUM           15
#define SEG_COLOR_LIST            {CV_GRAY25,     CV_DARKORANGE,  CV_RED/*CV_TOMATO*/,     CV_LIGHTGREEN,  CV_GREEN, \
                                   CV_CYAN,       CV_DARKCYAN,    CV_TEAL,    CV_YELLOWGREEN, CV_RED,   \
                                   CV_MEDIUMBLUE, CV_BLACK,       CV_MAGENTA, CV_GREEN,       CV_WHITE}

#define BBOX_UNKNOWN              0
#define BBOX_VEHICLE              1
#define BBOX_VEHICLE_SEDAN        2
#define BBOX_VEHICLE_SUV          3
#define BBOX_VEHICLE_VAN          4
#define BBOX_VEHICLE_TRUCK        5
#define BBOX_VEHICLE_TRAILER      6
#define BBOX_VEHICLE_BUS          7
#define BBOX_VEHICLE_GLARE        8
#define BBOX_VEHICLE_OTHER        9
#define BBOX_CART                 10
#define BBOX_VEHICLE_FACE_REAR    11
#define BBOX_VEHICLE_FACE_FRONT   12
#define BBOX_WHEEL                13
#define BBOX_GENERAL              14
#define BBOX_BICYCLE              15
#define BBOX_MOTORCYCLE           16
#define BBOX_CYCLIST              17
#define BBOX_MOTORCYCLIST         18
#define BBOX_PEDESTRIAN           19
#define BBOX_STROLLER             20
#define BBOX_STOPPER              21
#define BBOX_POLE                 22
#define BBOX_CLASSES_NUM          23
#define BBOX_COLOR_LIST           {CV_BLACK,   CV_WHITE,     CV_WHITE,     CV_WHITE,   CV_WHITE,     \
                                   CV_WHITE,   CV_WHITE,     CV_WHITE,     CV_WHITE,   CV_GRAY50,    \
                                   CV_WHITE,   CV_CYAN,      CV_CYAN,      CV_CYAN,    CV_RED,       \
                                   CV_YELLOW,  CV_YELLOW,    CV_RED,       CV_RED,     CV_RED,       \
                                   CV_YELLOW,  CV_CYAN,      CV_CYAN}

#define BBOX_GROUP_UNKNOWN        0
#define BBOX_GROUP_VEHICLE        1
#define BBOX_GROUP_FACE           2
#define BBOX_GROUP_WHEEL          3
#define BBOX_GROUP_CYCLE          4
#define BBOX_GROUP_PEDESTRIAN     5
#define BBOX_GROUP_GENERAL        6
#define BBOX_GROUP_MISC           7
#define BBOX_GROUP_OTHER          8
#define BBOX_GROUP_NUM            9

#define PHANTOMNET_DATA_BLURNESS  0
#define PHANTOMNET_DATA_BLOCKAGE  1
#define PHANTOMNET_DATA_HORIZON   2
#define PHANTOMNET_DATA_NUM       3

namespace phantom_ai
{
  enum class VisionTaskState
  {
    UNINITIALIZED,
    FAULT,
    RESET,
    RUN,
    PAUSE,
    EXIT,
    KILL
  };

  enum VisionTaskID
  {
    TASK_INVALID = 0,
    TASK_CAMERA,
    TASK_PHANTOMNET_0,
    TASK_PHANTOMNET_1,
    TASK_PHANTOMNET_2,
    TASK_PHANTOMNET_3,
    TASK_FEATURE_TRACK,
    TASK_OPTICAL_FLOW,
    TASK_CAMERA_MOTION,
    TASK_LANE_TRACK,
    TASK_OBJECT_TRACK,
    TASK_MESSAGE_PUBLISH,
    TASK_VISUALIZER,
    TASK_ROS_NODE,
    NUM_VISION_TASKS
  };

  enum VisionMessageID
  {
    MESSAGE_CAMERAS = 0,
    MESSAGE_PHANTOMNET,
    MESSAGE_FEATURE_TRACK,
    MESSAGE_OPTICAL_FLOW,
    MESSAGE_CAMERA_MOTION,
    MESSAGE_LANE_TRACK,
    MESSAGE_OBJECT_TRACK,
    MESSAGE_VISUALIZER,
    MESSAGE_VEHICLE_STATE_ROS,
    MESSAGE_MOBILEYE_ROS,
    MESSAGE_RADAR_ROS,
    MESSAGE_RESERVED,
    NUM_VISION_MESSAGES
  };

  enum CameraID
  {
    CAM_FRONT_CENTER = 0,
    CAM_FRONT_CENTER_CROP,
    CAM_FRONT_CENTER_NARROW,
    CAM_FRONT_CENTER_NARROW_CROP,
    CAM_FRONT_CENTER_SVM,
    CAM_FRONT_CENTER_SVM_CROP,
    CAM_FRONT_LEFT,
    CAM_FRONT_RIGHT,
    CAM_FRONT_LR,
    CAM_SIDE_LEFT,
    CAM_SIDE_RIGHT,
    CAM_REAR_CENTER,
    CAM_REAR_CENTER_CROP,
    CAM_REAR_SIDE_LEFT,
    CAM_REAR_SIDE_LEFT_CROP,
    CAM_REAR_SIDE_RIGHT,
    CAM_REAR_SIDE_RIGHT_CROP,
    CAM_REAR_CENTER_SVM,
    CAM_REAR_CENTER_SVM_CROP,
    CAM_GROUND,
    CAM_RESERVED,
    NUM_CAM_DIRS
  };

  enum CameraTypeEnum : std::int32_t
  {
    CAM_TYPE_NONE       = 0x0000,
    CAM_TYPE_FRONT      = 0x0001,
    CAM_TYPE_REAR       = 0x0002,
    CAM_TYPE_SIDE       = 0x0004,
    CAM_TYPE_LEFT       = 0x0008,
    CAM_TYPE_RIGHT      = 0x0010,
    CAM_TYPE_STRAIGHT   = 0x0020,
    CAM_TYPE_OBLIQUE    = 0x0040,
    CAM_TYPE_NORMAL     = 0x0080,
    CAM_TYPE_NARROW     = 0x0100,
    CAM_TYPE_SVM        = 0x0200,
    CAM_TYPE_MAIN       = 0x0400,
    CAM_TYPE_CROP       = 0x0800,
    CAM_TYPE_COMPOSITE  = 0x1000,
    CAM_TYPE_REAR_OBLIQUE = CAM_TYPE_REAR | CAM_TYPE_OBLIQUE
  };

  using CameraList = std::vector<CameraID>;

  using CameraType = std::int32_t;


  inline std::string vision_task_name(VisionTaskID id, bool warning = true)
  {
    switch(id)
    {
      case TASK_INVALID         :     return "task_invalid"        ;
      case TASK_CAMERA          :     return "task_camera"         ;
      case TASK_PHANTOMNET_0    :     return "task_phantomnet_0"   ;
      case TASK_PHANTOMNET_1    :     return "task_phantomnet_1"   ;
      case TASK_PHANTOMNET_2    :     return "task_phantomnet_2"   ;
      case TASK_PHANTOMNET_3    :     return "task_phantomnet_3"   ;
      case TASK_FEATURE_TRACK   :     return "task_feature_track"  ;
      case TASK_OPTICAL_FLOW    :     return "task_optical_flow"   ;
      case TASK_CAMERA_MOTION   :     return "task_camera_motion"  ;
      case TASK_LANE_TRACK      :     return "task_lane_track"     ;
      case TASK_OBJECT_TRACK    :     return "task_object_track"   ;
      case TASK_MESSAGE_PUBLISH :     return "task_message_publish";
      case TASK_VISUALIZER      :     return "task_visualizer"     ;
      case TASK_ROS_NODE        :     return "task_ros_node"       ;
      case NUM_VISION_TASKS     :
      default:
        PHANTOM_ERROR_IF(warning, "Invalid vision task: {}", id);
        return "task_unknown";
    }
  }

  inline VisionMessageID vision_message_name(std::string name, bool warning = true)
  {
    if      (name == "msg_cameras"        )   return MESSAGE_CAMERAS          ;
    else if (name == "msg_phantomnet"     )   return MESSAGE_PHANTOMNET       ;
    else if (name == "msg_feature_track"  )   return MESSAGE_FEATURE_TRACK    ;
    else if (name == "msg_optical_flow"   )   return MESSAGE_OPTICAL_FLOW     ;
    else if (name == "msg_camera_motion"  )   return MESSAGE_CAMERA_MOTION    ;
    else if (name == "msg_lane_track"     )   return MESSAGE_LANE_TRACK       ;
    else if (name == "msg_object_track"   )   return MESSAGE_OBJECT_TRACK     ;
    else if (name == "msg_visualizer"     )   return MESSAGE_VISUALIZER       ;
    else if (name == "msg_vehicle_state"  )   return MESSAGE_VEHICLE_STATE_ROS;
    else if (name == "msg_mobileye"       )   return MESSAGE_MOBILEYE_ROS     ;
    else if (name == "msg_radar"          )   return MESSAGE_RADAR_ROS        ;
    else {
      PHANTOM_ERROR_IF(warning, "Invalid vision message name: {}", name);
      return MESSAGE_RESERVED;
    }
  }

  inline std::string vision_message_name(VisionMessageID id, bool warning = true)
  {
    switch(id)
    {
      case MESSAGE_CAMERAS          :   return "msg_cameras"      ;
      case MESSAGE_PHANTOMNET       :   return "msg_phantomnet"   ;
      case MESSAGE_FEATURE_TRACK    :   return "msg_feature_track";
      case MESSAGE_OPTICAL_FLOW     :   return "msg_optical_flow" ;
      case MESSAGE_CAMERA_MOTION    :   return "msg_camera_motion";
      case MESSAGE_LANE_TRACK       :   return "msg_lane_track"   ;
      case MESSAGE_OBJECT_TRACK     :   return "msg_object_track" ;
      case MESSAGE_VISUALIZER       :   return "msg_visualizer"   ;
      case MESSAGE_VEHICLE_STATE_ROS:   return "msg_vehicle_state";
      case MESSAGE_MOBILEYE_ROS     :   return "msg_mobileye"     ;
      case MESSAGE_RADAR_ROS        :   return "msg_radar"        ;
      case MESSAGE_RESERVED         :   return "msg_reserved"     ;
      case NUM_VISION_MESSAGES      :
      default:
        PHANTOM_ERROR_IF(warning, "Invalid vision message: {}", id);
        return "vmsg_unknown";
    }
  }

  inline int set_cameras_flag(const CameraList& cameras)
  {
    int flag = 0;
    for (const auto& cam : cameras)
    {
      flag |= CAMERA_FLAG(cam);
    }
    return flag;
  }

  inline std::string cv_mat_type_name(const cv::Mat& m)
  {
    std::string s;
    uchar depth = m.type() & CV_MAT_DEPTH_MASK;
    switch (depth)
    {
      case CV_8U:  s = "8U";    break;
      case CV_8S:  s = "8S";    break;
      case CV_16U: s = "16U";   break;
      case CV_16S: s = "16S";   break;
      case CV_32S: s = "32S";   break;
      case CV_32F: s = "32F";   break;
      case CV_64F: s = "64F";   break;
      default:     s = "User";  break;
    }

    return fmt::format("{} x {} x {} [cwh] CV_{}C{}", m.channels(), m.cols, m.rows,
      s, 1 + (m.type() >> CV_CN_SHIFT));
  }


  //
  // Extra additions to the original header
  //
  inline CameraID camera_name(std::string name, bool warning = false)
  {
    if      (name == "front_center"            )  return CAM_FRONT_CENTER            ;
    else if (name == "front_center_crop"       )  return CAM_FRONT_CENTER_CROP       ;
    else if (name == "front_center_narrow"     )  return CAM_FRONT_CENTER_NARROW     ;
    else if (name == "front_center_narrow_crop")  return CAM_FRONT_CENTER_NARROW_CROP;
    else if (name == "front_center_svm"        )  return CAM_FRONT_CENTER_SVM        ;
    else if (name == "front_center_svm_crop"   )  return CAM_FRONT_CENTER_SVM_CROP   ;
    else if (name == "front_left"              )  return CAM_FRONT_LEFT              ;
    else if (name == "front_right"             )  return CAM_FRONT_RIGHT             ;
    else if (name == "front_lr"                )  return CAM_FRONT_LR                ;
    else if (name == "side_left"               )  return CAM_SIDE_LEFT               ;
    else if (name == "side_right"              )  return CAM_SIDE_RIGHT              ;
    else if (name == "rear_center"             )  return CAM_REAR_CENTER             ;
    else if (name == "rear_center_crop"        )  return CAM_REAR_CENTER_CROP        ;
    else if (name == "rear_left"               )  return CAM_REAR_SIDE_LEFT          ;
    else if (name == "rear_left_crop"          )  return CAM_REAR_SIDE_LEFT_CROP     ;
    else if (name == "rear_right"              )  return CAM_REAR_SIDE_RIGHT         ;
    else if (name == "rear_right_crop"         )  return CAM_REAR_SIDE_RIGHT_CROP    ;
    else if (name == "rear_center_svm"         )  return CAM_REAR_CENTER_SVM         ;
    else if (name == "rear_center_svm_crop"    )  return CAM_REAR_CENTER_SVM_CROP    ;
    else if (name == "ground"                  )  return CAM_GROUND                  ;
    else if (name == "reserved"                )  return CAM_RESERVED                ;
    else if (name == "none"                    )  return CAM_RESERVED                ;
    else {
      PHANTOM_ERROR_IF(warning, "Invalid camera name: {}", name);
      return NUM_CAM_DIRS;
    }
  }

  inline std::string camera_name(CameraID camera, bool warning = false)
  {
    switch (camera)
    {
      case CAM_FRONT_CENTER            :  return "front_center"            ;
      case CAM_FRONT_CENTER_CROP       :  return "front_center_crop"       ;
      case CAM_FRONT_CENTER_NARROW     :  return "front_center_narrow"     ;
      case CAM_FRONT_CENTER_NARROW_CROP:  return "front_center_narrow_crop";
      case CAM_FRONT_CENTER_SVM        :  return "front_center_svm"        ;
      case CAM_FRONT_CENTER_SVM_CROP   :  return "front_center_svm_crop"   ;
      case CAM_FRONT_LEFT              :  return "front_left"              ;
      case CAM_FRONT_RIGHT             :  return "front_right"             ;
      case CAM_FRONT_LR                :  return "front_lr"                ;
      case CAM_SIDE_LEFT               :  return "side_left"               ;
      case CAM_SIDE_RIGHT              :  return "side_right"              ;
      case CAM_REAR_CENTER             :  return "rear_center"             ;
      case CAM_REAR_CENTER_CROP        :  return "rear_center_crop"        ;
      case CAM_REAR_SIDE_LEFT          :  return "rear_left"               ;
      case CAM_REAR_SIDE_LEFT_CROP     :  return "rear_left_crop"          ;
      case CAM_REAR_SIDE_RIGHT         :  return "rear_right"              ;
      case CAM_REAR_SIDE_RIGHT_CROP    :  return "rear_right_crop"         ;
      case CAM_REAR_CENTER_SVM         :  return "rear_center_svm"         ;
      case CAM_REAR_CENTER_SVM_CROP    :  return "rear_center_svm_crop"    ;
      case CAM_GROUND                  :  return "ground"                  ;
      case CAM_RESERVED                :  return "reserved"                ;
      case NUM_CAM_DIRS                :
      default                          :
        PHANTOM_ERROR_IF(warning, "Invalid camera cam: {}", camera);
        return "unknown";
    }
  }

  inline bool is_main_crop_camera(CameraID camera0, CameraID camera1)
  {
    if ((int)camera1 - (int)camera0 == 1)
    {
      return  camera1 == CAM_FRONT_CENTER_CROP        ||
              camera1 == CAM_FRONT_CENTER_NARROW_CROP ||
              camera1 == CAM_FRONT_CENTER_SVM_CROP    ||
              camera1 == CAM_REAR_CENTER_CROP         ||
              camera1 == CAM_REAR_SIDE_LEFT_CROP      ||
              camera1 == CAM_REAR_SIDE_RIGHT_CROP     ||
              camera1 == CAM_REAR_CENTER_SVM_CROP;
    }

    return false;
  }

  struct CameraInfo
  {
    std::string name_;
    std::string serial_number_;
    std::string sensor_type_;
    std::string calib_model_;

    float lens_fov_;
    cv::Mat camera_matrix_;
    cv::Mat distortion_;
    cv::Mat skew_;
    cv::Size resolution_;
    cv::Size output_resolution_;
    cv::Rect crop_roi_;
    cv::Rect crop_roi_extra_;
    std::vector<float> position_meter_;
    std::vector<float> rotation_rad_;
    std::string location_;
    std::string vehicle_;
    std::string comment_;

    bool horizontal_flip_;
    bool vertical_flip_;
  };
  //
  // End of extra additions
  //

}


#endif
