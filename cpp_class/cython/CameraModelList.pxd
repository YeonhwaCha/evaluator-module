from libcpp cimport bool
from libcpp.memory cimport shared_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.pair cimport pair

from libcpp cimport camera_id

#ctypedef enum CameraID:
#    CAM_FRONT_CENTER,
#    CAM_FRONT_CENTER_CROP,
#    CAM_FRONT_CENTER_NARROW,
#    CAM_FRONT_CENTER_NARROW_CROP,
#    CAM_FRONT_CENTER_SVM,
#    CAM_FRONT_CENTER_SVM_CROP,
#    CAM_FRONT_LEFT,
#    CAM_FRONT_RIGHT,
#    CAM_FRONT_LR,
#    CAM_SIDE_LEFT,
#    CAM_SIDE_RIGHT,
#    CAM_REAR_CENTER,
#    CAM_REAR_CENTER_CROP,
#    CAM_REAR_SIDE_LEFT,
#    CAM_REAR_SIDE_LEFT_CROP,
#    CAM_REAR_SIDE_RIGHT,
#    CAM_REAR_SIDE_RIGHT_CROP,
#    CAM_REAR_CENTER_SVM,
#    CAM_REAR_CENTER_SVM_CROP,
#    CAM_GROUND,
#    CAM_RESERVED,
#    NUM_CAM_DIRS

#cdef extern from "../src/camera_model.cpp":
#    pass

# Declare the class with cdef
cdef extern from "../src/camera_model.h" namespace "phantom_ai":
    #ctypedef shared_ptr[CameraModel] CameraModelShPtr
    
    #cdef cppclass CameraModel:
    #    CameraModel() except +

    cdef cppclass CameraModelList:
        CameraModelList() except +
        CameraModelList(int) except +
        int num_cameras
        bool Initialize(string param_file, string vehicle, pair[int, int] image_size, vector[int] cameras)
        vector[float] GetCoordFromWorld(int cam, float y, float width_m, float position_y, float range_m)
        void Rotation(int cam)
        void Save()

        #vector[CameraModelShPtr] models_
