import camera_model

# from ../src/vision_constants.h
CAM_FRONT_CENTER = 0
NUM_CAM_DIRS = 21

if __name__ == '__main__':
 
    #camera_model = camera_model.PyCameraModelWrapper()
    #camera_model.initialize()
       
    calib_file_name = b'../test/camera_calibration_list.yaml'
    vehicle_name = b'genesis_4402'
    image_size = [960, 540]
    #cameras = [CAM_FRONT_CENTER, 1]
    cameras = [0, 1]
    print('CAMERAS ::', cameras)

    # need to figure out the shared_ptr
    camera_model = camera_model.PyCameraModelList(1) # fix me..
    camera_model.initialize(calib_file_name, 
                            vehicle_name, 
                            image_size, 
                            cameras)

    #camera_model.save()
    # can get an only one camera model 
    bbox = camera_model.get_coord_from_world(1, 226.4195680618286, 1.7000000476837158, -2.3125, 93.5999984741211)
    print('BBOX ::', bbox)
