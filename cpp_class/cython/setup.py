from setuptools import Extension, setup
from Cython.Build import cythonize
from Cython.Distutils import build_ext

import os

class BuildExtWithoutPlatformSuffix(build_ext):
    def get_ext_filename(self, ext_name):
        from distutils.sysconfig import get_config_var
        ext_path = ext_name.split('.')
        ext_suffix = get_config_var('EXT_SUFFIX') 
        return os.path.join('../../', *ext_path) + '.so'

ext_modules=cythonize(
        Extension("camera_model",
            sources=['camera_model.pyx'],
            include_dirs=['../fmt/include', '../src'],
            libraries=["opencv_features2d", "opencv_highgui", "opencv_core", "opencv_imgproc"],
            library_dirs=['/usr/local/lib'],
            extra_compile_args=["-std=c++11"],
            language="c++"
        ), language_level = "3")

setup(
        cmdclass={'build_ext': BuildExtWithoutPlatformSuffix },
        ext_modules=ext_modules)
