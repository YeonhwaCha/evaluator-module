# distutils: language = c++
# distutils: sources = ../src/camera_model.cpp

from libcpp.string cimport string
from libcpp.utility cimport pair
from libcpp.vector cimport vector
from CameraModelList cimport CameraModelList

cdef CAM_FRONT_CENTER = 0
cdef NUM_CAM_DIRS = 21

cdef class PyCameraModelList:
    cdef CameraModelList c_camera_model_list
    #cdef CameraModelShPtr thisptr

    def __cinit__(self, int num_cameras):
        #self.thisptr = new CameraModelShPtr(new CameraModel())
        self.c_camera_model_list = CameraModelList(num_cameras)

    #def __dealloc__(self):
    #    self.thisptr.reset()
        
    def initialize(self, param_file, vehicle, image_size, cameras):
        print('Initialization')
        print('PARAM FILE ::', param_file)
        print('VEHICLE ::', vehicle)
        print('IMAGE_SIZE ::', image_size)
        print('CAMERAS ::', cameras)

        # convert python list to std::vector
        cdef vector[int] cameras_
        for cam in cameras:
            cameras_.push_back(cam)
        
        # convert python list to std::pair
        cdef pair[int, int] image_size_
        image_size_ = pair[int, int] (<int>image_size[0], <int>image_size[1])

        self.c_camera_model_list.Initialize(param_file, vehicle, image_size_, cameras_)
                 
    def get_coord_from_world(self, cam, y, width_m, position_y, range_m):
        return self.c_camera_model_list.GetCoordFromWorld(cam, y, width_m, position_y, range_m)

    #def get_rotation(self, cam):
    #    self.c_camera_model_list.Rotation(cam)

    def save(self):
        self.c_camera_model_list.Save()
