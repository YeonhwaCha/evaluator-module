# README #

### How do I get set up? ###

Everything should be self-contained here and no need to download something else.
```
python setup.py build_ext --inplace
```

Once the build is successful, run the test.
```
python test.py
```
