import copy
import json
import math
import numpy as np
import sys
import warnings

from numpy.polynomial import polynomial

from common.utils import (coordinates_to_pts)
from labelmap.phantom_lane_labelmap import name2label
from helpers.lane_utils import (offset_from_center_on_the_bottom)

def reassign_relation_id(annotation_entities, im_size, tr_im_size,lane_ids, lines, obj_indices, delete_list):
    def get_delete_offsets(candidates):
        candidates.sort()
        max = sys.maxsize
        need_to_delete = []
        for candidate in candidates:
            if abs(max - candidate) < 100:
                if candidate < 0:
                    need_to_delete.append(max)
                else:
                    need_to_delete.append(candidate)
            max = candidate
        return need_to_delete

    def reassign_group_id(entities, offsets_l, offsets_r):
        offsets_l.sort()
        offsets_r.sort()
        delete_offset_l = get_delete_offsets(offsets_l)
        delete_offset_r = get_delete_offsets(offsets_r)
        delete_indices = []
        for idx, obj in enumerate(entities):
            if obj['annotation_type'] == 'line' and obj['attributes']['relation_id'] != None:
                if not 'offset' in obj.keys(): continue
                if obj['offset'] < 0:
                    if obj['offset'] in delete_offset_l:
                        delete_indices.append(idx)
                    group_id = offsets_l.index(obj['offset']) - len(offsets_l)
                    obj['attributes']['relation_id'] = group_id
                else:
                    if obj['offset'] in delete_offset_r:
                        delete_indices.append(idx)
                    group_id = offsets_r.index(obj['offset']) + 1
                    obj['attributes']['relation_id'] = group_id
            
        delete_indices.sort(reverse=True)
        cnt = 0
        for delete_idx in delete_indices:
            if not delete_idx in delete_list: 
                cnt = cnt + 1
            del entities[delete_idx]
        
        new_delete_list = [item - cnt for item in delete_list]
        
        new_delete_list = []
        return entities, new_delete_list
    
    # STEP 1 : 
    # gather the indices of the original lane ids for each lane_id_category
    lane_id_categories = np.unique(lane_ids)
    indices_for_grouping = {}
    for lane_id_category in lane_id_categories:
        type_list = [i for i in range(len(lane_ids)) if lane_ids[i] == lane_id_category]
        indices_for_grouping.update({lane_id_category : type_list})
    
    # STEP 2 :
    # gather line points from each group id 
    gt_lines = {}
    offsets_l = []
    offsets_r = []
    for group_id, line_indices in indices_for_grouping.items():
        line_group = []
        for idx in line_indices:
            line_group.extend(lines[idx].tolist())
        line_group = np.array(line_group)
        line_group = line_group[line_group[:, 1].argsort()[::-1]] # ascending order based on y axis
        offset = offset_from_center_on_the_bottom(line_group, im_size)
        if offset < 0:
            offsets_l.append(offset)
        else:
            offsets_r.append(offset)
        
        for idx in line_indices:
            annotation_entities[obj_indices[idx]].update({'offset' : offset})
    
    new_annotation_entities = reassign_group_id(annotation_entities, offsets_l, offsets_r)
    
    return new_annotation_entities


def front_lane_assignment(annotation_entities, im_size, tr_im_size):
    lane_ids = []
    lines = []
    obj_indices = []
    delete_list = []

    for obj_idx, obj in enumerate(annotation_entities):
        if obj['annotation_type'] == 'line' and obj['attributes']['relation_id'] != None:
            label = obj['label']
            relation_id = obj['attributes']['relation_id']
            try:
                category_id = name2label[label].trainId
            except:
                print("[Error] :: [{}] is not included in this annotation file".format(label))
            
            if category_id > 0:
                coordinates = obj['coordinates']
                # Some Annotations are wrong with group id.
                # It makes a problem when coordinate's lenght is under 3
                # Actually I don't want to add this code such as below, :(
                if len(coordinates) < 3:
                    pow_x = pow(coordinates[0]['x'] - coordinates[1]['x'], 2) 
                    pow_y = pow(coordinates[0]['y'] - coordinates[1]['y'], 2)
                    dist = math.sqrt(pow_x + pow_y)
                    if dist < 15:
                        delete_list.append(obj_idx)
                        continue
                #########################################################
                pts = coordinates_to_pts(coordinates)
                pts = pts[pts[:, 1].argsort()]    # ascending order based on y axis
                lane_ids.append(relation_id)
                lines.append(pts)
                obj_indices.append(obj_idx)
    
    annotation_entities, deleted_entities = reassign_relation_id(annotation_entities, im_size, tr_im_size, lane_ids, lines, obj_indices, delete_list)
    
    deleted_entities.sort(reverse=True)
    for del_idx in deleted_entities:
        del annotation_entities[del_idx]
    
    return annotation_entities


def get_coord_from_lut(cfg, coords, direction_to, im_size=(1, 1), tr_im_size=(1, 1)):
    new_coords = []
    if direction_to == 'left':
        cameraMatrix = cfg.front_center_to_left_matrix
    elif direction_to == 'right':
        cameraMatrix = cfg.front_center_to_right_matrix
    else:
        print("You should put direction to find a look up table")

    rows = cameraMatrix['rows']
    cols = cameraMatrix['cols']
    lut = np.array(cameraMatrix['data']).reshape(rows, cols, 2)

    for coord in coords:
        new_coord = {}
        if rows != im_size[0] and cols != im_size[1]:
            coord['x'] = min(tr_im_size[1] - 1, coord['x'] / im_size[1] * tr_im_size[1])
            coord['y'] = min(tr_im_size[0] - 1, coord['y'] / im_size[0] * tr_im_size[0])
            
        new_coord.update({'x' : lut[int(coord['y']), int(coord['x']), 0] / tr_im_size[1] * im_size[1]})
        new_coord.update({'y' : lut[int(coord['y']), int(coord['x']), 1] / tr_im_size[0] * im_size[0]})
        new_coords.append(new_coord)
        
    return new_coords
