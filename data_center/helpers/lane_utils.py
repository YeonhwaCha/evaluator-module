import warnings
import numpy as np
import sys


def get_coord_from_lut(cfg, coords, direction_to):
    new_coords = []
    if direction_to == 'left':
        cameraMatrix = cfg.front_center_to_left_matrix
    elif direction_to == 'right':
        cameraMatrix = cfg.front_center_to_right_matrix
    else:
        print("You should put direction to find a look up table")
    
    rows = cameraMatrix['rows']
    cols = cameraMatrix['cols'] 
    lut = np.array(cameraMatrix['data']).reshape(rows, cols, 2) 
    
    for coord in coords:
        if rows != cfg.input_img_size[0] and cols != cfg.input_img_size[1]:
            coord[0] = min(cfg.train_img_size[1] - 1, coord[0])
            coord[1] = min(cfg.train_img_size[0] - 1, coord[1]) 
        
        x = int(lut[coord[1], coord[0], 0])
        y = int(lut[coord[1], coord[0], 1])
        new_coords.append([x, y])
    
    return new_coords     


def make_one_line_from_neighbors(origin_lines):
    prev_l = -sys.maxsize
    prev_r = sys.maxsize         
    for group_id in sorted(origin_lines.keys()): 
        if int(group_id) < 0:
            if abs(prev_l - origin_lines[group_id]['offset']) < 100: 
                prev_l = origin_lines[group_id]['offset']
                del origin_lines[group_id]
            else:
                prev_l = origin_lines[group_id]['offset']
        else:
            if abs(prev_r - origin_lines[group_id]['offset']) < 100:
                prev_r = origin_lines[group_id]['offset']           
                del origin_lines[group_id]  
            else:
                prev_r = origin_lines[group_id]['offset']
    return origin_lines 

# need to extend line marks and calculate the offset from the center on bottom line 
def offset_from_center_on_the_bottom(line, tr_im_size):
    offset = line[0, 0] - (tr_im_size[1] / 2)
    # line [x, y] and tr_im_size(img_h, img_w)
    if line[len(line)-1, 1] != tr_im_size[0]:
        try:
            with warnings.catch_warnings(): 
                warnings.simplefilter('ignore', np.RankWarning) 
                A, B = np.polyfit(line[0:2, 0], line[0:2, 1] * -1, 1)
                x_axis = int((-tr_im_size[0] - B) / A) 
                offset = x_axis - (tr_im_size[1] / 2)
        except:
            print('Something wrong in the offset calculation')
    return offset 

def reassign_group_id(gt_lines, offsets_l, offsets_r):  
    new_gt_lines = {}
    offsets_l.sort()        
    offsets_r.sort()    
    
    for key, value in gt_lines.items():     
        if value['offset'] < 0:     
            group_id = offsets_l.index(value['offset']) - len(offsets_l)
        else:
            group_id = offsets_r.index(value['offset']) + 1  
        new_gt_lines.update({str(group_id) : value})    
    
    return new_gt_lines              
