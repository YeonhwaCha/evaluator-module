import glob
import os

def gather_simulation_files(dir_list, rosbags):
    prev = []
    for idx, dir in enumerate(dir_list):
        files = glob.glob(os.path.join(dir, '*.json'))
        
        if idx != 0:
            files = [item for item in files if not os.path.basename(item).split('_') in prev]
        
        rosbags.extend(files)
        prev = [os.path.basename(item).split('_')[0] for item in files]
    
    return rosbags
            

