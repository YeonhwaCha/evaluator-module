import cv2
import numpy as np
import sys
import warnings

def coordinates_to_pts(coordinates):
    # [[x1, y1]], [x2, y2]] -> coordinate based on image coordination 
    pts = np.array([(coord['x'], coord['y']) for coord in coordinates])
    pts = pts.round().astype(np.int32)
    return pts


def cluster_gt_line(lane_ids, lines, im_size):
    lane_type = np.unique(lane_ids)
    # indices_for_grouping {'group_id' : list (indices on lines array)}
    indices_for_grouping = {}
    for type in lane_type:
        type_list = [i for i in range(len(lane_ids)) if lane_ids[i] == type]
        indices_for_grouping.update({type : type_list})
    
    gt_lines = {}
    for group_id, line_indices in indices_for_grouping.items():
        line = []
        for idx in line_indices:
            line.extend(lines[idx].tolist())
        line = np.array(line)
        line = line[line[:,1].argsort()] # ascending order based on y axis
        # line [x, y] and im_size(img_h, img_w)
        # ex) [4, 1][3, 2][2, 3]
        if line[len(line)-1, 1] != im_size[0]:
            try:
                with warnings.catch_warnings():
                    warnings.simplefilter('ignore', np.RankWarning)
                    A, B = np.polyfit(line[len(line)-2:len(line), 0], line[len(line)-2:len(line), 1] * -1, 1)
                    x_axis = int(1/A * (-im_size[0]-B))
                    line = line.tolist()
                    if x_axis > 0:
                        line.append([min(x_axis, sys.maxsize), im_size[0]])
                    else:
                        line.append([max(x_axis, -sys.maxsize), im_size[0]])
 
            except:
                line = line.tolist()
                print('cluster_gt_line is something wrong on cluster_gt_line')
        else:
            line = line.tolist()
        
        line.insert(0, [line[0][0], 0])
        line_object  = {
            'coords' : line
        }
        gt_lines.update({group_id : line_object})
    
    return gt_lines

#########################################################
# Delete boxes which are showed partially...
def delete_partial_boxes(annotation_entities, im_size):
    partial_box_indices = []
    for idx, obj in enumerate(annotation_entities):
        if obj['annotation_type'] == 'bounding_box':
            coordinates =  obj['coordinates']
            label = obj['label'].lower()
            bottom_center_x = (coordinates[2]['x'] + coordinates[0]['x']) / 2
            if coordinates[0]['x'] < 2 or coordinates[2]['x'] > im_size[1] - 2 \
                or coordinates[0]['y'] < 2 or coordinates[2]['y'] > im_size[0] - 2 \
                or bottom_center_x > int(im_size[1]*0.93):
                #print('x : {} < 5 or > {}'.format(coordinates[0]['x'], im_size[1] - 5)) 
                #print('y : {} < 5 or > {}'.format(coordinates[0]['y'], im_size[0] - 5))
                #print('x : {} < 5 or > {}'.format(coordinates[2]['x'], im_size[1] - 5)) 
                #print('y : {} < 5 or > {}'.format(coordinates[2]['y'], im_size[0] - 5))
                partial_box_indices.append(idx)
            
            partial_box_indices.sort(reverse=True)
    
    for idx in partial_box_indices:
        del annotation_entities[idx]
    
    return annotation_entities


def assign_lane(annotation_entities, im_size):
    lane_ids = []
    lines = []
    for obj in annotation_entities:
        if obj['annotation_type'] == 'line':
            lane_id = obj['attributes']['relation_id']
            if lane_id == None: continue
            coordinates = obj['coordinates']
            pts = coordinates_to_pts(coordinates)
            pts = pts[pts[:,1].argsort()] # ascending order based on y axis
            
            lane_ids.append(lane_id)
            lines.append(pts)
    
    gt_lines = cluster_gt_line(lane_ids, lines, im_size)
    return gt_lines


def gather_target_bboxes(annotation_entities, direction_of_cam, gt_lines, im_size, img):
    target_bboxes = []
    
    # draw GT line
    gt_img = np.ones(im_size, np.uint8) * 255
    bbox_obj = {}
    for obj in annotation_entities:
        if obj['annotation_type'] == 'bounding_box':
            coordinates = obj['coordinates']
            pt_x = int((coordinates[3]['x'] + coordinates[2]['x']) / 2)
            pt_y = int(coordinates[2]['y'])
            bbox_obj.update({(pt_x, pt_y) : obj})
            gt_img[pt_y, pt_x] = 1
    for key, value in gt_lines.items():
        if key == -1 or key == 1:
            gt_coords = np.array(value['coords'])
            gt_coords = gt_coords.astype(int)
            for idx in range(0, gt_coords.shape[0]-1):
                if gt_coords[idx+1][0] > 0 and gt_coords[idx+1][0] < 960:
                    cv2.line(gt_img, tuple(gt_coords[idx]), tuple(gt_coords[idx+1]), 0, 1)
        else:
            coords = value['coords']
            coords.append([coords[len(coords)-1][0], coords[0][1]])
            cv2.fillPoly(gt_img, [np.array(coords)], 255)
    # for debugging..
    #cv2.imshow('DEBUG', gt_img)
    #cv2.waitKey(0)
    
    left = False
    center = False
    right = False
    target_bbox = []
    # scan from bottom to top 
    for idx in range(im_size[0]-1, 0, -1):
        exist_obj = np.where(gt_img[idx] == 1) 
        if exist_obj[0].shape[0] == 0: continue
        exist_line = np.where(gt_img[idx] == 0)
        if exist_line[0].shape[0] == 0: continue
        exist_obj = exist_obj[0].tolist()
        for obj in exist_obj:
            if obj < exist_line[0][0]:
                if left == True: continue
                left = True
                if direction_of_cam == 'front':
                    bbox_obj[obj, idx]['lane_assignment'] = 'left'
                else:
                    bbox_obj[obj, idx]['lane_assignment'] = 'right'
                '''
                if exist_line[0].shape[0] > 1:
                    bbox_obj[obj, idx]['lane_assignment'] = 'left'
                else:
                    bbox_obj[obj, idx]['lane_assignment'] = 'None'
                '''
                target_bbox.append(bbox_obj[obj, idx])
            if obj > exist_line[0][0] and obj < exist_line[0][len(exist_line[0])-1]:
                if center == True: continue
                center = True
                bbox_obj[obj, idx]['lane_assignment'] = 'center'
                '''
                if exist_line[0].shape[0] > 1:
                    bbox_obj[obj, idx]['lane_assignment'] = 'center'
                else:
                    bbox_obj[obj, idx]['lane_assignment'] = 'None'
                '''
                target_bbox.append(bbox_obj[obj, idx])
            if obj > exist_line[0][len(exist_line[0])-1]:
                if right == True: continue
                right = True
                if direction_of_cam == 'front':
                    bbox_obj[obj, idx]['lane_assignment'] = 'right'
                else:
                    bbox_obj[obj, idx]['lane_assignment'] = 'left'
                '''
                if exist_line[0].shape[0] > 1:
                    bbox_obj[obj, idx]['lane_assignment'] = 'right'
                else:
                    bbox_obj[obj, idx]['lane_assignment'] = 'None'
                '''
                target_bbox.append(bbox_obj[obj, idx])
    
    return target_bbox

###########################################################################
# Step 1 : remove bounding boxes which is showed partially. 
# delete_partial_boxes()
# Step 2 : assign the number of lane and make a group based on grouping id 
# assign_lane() 
# Step 3 : gather target objects based on lane assignment

def front_bbox_assignment(annotation_entities, direction_of_cam, im_size, img_filename):
    annotation_entities = delete_partial_boxes(annotation_entities, im_size)
    gt_lines = assign_lane(annotation_entities, im_size)
    annotation_entities = gather_target_bboxes(annotation_entities, direction_of_cam, gt_lines, im_size, img_filename)
    '''
    import os
    filepath = os.path.join('/home/yeonhwa/extra-storage/evaluation-dataset/west_coast/images', img_filename)
    print('/home/yeonhwa/extra-storage/evaluation-dataset/west_coast/images', img_filename)
    img = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
    gt_img = np.ones(im_size, np.uint8) * 255
    
    for obj in annotation_entities: 
        if obj['annotation_type'] == 'bounding_box':
            coordinates = obj['coordinates']
            pt_x = int((coordinates[3]['x'] + coordinates[2]['x']) / 2)
            pt_y = int(coordinates[2]['y'])
            # need to be deleted
            cv2.circle(gt_img, (pt_x, pt_y), 1, 1, -1)
            pt1 = (int(coordinates[0]['x']), int(coordinates[0]['y']))
            pt2 = (int(coordinates[2]['x']), int(coordinates[2]['y']))
            #print('PT1 ::', pt1)
            #print('PT2 ::', pt2)
            cv2.rectangle(gt_img, pt1, pt2, 1, 1)
    
    for idx, coord in gt_lines.items():
        points = coord['coords']
        for index in range(len(points)-1):
            cv2.line(gt_img, tuple(points[index]), tuple(points[index+1]), 0, 1)
    
    #if img_filename == '2019-11-01-17-03-35_vision_evaluation_data_csi_cam_front_center_image_raw-1572653121553.png':
    #    import pdb; pdb.set_trace()
    gt_img = cv2.addWeighted(img, 0.5, gt_img, 0.5, 1)
    cv2.imshow('test', gt_img)
    cv2.waitKey(0)
    '''
    return annotation_entities
