import copy
import json
import numpy as np
import os
import warnings

from common.utils import (coordinates_to_pts)
from helpers.lane_utils import (get_coord_from_lut,
                                make_one_line_from_neighbors,
                                offset_from_center_on_the_bottom,
                                reassign_group_id)
from labelmap.phantom_lane_labelmap import name2label

class LaneGt:
    def __init__(self, cfg, left_ann_files, center_ann_files, right_ann_files):
        self.cfg = cfg
        self.left_ann_files   = left_ann_files
        self.center_ann_files = center_ann_files
        self.right_ann_files  = right_ann_files
        self.im_size          = cfg.input_img_size
        self.tr_im_size       = cfg.train_img_size
 
    def gt_lines_object(self, direction_of_cam, annotated_object_id, image_id, labels, lane_ids, lines):
        lane_type = np.unique(lane_ids)
        # indices_for_grouping {'group_id' : list (indices on lines array) }
        indices_for_grouping = {}
        for type in lane_type:
            type_list = [i for i in range(len(lane_ids)) if lane_ids[i] == type] 
            indices_for_grouping.update({type : type_list})
        
        gt_lines = {}
        offsets_l = []   # offset < 0 : left side
        offsets_r = []   # offset > 0 : right side
        
        for group_id, line_indices in indices_for_grouping.items():
            line_group = []
            label = labels[line_indices[0]]
            # gather line pixels which have the same group id
            for idx in line_indices:
                line_group.extend(lines[idx].tolist())
            line_group = np.array(line_group)
            line_group = line_group[line_group[:,1].argsort()[::-1]] # ascending order based on y axis
            line_group = line_group / self.im_size * self.tr_im_size
            line_group = line_group.astype(int)
            offset = offset_from_center_on_the_bottom(line_group, self.tr_im_size)
            if offset < 0:
                offsets_l.append(offset)
            else:
                offsets_r.append(offset)
            line_object = {
                'direction_of_cam' : direction_of_cam,
                'filename' : annotated_object_id,
                'image_id' : image_id,
                'coords'   : line_group.tolist(),
                'label'    : label,
                'offset'   : offset
            }
            gt_lines.update({group_id : line_object})
        gt_lines = reassign_group_id(gt_lines, offsets_l, offsets_r)
        gt_lines = make_one_line_from_neighbors(gt_lines)

        return gt_lines

    def get_left_and_right_line_from_center_img(self, center_lines):
        left_lines  = copy.deepcopy(center_lines)
        right_lines = copy.deepcopy(center_lines)
    
        for timestamp in sorted(center_lines.keys()):
            for key, item in center_lines[timestamp].items():
                left_coord = get_coord_from_lut(self.cfg, item['coords'].copy(), 'left')
                left_lines[timestamp][key]['coords'] = left_coord
                
                right_coord = get_coord_from_lut(self.cfg, item['coords'].copy(), 'right')
                right_lines[timestamp][key]['coords'] = right_coord
        
        return left_lines, right_lines

    def load_phantom_ann(self, src_path, direction_of_cam):
        annotations = {}
        for timestamp in sorted(self.center_ann_files.keys()):  
            ann_filename = self.center_ann_files[timestamp]
            center_ann_filepath = os.path.join(src_path, 'phantom_annotations/phantom_{}'.format(ann_filename))
            
            labels = []
            lane_ids = []
            lines = []  
                
            if os.path.exists(center_ann_filepath):
                # read a phantom annotation file
                with open(center_ann_filepath) as f:
                    ann = json.loads(str(f.read()))
                
                annotation_entities = ann['annotation_entities']
                annotated_object_id = ann['annotated_object_id']
                       
                for obj in annotation_entities: 
                    if obj['annotation_type'] == 'line':
                        label = obj['label']
                        if label == 'lane-dotted': continue
                        lane_id = obj['attributes']['relation_id']
                        if lane_id == None: continue
                        try:
                            category_id = name2label[label].trainId
                            supercategory = name2label[label].trainIdname 
                        except:                             
                            print("[Error] :: [{}] is not included in this annotation file".format(label))
                    
                        if category_id > 0:
                            coordinates = obj['coordinates']
                            pts = coordinates_to_pts(coordinates)
                            pts = pts[pts[:,1].argsort()] # ascending order based on y axis
                            if pts[len(pts)-1][1] < int(self.im_size[0] / 2) + 60: continue
                            if np.sqrt(np.square(pts[0][0] - pts[len(pts)-1][0]) + np.square(pts[0][1] - pts[len(pts)-1][1])) < 20: continue
                            labels.append(label)
                            lane_ids.append(lane_id)
                            lines.append(pts)
                
            else:
                annotated_object_id = None
                file1 = open("./err_msg.txt", "a") 
                file1.write('{} does not exist\n'.format(ann_filename))
                file1.close() 
                # DEBUG
                file2 = open("./error.txt", "a") 
                file2.write('{}\n'.format(timestamp))
                file2.close() 
                print(center_ann_filepath)
            reassigned_lines = self.gt_lines_object(direction_of_cam, annotated_object_id, 0, labels, lane_ids, lines)
            annotations.update({timestamp : reassigned_lines})
                
        return annotations

    def create_lane_gt(self, src_path, direction_of_cam, multiple=False, output_dir=None):
        _left_lines   = {}
        _center_lines = {}
        _right_lines  = {}

        # ** Don't change the order of creating lane 
        # creating center line should be the first step for creating lane gt set
        _center_lines = self.load_phantom_ann(src_path, direction_of_cam)
        
        if multiple == True:
            # create gt line data from left and right image
            _left_lines, _right_lines = self.get_left_and_right_line_from_center_img(_center_lines)
        
        lines = {}
        lines.update({'left'   : _left_lines})
        lines.update({'center' : _center_lines})
        lines.update({'right'  : _right_lines})
        
        phantom_format_obj = {}     
        phantom_format_obj['annotations'] = lines
        
        outfilename = os.path.join(output_dir, "{}_lane_gt.json".format(direction_of_cam))
        
        if output_dir != None:
            print('You will get a new lane ground truth output file from {}'.format(output_dir))
            if not os.path.isdir(output_dir):
                os.makedirs(output_dir)

            with open(outfilename, 'w') as f:
                json.dump(phantom_format_obj, f)

        return phantom_format_obj
        
#------------------------------------------------
# main for testing
#------------------------------------------------
if __name__ == '__main__':
    print('........convert_to_ford_gt_front_lane')
    output_dir = './tmp'
    
    json_filepath = '/home/yeonhwa/phantom/phantom-data-pipeline/evaluator-module/examples/ford_setting_eastcoast.json'

    from configuration.config import (Config)
    from data_center import (DataCenter)
    
    cfg = Config(json_filepath)
    
    data_center      = DataCenter(cfg)
    data_center.load_dataset(cfg.master_json_path)
    
    date = '2019-12-16'

    front_left_ann_files   = data_center.fl_ann_files[date]
    front_center_ann_files = data_center.fc_ann_files[date]
    front_right_ann_files  = data_center.fr_ann_files[date]

    phantom_lane_gt = LaneGt(cfg, front_left_ann_files, front_center_ann_files, front_right_ann_files)
    phantom_lane_gt.create_lane_gt(cfg.annotation_path, 
                                   direction_of_cam='front', 
                                   multiple=True,
                                   output_dir=output_dir)
