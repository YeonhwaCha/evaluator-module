#!/usr/bin/python

"""
Downloading annotations
Copyright 2019 Phantom AI
"""

import argparse
import multiprocessing
import requests
import os
import json
from requests.exceptions import MissingSchema

##### -##### - ##### - ##### - #####
# PARAMETERS TO BE FILLED IN AT THE TOP
PROJECT = 'vision-eval-set_2'
PROVIDER = 'datatang'
DOWNLOAD_SEGMENTATION_IMAGES = False

# TIME FILTER
START_TIME = None
END_TIME   = None

# Iteration of annotation retrieval api  - should not need to be changed
iteration = 1000
##### - ##### - ##### - ##### - #####

# Filters
search_filter = {
    'time_range': {},
    'filters': {},
    'filters_out': {}
}

"""
Time Range is within ISO8601
example
START_TIME = '2019-08-09T12:00:49.646Z'
END_TIME = '2019-08-09T23:46:49.646Z'
"""
time_range = {
    'start_time': START_TIME,
    'end_time': END_TIME
}

"""
Filter

both below fields can be found on theia annotation
Example
PROJECT = 'task_6'
PROVIDER = 'datatang'

filters can become more detailed by adding another field/value
example
{
    'field': 'annotation_type',
    'values': 'mixed'
}
            
Note: filters_out works through the same method as standard filter, except removing the specified query
"""
filters = [
            {
                'field': 'provider',
                'values': [PROVIDER]
            },
            {
                'field': 'project',
                'values': [PROJECT]
            }
          ]

if START_TIME and END_TIME:
    search_filter['time_range'] = time_range

if PROJECT and PROVIDER:
    search_filter['filters'] = filters

# defining API endpoints
IMAGE_RETRIEVAL_API = 'http://data-management/v1/images'
ANNOTATION_RETRIEVAL_API = 'http://data-management/v1/annotations'


def write_chunk(annotation):
    data_provider_annotation_filename = annotation['filename']
    
    phantom_annotation_filename = 'phantom_{}'.format(data_provider_annotation_filename)
    image_filename = annotation['annotated_object_id']
    failed_downloads = []
    
    #print('Downloading {}:annotation={}, image={}'.format(total_annotations,
    #                                                      data_provider_annotation_filename, image_filename))

    # download phantom_annotation
    #phantom_annotation_file_path = os.path.join(phantom_annotations_path, phantom_annotation_filename)
    #with open(phantom_annotation_file_path, 'w') as f:
    #    json.dump(annotation, f)

    if DOWNLOAD_SEGMENTATION_IMAGES:
        # create folder structure for images
        if not os.path.exists('./images'):
            os.makedirs('./images')
        if not os.path.exists('./segmentation'):
            os.makedirs('./segmentation')
        # define image path
        image_file_path = os.path.join('./images', image_filename)

        # download segmentations
        scale_images = annotation['annotation_entities']
        annotation_image_path = os.path.join('./segmentation', image_filename)
        for scale_image in scale_images:
            try:
                image = requests.get(scale_image['url'])
                with open(annotation_image_path, 'wb') as f:
                    for chunk in image.iter_content(chunk_size=1024):
                        if chunk:
                            f.write(chunk)
            except MissingSchema:
                failed_downloads.append('{}: segmentation failed to be downloaded'.format(scale_image))
    else:
        # define image path
        image_file_path = os.path.join(images_path, image_filename)
        
    # download image
    r_image = requests.get('{0}/{1}'.format(IMAGE_RETRIEVAL_API, image_filename), stream=True)
    with open(image_file_path, 'wb') as f:
        for chunk in r_image.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)

    # download raw_annotation
    #r_data_provider_annotation = requests.get('{0}/{1}'.format(
    #    ANNOTATION_RETRIEVAL_API, data_provider_annotation_filename), stream=True)

    #raw_annotation_file = os.path.join(raw_annotations_path, data_provider_annotation_filename)
    #with open(raw_annotation_file, 'wb') as f:
    #    for chunk in r_data_provider_annotation.iter_content(chunk_size=1024):
    #        if chunk:
    #            f.write(chunk)

    if failed_downloads:
        print(failed_downloads)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--rootpath', default='../../source_for_evaluation', help='put your root path for image annotation')
    args = parser.parse_args()
    
    import pdb; pdb.set_trace()
    root_path = os.path.join(args.rootpath, PROJECT)
    if not os.path.isdir(root_path):
        os.mkdir(root_path)
    images_path = os.path.join(root_path, 'images')
    phantom_annotations_path = os.path.join(root_path, 'phantom_annotations')
    raw_annotations_path = os.path.join(root_path, 'raw_annotations')

    r_summary = requests.post('{}/meta/summary/'.format(ANNOTATION_RETRIEVAL_API), json={
        'search': search_filter,
        'dimensions': []
    })

    if not r_summary.ok:
        raise ValueError(r_summary.text)

    # create directories if not exist
    if not os.path.exists(images_path):
        os.makedirs(images_path)

    if not os.path.exists(phantom_annotations_path):
        os.makedirs(phantom_annotations_path)

    if not os.path.exists(raw_annotations_path):
        os.makedirs(raw_annotations_path)
        
    total_annotations = r_summary.json()['total']
    print(total_annotations)
    
    for i in range(0, total_annotations, iteration):
        r = requests.post('{}/meta/'.format(ANNOTATION_RETRIEVAL_API), json={'filters': filters},
                params={'size': iteration, 'offset': i})
        if not r.ok:
            raise ValueError(r.text)
    
        # downloading all the data
        p = multiprocessing.Pool(multiprocessing.cpu_count())
        result = p.map(write_chunk, r.json(), 100)
        p.close()
        p.join()
        
