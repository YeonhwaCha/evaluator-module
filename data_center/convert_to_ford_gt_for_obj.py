import copy
import datetime
import json
import os

from common.utils import (bbox_iou)
from helpers.front_bbox_assignment import front_bbox_assignment
from helpers.front_lane_assignment import (front_lane_assignment,
                                           get_coord_from_lut)

from labelmap.phantom_bbox_labelmap import name2label

class ObjGt:
    # center_ann_files is always exist
    def __init__(self, cfg, left_ann_files, center_ann_files, right_ann_files):
        self.cfg = cfg
        self.left_ann_files = left_ann_files
        self.center_ann_files = center_ann_files
        self.center_ann_for_lines = {}
        self.center_ann_for_boxes = {}
        self.right_ann_files = right_ann_files
        # setting environment from configuration
        self.im_size = cfg.input_img_size
        self.tr_im_size = cfg.train_img_size
        self.name2label = name2label

    def bbox_dict(self, entities, id, cam_id):
        ann_dict = None
        for obj in entities:
            if obj['annotation_type'] == 'bounding_box':
                label = obj['label']
                try:
                    category_id = name2label[label].trainId
                    supercategory = name2label[label].trainIdname
                except:
                    print("[Error] :: [{}] is not included in this annotation file".format(label))
    
                if category_id > 0:
                    coordinates = obj['coordinates']
                    # theia's x = coco's y, theia's y = coco's x
                    bbox = self.phantom_to_coco_bbox(coordinates, self.im_size, self.tr_im_size)
                    # annotations
                    ann_dict = {
                        'id'              : id,         # int
                        'cam_id'          : cam_id,     # str
                        'category_id'     : label,                     # str
                        'bbox'            : bbox,                      # [x, y, width, height]
                        'area'            : bbox[2]*bbox[3],           # float
                        'lane_assignment' : obj['lane_assignment'],    # string
                    }
                    break
        
        return ann_dict

    
    def load_phantom_ann_from_single_image(self, src_path, direction_of_cam, annotations_in, location_of_cam):
        annotations = {}
        id = 0
            
        for timestamp in sorted(annotations_in.keys()):
            ann_filename =annotations_in[timestamp]
            ann_filepath = os.path.join(src_path, 'phantom_annotations/phantom_{}'.format(ann_filename))
            gt_annotation_list = []
            if not os.path.exists(ann_filepath):
                annotation_entities = []
                annotated_object_id = None
                file1 = open("./err_msg.txt", "a")
                file1.write('Error : {} does not exist...\n'.format(ann_filepath))
                file1.close()
                file2 = open("./error.txt","a")
                file2.write('{}\n'.format(timestamp))
                file2.close()
                print(ann_filepath)
            else:
                # read a phantom annotation file
                with open(ann_filepath) as f:
                    ann = json.loads(str(f.read()))
            
                annotation_entities = ann['annotation_entities']
                annotated_object_id = ann['annotated_object_id']
                # lane assignment
                if location_of_cam == 'center':
                    annotation_entities = front_lane_assignment(annotation_entities, self.im_size, self.tr_im_size) 
                    # update the lane information of center image
                    self.center_ann_for_lines.update({timestamp : annotation_entities})
                
                else:
                    center_ann = copy.deepcopy(self.center_ann_for_lines[timestamp])
                    for obj in center_ann:
                        if obj['annotation_type'] == 'line':
                            new_coords = get_coord_from_lut(self.cfg, obj['coordinates'], location_of_cam, self.im_size, self.tr_im_size)
                            obj['coordinates'] = new_coords
                            annotation_entities.append(copy.deepcopy(obj))
                annotation_entities = front_bbox_assignment(annotation_entities, direction_of_cam, self.im_size, annotated_object_id)
                # bbox assignment
                # - if '+1' line is not exist, how can I compare with right side camera. 
                # - if the right of center is not exist, why it is not showed in the center of image????? 
                if location_of_cam != 'center':
                    for idx, bbox_from_center in enumerate(self.center_ann_for_boxes[timestamp]['bbox']['center']):
                        for ann_idx, annotation_entity in enumerate(annotation_entities):
                            if bbox_from_center['lane_assignment'] == annotation_entity['lane_assignment']:
                                list_to_phantom = [{'x' : bbox_from_center['bbox'][0], 'y' : bbox_from_center['bbox'][1]},
                                                   {'x' : bbox_from_center['bbox'][0] + bbox_from_center['bbox'][2],
                                                    'y' : bbox_from_center['bbox'][1] + bbox_from_center['bbox'][3]}]
                            
                                trans_coords = get_coord_from_lut(self.cfg, list_to_phantom, location_of_cam, self.tr_im_size, self.tr_im_size)
                                trans_coords = [trans_coords[0]['x'], trans_coords[0]['y'], 
                                                trans_coords[1]['x'] - trans_coords[0]['x'],
                                                trans_coords[1]['y'] - trans_coords[0]['y']]
                                trans_entity_box = self.phantom_to_coco_bbox(annotation_entity['coordinates'], self.im_size, self.tr_im_size)
                                iou = bbox_iou(trans_entity_box, trans_coords)
                                if iou == 0:
                                    # guess the size of bbox is bigger than the left bbox 
                                    if (bbox_from_center['bbox'][2] * bbox_from_center['bbox'][3]) < \
                                       (trans_entity_box[2] * trans_entity_box[3]):
                                        del self.center_ann_for_boxes[timestamp]['bbox']['center'][idx]
                                    else:
                                        del annotation_entities[ann_idx]
            for obj in annotation_entities:
                if obj['annotation_type'] == 'bounding_box':
                    label = obj['label']
                    try:
                        category_id = self.name2label[label].trainId
                        supercategory = self.name2label[label].trainIdname
                    except:
                        print("[Error] :: [{}] is not included in this annotation file".format(label))
                        
                    if category_id > 0:
                        coordinates = obj['coordinates']
                        # theia's x = coco's y, theia's y = coco's x
                        bbox = self.phantom_to_coco_bbox(coordinates, self.im_size, self.tr_im_size)
                        # annotations
                        ann_dict = {
                            'id'              : id,                                                 # int
                            'cam_id'          : '{}_{}'.format(direction_of_cam, location_of_cam),  # str
                            'category_id'     : label,                       # str
                            'bbox'            : bbox,                        # [x, y, width, height]
                            'area'            : bbox[2]*bbox[3],             # float
                            'lane_assignment' : obj['lane_assignment'],      # string
                        }
                        gt_annotation_list.append(ann_dict)
                        id = id + 1
            
            if len(gt_annotation_list) == 0:
                ann_dict = {
                    'id'              : id,                                                        # int
                    'cam_id'          : '{}_{}'.format(direction_of_cam, location_of_cam),         # str
                    'category_id'     : 'BG',                                         # str
                    'bbox'            : [0, 0, 0, 0],                                 # [x, y, width, height]
                    'area'            : 0,                                            # float
                    'lane_assignment' : 'left',                                         # string
                }
                gt_annotation_list.append(ann_dict)
            filenames = {}
            filenames.update({location_of_cam : annotated_object_id})
            
            gt_ann_obj = {}
            gt_ann_obj.update({'filenames' : filenames})
            gt_ann_obj.update({'bbox' : {location_of_cam : gt_annotation_list}})
            
            annotations.update({timestamp : gt_ann_obj})
        
        return annotations

    # need to add in later :: get information from folder name or file name
    def load_info_format(self, description):
        now  = datetime.datetime.now()
        year = now.year
        date = now.strftime("%m-%d %H:%M")

        info = {
            "description": description,
            "year": year,
            "contributor": 'phantomAI',
            "date_created": date
        }
        
        return info

    """
     Params 
       coordinates : [left_top(x, y), right_top(x, y), right_bottom(x, y), left_bottom(x, y)
     Return 
       bbox : [x, y, width, height]
    """
    def phantom_to_coco_bbox(self, coordinates, src_im_size, dst_im_size):
        if dst_im_size == None:
            dst_im_size = (1, 1)
        
        x = coordinates[0]['x'] / src_im_size[0] * dst_im_size[0]
        y = coordinates[0]['y'] / src_im_size[1] * dst_im_size[1]
        w = (coordinates[2]['x'] - coordinates[0]['x']) / src_im_size[1] * dst_im_size[1]
        h = (coordinates[2]['y'] - coordinates[0]['y']) / src_im_size[0] * dst_im_size[0] 
        
        return [x, y, w, h]
    
    def create_gt_for_obj(self, src_path, direction_of_cam=None, multiple=False, output_dir=None):
        print('You will get a new object ground truth file from {}'.format(output_dir))
        annotations = {}
        if multiple == True:
            # ** important ** : center annotation should be the first step
            center_ann = self.center_ann_files 
            center_ann = self.load_phantom_ann_from_single_image(src_path, direction_of_cam, center_ann, 
                                                                 location_of_cam='center')
            self.center_ann_for_boxes = center_ann
            left_ann   = self.left_ann_files
            left_ann   = self.load_phantom_ann_from_single_image(src_path, direction_of_cam, left_ann, 
                                                                 location_of_cam='left')
            right_ann  = self.right_ann_files
            right_ann  = self.load_phantom_ann_from_single_image(src_path, direction_of_cam, right_ann, 
                                                                 location_of_cam='right')
            outfilename = os.path.join(output_dir, "{}_master_gt.json".format(direction_of_cam))
            
            # merge left and right to center annotaitons
            for timestamp, v in self.center_ann_for_boxes.items():
                if timestamp in left_ann.keys():
                    if not 'filenames' in left_ann[timestamp].keys(): 
                        v.update({'filenames' : '/home/yeonhwa/NoAvailable.png'})
                    else:
                        v['filenames'].update(left_ann[timestamp]['filenames'])
                        v['bbox'].update(left_ann[timestamp]['bbox'])
                else:
                    left_ann.update({timestamp : {}})
                    left_ann[timestamp].update({'filenames' : '/home/yeonhwa/NoAvailable.png'})
                     
                if timestamp in right_ann.keys():
                    if not 'filenames' in right_ann[timestamp].keys(): 
                        v.update({'filenames' : '/home/yeonhwa/NoAvailable.png'})
                        #v['bbox'].update(left_ann[timestamp]['bbox'])
                        #v['bbox'].update(right_ann[timestamp]['bbox'])
                    else:
                        v['filenames'].update(right_ann[timestamp]['filenames'])
                        v['bbox'].update(right_ann[timestamp]['bbox'])
                else:
                    right_ann.update({timestamp : {}})
                    right_ann[timestamp].update({'filenames' : '/home/yeonhwa/NoAvailable.png'})

        else:
            center_ann  = self.center_ann_files
            center_ann  = self.load_phantom_ann_from_single_image(src_path, direction_of_cam, center_ann,
                                                                  location_of_cam='center')
            self.center_ann_for_boxes = center_ann
            outfilename = os.path.join(output_dir, "{}_master_gt.json".format(direction_of_cam))
        
        annotations = self.center_ann_for_boxes
        info = self.load_info_format('This is for Ford Evaluation in 2019')
        
        front_master_format_obj = {}
        front_master_format_obj['info'] = info
        front_master_format_obj['annotations'] = annotations
        
        
        if output_dir != None:
            if not os.path.isdir(output_dir):   
                os.makedirs(output_dir)

            with open(outfilename, 'w') as f:
                json.dump(front_master_format_obj, f)
        else:
            print("You should put output dir for obj_gt.json file")
        
        return front_master_format_obj

#--------------------------------------------------
# main for testing
#--------------------------------------------------
if __name__ == '__main__':
    print('........testing convert_to_ford_gt_for_front_obj')
    output_dir = './tmp'

    json_filepath = '/home/yeonhwa/phantom/phantom-data-pipeline/evaluator-module/examples/ford_setting_westcoast.json'
    date = '2019-11-18'

    from configuration.config import (Config)
    from data_center import (DataCenter)

    cfg = Config(json_filepath)

    data_center      = DataCenter(cfg)
    data_center.load_dataset(cfg.master_json_path)
    
    left_ann_files   = data_center.fl_ann_files[date]
    center_ann_files = data_center.fc_ann_files[date]
    right_ann_files  = data_center.fr_ann_files[date]

    # using left/center/right images for ground truth
    front_gt = ObjGt(cfg, left_ann_files, center_ann_files, right_ann_files)
    front_gt.create_gt_for_obj(cfg.annotation_path, 
                               direction_of_cam='front',
                               multiple=True,
                               output_dir=output_dir)
