import glob
import json
import numpy as np
import os 

from common.utils import (find_the_closest_filename_from_timestamp,
                          coordinates_to_pts)
from helpers.simulation_utils import (gather_simulation_files)

class Simulation:
    def __init__(self, cfg, target_date):
        assert target_date != None
        
        self.front_rosbags = []
        self.rear_rosbags = []
        root_directory = "{}/{}/{}".format(cfg.simulation, cfg.vehicle_name, target_date)
        #root_directory = "{}/{}/{}".format(target_folder, cfg.vehicle_name, 'evaluation-demo')
        eval_dir_list = os.listdir(root_directory)
        for eval_dir in eval_dir_list:
            if eval_dir == '2019-11-01-15-18-25_test' : continue
            front_d = "{}/{}/phantomvision_measurement".format(root_directory, eval_dir)
            rear_d  = "{}/{}/phantomvision_measurement_rear".format(root_directory, eval_dir)
            
            front_dir_list = [os.path.join(front_d, f) for f in os.listdir(front_d)]
            rear_dir_list  = [os.path.join(rear_d, f) for f in os.listdir(rear_d)]
            
            front_dir_list.sort()
            rear_dir_list.sort()

            self.front_rosbags = gather_simulation_files(front_dir_list, self.front_rosbags)
            self.rear_rosbags = gather_simulation_files(rear_dir_list, self.rear_rosbags)
        
    def get_freespace_info_from_simulation(self, timestamp):
        freespace_info = []
        # input timestamp format : 0000000000_000000000 (19 digits)
        ann_timestamp = timestamp[0:8]
        simulation_timestamp = None
        candidates = [f for f in self.front_rosbags if ann_timestamp in os.path.basename(f).split('_')[0]]
        candidates.sort()
        if len(candidates) > 0:
            filename = find_the_closest_filename_from_timestamp(timestamp, candidates)
            simulation_timestamp = os.path.basename(filename)
            with open(filename, 'r') as fs:
                simulation_obj = json.load(fs)
            coords = simulation_obj['free_spaces']
            freespace_info = coordinates_to_pts(coords[0]['points'])
            freespace_info = np.vstack([np.array([0, 288]), freespace_info, np.array([512, 288])])
        else:
            print('TIMESTAMP[{}] : Rosbag does not have this timestamp'.format(timestamp))
        
        return freespace_info, simulation_timestamp
 

    def get_lane_info_from_simulation(self, timestamp, direction_of_cam):
        lane_info = {}
        # input timestamp format : 0000000000_000000000 (19 digits)
        ann_timestamp = timestamp[0:8]
        simulation_timestamp = None
        
        if direction_of_cam == 'front':
            candidates = [f for f in self.front_rosbags if ann_timestamp in os.path.basename(f).split('_')[0]]
        elif direction_of_cam == 'rear':
            candidates = [f for f in self.rear_rosbags if ann_timestamp in os.path.basename(f).split('_')[0]]
        else:
            print("You sould put direction_of_cam")
        candidates.sort()
        
        if len(candidates) > 0:
            filename = find_the_closest_filename_from_timestamp(timestamp, candidates)
            simulation_timestamp = os.path.basename(filename)
            with open(filename, 'r') as fs:
                simulation_obj = json.load(fs)
            lane_info = simulation_obj['lanes']
        else:
            print('TIMESTAMP[{}] : Rosbag does not have this timestamp'.format(timestamp))
        
        return lane_info, simulation_timestamp
 

    def get_obj_info_from_simulation(self, timestamp, direction_of_cam):
        obj_info = {}
        ann_timestamp = timestamp[0:8]
        simulation_timestamp = None
        if direction_of_cam == 'front':
            candidates = [f for f in self.front_rosbags if ann_timestamp in os.path.basename(f).split('_')[0]]
        elif direction_of_cam == 'rear':
            candidates = [f for f in self.rear_rosbags if ann_timestamp in os.path.basename(f).split('_')[0]]
        else:
            print("You sould put direction_of_cam")
        candidates.sort()
        
        if len(candidates) > 0:
            filename = find_the_closest_filename_from_timestamp(timestamp, candidates)
            simulation_timestamp = os.path.basename(filename)
            with open(filename, 'r') as fs:
                simulation_obj = json.load(fs)
            obj_info = simulation_obj['objects']
        else:
            print('TIMESTAMP[{}] : Rosbag does not have this timestamp'.format(timestamp))
        return obj_info, simulation_timestamp


if __name__ == '__main__':
    print('This is for simulation datacenter test')
    json_filepath = '/home/yeonhwa/phantom/phantom-data-pipeline/evaluator-module/examples/ford_setting_westcoast.json'

    from configuration.config import (Config)
    
    cfg = Config(json_filepath)
    date = '2019-11-18'
    simulation = Simulation(cfg, date)
    info = simulation.get_lane_info_from_simulation('1572655543413', 'rear')
    print(info)

