""" 
data_center for managing images and annotations          
Copyright 2018 Phantom AI                                                                                                                                                                                                         
"""
import glob
import json
import os
import time

############################################################
#  11/08/2019 : could be changed the camera organization
#  FRONT LEFT   |   FRONT CENTER   |   FRONT RIGHT  
#  SIDE  LEFT   |                  |   SIDE  RIGHT
#  REAR  LEFT   |   REAR  CENTER   |   REAR  RIGHT 
#  How can I make camera topic dynamically???? 
############################################################

class DataCenter:
    def __init__(self, cfg):
        self.master_json_files = {}
        self.location_info     = {}
        
        self.fl_ann_files = {}
        self.fc_ann_files = {}
        self.fr_ann_files = {}
        self.rl_ann_files = {}
        self.rc_ann_files = {}
        self.rr_ann_files = {}
        
        self.fl_png_files = {}
        self.fc_png_files = {}
        self.fr_png_files = {}
        self.rl_png_files = {}
        self.rc_png_files = {}
        self.rr_png_files = {}
        
        self.fl_topic = cfg.fl_topic
        self.fc_topic = cfg.fc_topic
        self.fr_topic = cfg.fr_topic
        self.rl_topic = cfg.rl_topic
        self.rc_topic = cfg.rc_topic
        self.rr_topic = cfg.rr_topic

        self.timestamps = {}

    def load_dataset(self, master_gt_path, choose_timestamps=[], on_save_timestamps=False):
        if master_gt_path == None: print(":( Front Master GT set must be exist")
        
        master_json_files  = glob.glob(os.path.join(master_gt_path, "*.json"))
        
        try : 
            for json_file in master_json_files:
                timestamp = os.path.basename(json_file).split("-")[6].split(".json")[0]
               
                # If you want to evaluate specific timestamps, 
                # you need to use choose_timestamps list. 
                if len(choose_timestamps) > 0:
                    if not timestamp in choose_timestamps: continue
                
                with open(json_file) as f:
                    master_json = json.loads(str(f.read()))
                
                datetime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(timestamp[0:10])))
                datetime = datetime[0:10]   
                if not datetime in self.timestamps.keys():
                    self.timestamps.update({datetime : []})
                self.timestamps[datetime].append(timestamp)

                try :
                    if not datetime in self.location_info.keys():
                        self.location_info.update({datetime : {}})
                    # need to be modified in later
                    if "location" in master_json.keys():
                        location = master_json["location"]
                        # please change this format to the same format 
                        # need to ask to Eric...
                        if isinstance(master_json["location_info"], dict):
                            master_json["location_info"].update(location)
                        else:
                            master_json["location_info"][0].update(location)
                    self.location_info[datetime].update({timestamp : master_json["location_info"]})
                except:
                    print(master_json["location_info"][0])
                    print('This version does not have [location_info] notation')
                
                if not datetime in self.master_json_files.keys():
                    self.master_json_files.update({datetime : {}})
                self.master_json_files[datetime].update({timestamp : json_file})
                
                related_images = master_json["related_images"]
                
                for image in related_images:   
                    annotation_md5 = "{}.ann".format(image["annotation_md5"])
                    
                    # gather front left images and annotations
                    if self.fl_topic in image.keys():
                        if not datetime in self.fl_png_files.keys():
                            self.fl_ann_files.update({datetime : {}})
                            self.fl_png_files.update({datetime : {}})
                        self.fl_ann_files[datetime].update({timestamp : annotation_md5})
                        self.fl_png_files[datetime].update({timestamp : image[self.fl_topic]})
                    
                    # gather front center images and annotations
                    if self.fc_topic in image.keys():
                        if not datetime in self.fc_png_files.keys():
                            self.fc_ann_files.update({datetime : {}})
                            self.fc_png_files.update({datetime : {}})
                        self.fc_ann_files[datetime].update({timestamp : annotation_md5})
                        self.fc_png_files[datetime].update({timestamp : image[self.fc_topic]})

                    # gather front right images and annotations
                    if self.fr_topic in image.keys():
                        if not datetime in self.fr_png_files.keys():
                            self.fr_ann_files.update({datetime : {}})
                            self.fr_png_files.update({datetime : {}})
                        self.fr_ann_files[datetime].update({timestamp : annotation_md5})
                        self.fr_png_files[datetime].update({timestamp : image[self.fr_topic]})
                    
                    # gather rear left images and annotations
                    if self.rl_topic in image.keys():
                        if not datetime in self.rl_png_files.keys():
                            self.rl_ann_files.update({datetime : {}})
                            self.rl_png_files.update({datetime : {}})
                        self.rl_ann_files[datetime].update({timestamp : annotation_md5})
                        self.rl_png_files[datetime].update({timestamp : image[self.rl_topic]})

                    # gather rear center images and annotations
                    if self.rc_topic in image.keys():
                        if not datetime in self.rc_png_files.keys():
                            self.rc_ann_files.update({datetime : {}})
                            self.rc_png_files.update({datetime : {}})
                        self.rc_ann_files[datetime].update({timestamp : annotation_md5})
                        self.rc_png_files[datetime].update({timestamp : image[self.rc_topic]})
                    
                    # gather rear right images and annotations
                    if self.rr_topic in image.keys():
                        if not datetime in self.rr_png_files.keys():
                            self.rr_ann_files.update({datetime : {}})
                            self.rr_png_files.update({datetime : {}})
                        self.rr_ann_files[datetime].update({timestamp : annotation_md5})
                        self.rr_png_files[datetime].update({timestamp : image[self.rr_topic]})
        except:
            print(":( YOUR JSON FILE iS SOMETHING WRONG-{}".format(os.path.basename(json_file)))
        
        if on_save_timestamps == True:
            timestamp_list_file = open("./timestamps", "+w")
            self.timestamps.sort()
            for timestamp in self.timestamps:
                timestamp_list_file.write('{}\n'.format(timestamp))
            timestamp_list_file.close()

##############################
# main for testing
##############################
if __name__ == '__main__':
    print("This is test code")
    #json_filepath = '/home/yeonhwa/phantom/phantom-data-pipeline/evaluator-module/examples/ford_setting_westcoast.json'
    #json_filepath = '/home/yeonhwa/phantom/phantom-data-pipeline/evaluator-module/examples/ford_setting_eastcoast.json'
    json_filepath = '/home/yeonhwa/phantom/phantom-data-pipeline/evaluator-module/examples/ford_setting_eastcoast.json'
    #timestamp_file = '../../source_for_evaluation/timestamps/middle-area-json_6121_timestamps.json'
    timestamp_file = '../../source_for_evaluation/timestamps/east-coast-mar-20-combined_30352_timestamps.json'

    with open(timestamp_file, 'r') as fs:
        timestamps = json.load(fs)
    
    #timestamps = ['1572653053489', '1572653133585', '1572653261750', '15726534740320',
    #              '1572653586140', '1572653610144', '1572653902490']

    #timestamps = ['1576759571283']
    #timestamps = ['1576504185438']
    timestamps  = ['1576504161437', '1576504164370', '1576504167371', '1576504170371', '1576504173370',
                   '1576504176371', '1576504179371', '1576504182371', '1576504185438', '1576508032300']

    from configuration.config import (Config)
    from data_center import (DataCenter)
    
    cfg = Config(json_filepath)                         
    
    data_center      = DataCenter(cfg)     
    data_center.load_dataset(cfg.master_json_path, timestamps) 
    import pdb; pdb.set_trace()
    # we are using front center as a master for all related images.
    front_center = data_center.fc_ann_files
    front_image  = data_center.fc_png_files
    print(data_center.location_info)
    print(json.dumps(front_center, indent=2))
    print(json.dumps(front_image, indent=2))
    #rear_center = datacenter.rear_center_ann_files
    #print(json.dumps(rear_center, indent=2))
