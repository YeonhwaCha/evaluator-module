import glob
import json
import numpy as np
import os

from common.utils import (find_the_closest_filename_from_timestamp)

class RefFromLidar:
    def __init__(self, cfg, area=None):
        lane_root_directory = cfg.lane_reference
        self.lidar_lane_files = []
        self.lidar_lane_files = glob.glob(os.path.join(cfg.lane_reference, "*.txt"))
        '''
        lane_ref_list = os.listdir(lane_root_directory)
        for lane in lane_ref_list:
            lane_dir = os.path.join(lane_root_directory, lane)
            lane_ref_list = glob.glob(os.path.join(lane_dir, "*.txt"))
            self.lidar_lane_files.extend(lane_ref_list)
        '''
        root_directory = cfg.point_cloud
        self.lidar_pc_files = []
        pc_ref_list = os.listdir(root_directory)
        for pc in pc_ref_list:
            if area != None:
                if not area in pc.lower(): continue
            pc_dir = os.path.join(cfg.point_cloud, pc)
            pc_ref_list = glob.glob(os.path.join(pc_dir, "*.pc"))
            self.lidar_pc_files.extend(pc_ref_list)
        
        self.lidar_obj_files   = glob.glob(os.path.join(cfg.object_reference, "*.json"))

    def get_lane_info_from_lidar(self, timestamp):
        lanes = {}
        # input timestamp format : 0000000000_000000000 (19 digits)
        ann_timestamp = timestamp[0:7]
        timestamp_from_reference = None
        #lane_lidar_files = [file for file in self.lidar_lane_files 
        #                    if ann_timestamp in os.path.basename(file).split('_')[0]]
        lane_lidar_files = self.lidar_lane_files
        lane_lidar_files.sort()
        if len(lane_lidar_files) == 0:
            err = 'The result of lidar is empty'
            #print(err)
        else:
            filename = find_the_closest_filename_from_timestamp(timestamp, lane_lidar_files)
            timestamp_from_reference = os.path.basename(filename)
            with open(filename, 'r') as f:
                lidar_lane_list = f.readlines()
            
            for lane in lidar_lane_list:
                lane = lane.strip('\n')
                lane = lane.split('\t')
                lane_number = lane[0]
                lane = np.array(lane[1:len(lane)])
                lane = lane.reshape(int(lane.shape[0]/2), 2).astype(float)
                lanes.update({lane_number : lane})
        
        return lanes, timestamp_from_reference

    def get_obj_info_from_lidar(self, timestamp):
        obj_lidar_list = []
        ann_timestamp = timestamp[0:7]
        timestamp_from_reference = None
        
        #obj_lidar_files = [file for file in self.lidar_obj_files 
        #                    if ann_timestamp in os.path.basename(file).split('_')[0]]
        obj_lidar_files = self.lidar_obj_files
        obj_lidar_files.sort()
        
        if len(obj_lidar_files) == 0: 
            err = 'The result of lidar is empty'
            #print(err)
        else:
            filename = find_the_closest_filename_from_timestamp(timestamp, obj_lidar_files)
            timestamp_from_reference = os.path.basename(filename)
            with open(filename, 'r') as f:
                obj_lidar_list = json.loads(f.read())

        return obj_lidar_list, timestamp_from_reference
    
    def get_pc_info_from_lidar(self, timestamp):
        pc_lidar_list = []
        ann_timestamp = timestamp[0:7]
        pc_lidar_files = [file for file in self.lidar_pc_files 
                            if ann_timestamp in os.path.basename(file).split('_')[0]]
        pc_lidar_files.sort()
        if len(pc_lidar_files) == 0:
            err = 'The result of lidar is empty'
            #print(err)
        else:
            filename = find_the_closest_filename_from_timestamp(timestamp, pc_lidar_files)
            dt = np.dtype([('x', np.float32), ('y', np.float32), ('z', np.float32),
                           ('i', np.uint8), ('t', np.uint32)])
            pcl_raw = np.fromfile(filename, dtype=dt)
            pc_lidar_list = np.array([[p['x'], p['y'], p['z'], p['i']] for p in pcl_raw])
            #time = np.array([p['t'] for p in pcl_raw], np.uint32)
            
        return pc_lidar_list

if __name__ == '__main__':
    ref_from_lidar = RefFromLidar()
    ref_from_lidar.get_obj_info_from_lidar('1572653255222')
