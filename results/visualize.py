import cv2
import json
import numpy as np
import os
import time

from common.utils import (Bbox,
                          xywh_to_coord)
from ref_from_lidar import (RefFromLidar)
from results.helpers.draw_utils import (draw_all_lane,
                                        draw_gt_bbox,
                                        draw_gt_lane,
                                        draw_dt_lane,
                                        draw_dt_bbox_from_lidar,
                                        draw_ref_bbox_from_lidar,
                                        draw_result_on_center_img)
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

################################################
# img0, img1, img2
#------ img3 ------
# img4, img5, img6

XMIN = -80
XMAX = 80
YMIN = -9
YMAX = 9

class Visualize:
    def __init__(self, cfg, date, src_path, area=None, img_size=[288, 512, 3]):
        fig = Figure(figsize=(16*3, 9), dpi=32)
        ax = fig.add_subplot(111)
        ax.axis([XMIN, XMAX, YMIN, YMAX])
        ax.grid(alpha=.4, linestyle='--')
        ax.tick_params(axis='y', labelsize=25)
        ax.tick_params(axis='x', labelsize=25)
        self.ax = ax
        self.ax.axvline(x=0)
        self.cfg = cfg
        self.canvas = FigureCanvas(fig)
        self.src_path = src_path
        self.img_size = img_size
        self.lane_img = np.ones(self.img_size, np.uint8) * 255
        
        # image 
        self.fl_img = np.zeros(img_size, dtype=np.uint8)  # fronr_left
        self.fc_img = np.zeros(img_size, dtype=np.uint8)  # front_center
        self.fr_img = np.zeros(img_size, dtype=np.uint8)  # front_right
        self.rl_img = np.zeros(img_size, dtype=np.uint8)  # rear_left
        self.rc_img = np.zeros(img_size, dtype=np.uint8)  # rear_center
        self.rr_img = np.zeros(img_size, dtype=np.uint8)  # rear_right
 
        # accuracy
        self.fl_lane_acc = None
        self.fr_lane_acc = None
        self.rl_lane_acc = None
        self.rr_lane_acc = None
        self.freespace_acc = None

        # timestamps
        self.ref_lane_timestamp = None
        self.simulation_lane_timestamp = None
        
        self.bird_eye_view = np.ones([img_size[0], img_size[1]*3, 3], dtype=np.uint8) * 255
        self.ref_from_lidar = RefFromLidar(cfg, area)
        
        with open('../examples/out/{}/front_lane_gt.json'.format(date), 'r') as f:
            gt_from_json = json.load(f)
        self.fl_ann = gt_from_json['annotations']['left']
        self.fr_ann = gt_from_json['annotations']['right']
        
        with open('../examples/out/{}/rear_lane_gt.json'.format(date), 'r') as f:
            gt_from_json = json.load(f)
        self.rl_ann = gt_from_json['annotations']['left']
        self.rr_ann = gt_from_json['annotations']['right']


    def load_images(self, filenames, direction_of_cam):
        for key, filename in filenames.items():
            cam_location = '{}-{}'.format(direction_of_cam, key)
            if filename == None:
                filepath = os.path.join('./NoAvailable.png')
            else:
                filepath = os.path.join(self.src_path, filename)
            img = cv2.imread(filepath)
            img = cv2.resize(img, (self.img_size[1], self.img_size[0]), cv2.INTER_LINEAR)
            if cam_location == 'front-left'    : self.fl_img = img
            elif cam_location == 'front-center': self.fc_img = img
            elif cam_location == 'front-right' : self.fr_img = img
            elif cam_location == 'rear-left'   : self.rl_img = img
            elif cam_location == 'rear-center' : self.rc_img = img
            elif cam_location == 'rear-right'  : self.rr_img = img


    def draw_bbox(self, viz_result, timestamp):
        
        pcl = self.ref_from_lidar.get_pc_info_from_lidar(str(timestamp))
        # Draw all reference objects
        ref_objs, timestamp_from_reference = self.ref_from_lidar.get_obj_info_from_lidar(str(timestamp))
        ref_world_boxes = []
        for ref_obj in ref_objs:
            ref = {}
            ref_world_bbox = Bbox([ref_obj['x'], ref_obj['y'], ref_obj['length'], ref_obj['width']])
            ref.update({'coord' : ref_world_bbox})  
            ref.update({'dist' : (ref_obj['lane_assignment'], ref_obj['lane_long_order'])}) 
            ref_world_boxes.append(ref)      
        
        # draw front ground truth
        front_gt_objs = viz_result.result['front']['obj']
        for location_of_cam, front_gt_obj in front_gt_objs.items():
            front_gt_bbox_obj = front_gt_obj['2d_ref_boxes']
            if not 'bbox' in front_gt_bbox_obj.keys(): continue
            front_gt_bboxes = front_gt_bbox_obj['bbox']
            for key, gt_bboxes in front_gt_bboxes.items():
                for gt_bbox in gt_bboxes:
                    cam_location = gt_bbox['cam_id']
                    obj_location = gt_bbox['lane_assignment']
                    coord2 = xywh_to_coord(gt_bbox['bbox'])
                    pt1 = (int(coord2[0]), int(coord2[1]))
                    pt2 = (int(coord2[2]), int(coord2[3]))
                    if cam_location == 'front_left': 
                        self.fl_img = draw_gt_bbox(self.fl_img, pt1, pt2, obj_location)
                    elif cam_location == 'front_center': 
                        self.fc_img = draw_gt_bbox(self.fc_img, pt1, pt2, obj_location)
                    elif cam_location == 'front_right':
                        self.fr_img = draw_gt_bbox(self.fr_img, pt1, pt2, obj_location)
        
        # draw rear ground truth
        rear_gt_objs = viz_result.result['rear']['obj']
        for location_of_cam, rear_gt_obj in rear_gt_objs.items():
            rear_gt_bbox_obj = rear_gt_obj['2d_ref_boxes']
            if not 'bbox' in rear_gt_bbox_obj.keys(): continue
            rear_gt_bboxes = rear_gt_bbox_obj['bbox']
            for key, gt_bboxes in rear_gt_bboxes.items():
                for gt_bbox in gt_bboxes:
                    cam_location = gt_bbox['cam_id']
                    obj_location = gt_bbox['lane_assignment']
                    coord2 = xywh_to_coord(gt_bbox['bbox'])
                    pt1 = (int(coord2[0]), int(coord2[1]))
                    pt2 = (int(coord2[2]), int(coord2[3]))
                    if cam_location == 'rear_left':
                        self.rl_img = draw_gt_bbox(self.rl_img, pt1, pt2, obj_location)
                    elif cam_location == 'rear_center':
                        self.rc_img = draw_gt_bbox(self.rc_img, pt1, pt2, obj_location)
                    elif cam_location == 'rear_right':
                        self.rr_img = draw_gt_bbox(self.rr_img, pt1, pt2, obj_location)
        
        # draw front detection result
        front_dt_objs = viz_result.result['front']['obj']
        for cam_location, front_dt_obj in front_dt_objs.items():
            for dt_bbox in front_dt_obj['2d_det_boxes']:
                coord2 = xywh_to_coord(dt_bbox)
                pt1 = (int(coord2[0]), int(coord2[1]))
                pt2 = (int(coord2[2]), int(coord2[3]))
                if cam_location == 'left':
                    cv2.rectangle(self.fl_img, pt1, pt2, (0, 255, 0), 1)
                elif cam_location == 'center':
                    cv2.rectangle(self.fc_img, pt1, pt2, (0, 255, 0), 1)
                elif cam_location == 'right':
                    cv2.rectangle(self.fr_img, pt1, pt2, (0, 255, 0), 1)
              
        # draw rear detection result
        rear_dt_objs = viz_result.result['rear']['obj']
        for cam_location, rear_dt_obj in rear_dt_objs.items():
            for dt_bbox in rear_dt_obj['2d_det_boxes']:
                coord2 = xywh_to_coord(dt_bbox)
                pt1 = (int(coord2[0]), int(coord2[1]))
                pt2 = (int(coord2[2]), int(coord2[3]))
                if cam_location == 'left':
                    cv2.rectangle(self.rl_img, pt1, pt2, (0, 255, 0), 1)
                elif cam_location == 'center':
                    cv2.rectangle(self.rc_img, pt1, pt2, (0, 255, 0), 1)
                elif cam_location == 'right':
                    cv2.rectangle(self.rr_img, pt1, pt2, (0, 255, 0), 1)               
    
        if len(pcl) != 0:
            validx = np.logical_and(pcl[:, 0] > XMIN, pcl[:, 0] < XMAX)
            validy = np.logical_and(pcl[:, 1] > YMIN, pcl[:, 1] < YMAX)
            validz = np.logical_and(pcl[:, 2] > -1, pcl[:, 2] < 2)
            valid = np.logical_and.reduce((validx, validy, validz))
            pcl_ = pcl[valid, :]
            self.ax.scatter(pcl_[:, 0], pcl_[:, 1], c='gray', s=1, cmap=None)
        
        # draw bbox from rosbag simulation
        front_dt_world_boxes = viz_result.result['front']['obj']['center']['3d_det_boxes']
        rear_dt_world_boxes = viz_result.result['rear']['obj']['center']['3d_det_boxes']
        self.ax = draw_dt_bbox_from_lidar(self.ax, front_dt_world_boxes, rear_dt_world_boxes)
        # draw lidar from lidar
        front_ref_world_boxes = viz_result.result['front']['obj']['center']['3d_ref_boxes_for_det']
        rear_ref_world_boxes = viz_result.result['rear']['obj']['center']['3d_ref_boxes_for_det']
        min_ref_bboxes = [front_ref_world_boxes] + [rear_ref_world_boxes]
        self.ax = draw_ref_bbox_from_lidar(self.ax, ref_world_boxes, min_ref_bboxes)
 

    def draw_freespace(self, freespace_gt, freespace_dt_pts, iou):
        self.freespace_acc = iou
        freespace_img = np.zeros(self.fc_img.shape, dtype=np.uint8)
        for row in range(self.fc_img.shape[0]):
            for col in range(self.fc_img.shape[1]):
                if freespace_gt[row, col] == 1:
                    freespace_img[row, col, 1] = 255
        self.fc_img = cv2.addWeighted(self.fc_img, 0.8, freespace_img, 0.2, 1)
        
        freespace_dt_img = np.zeros(self.fc_img.shape, dtype=np.uint8)
        cv2.fillPoly(freespace_dt_img, np.array([freespace_dt_pts.astype(int)]), (0, 0, 255))
        self.fc_img = cv2.addWeighted(self.fc_img, 0.8, freespace_dt_img, 0.2, 1)
    

    def draw_eval_result(self, eval_result):
        self.fl_lane_acc = eval_result.result['lane']['front_left']['Acc_World']
        self.fr_lane_acc = eval_result.result['lane']['front_right']['Acc_World'] 
        self.rl_lane_acc = eval_result.result['lane']['rear_left']['Acc_World']
        self.rr_lane_acc = eval_result.result['lane']['rear_right']['Acc_World']
        
        cv2.rectangle(self.fc_img, (0, 288), (512, 258), (0, 0, 0), -1)
        cv2.putText(self.fc_img, self.simulation_lane_timestamp, (15, 273), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))

        for direction_of_cam in ['front', 'rear']:
            ref_obj_left = eval_result.result['obj']['{}_left'.format(direction_of_cam)]['Ref']
            if ref_obj_left == True:
                obj_left = str(eval_result.result['obj']['{}_left'.format(direction_of_cam)]['Det'])
            else:
                obj_left = 'N/A'
            ref_obj_center =  eval_result.result['obj']['{}_center'.format(direction_of_cam)]['Ref']
            if ref_obj_center == True:
                obj_center  = str(eval_result.result['obj']['{}_center'.format(direction_of_cam)]['Det'])
            else:
                obj_center = 'N/A'
            ref_obj_right =  eval_result.result['obj']['{}_right'.format(direction_of_cam)]['Ref']
            if ref_obj_right == True:
                obj_right  = str(eval_result.result['obj']['{}_right'.format(direction_of_cam)]['Det'])
            else:
                obj_right = 'N/A'
            
            det_lane_left = str(eval_result.result['lane']['{}_left'.format(direction_of_cam)]['Det'])
            ref_lane_left = str(eval_result.result['lane']['{}_left'.format(direction_of_cam)]['Ref'])
            
            det_lane_right = str(eval_result.result['lane']['{}_right'.format(direction_of_cam)]['Det'])
            ref_lane_right = str(eval_result.result['lane']['{}_right'.format(direction_of_cam)]['Ref'])
            
            txt_lane      = 'lane  : left-{}, right-{}'.format(det_lane_left, det_lane_right)
            txt_obj       = 'bbox : left-{}, center-{} right-{}'.format(obj_left, obj_center, obj_right)
            txt_freespace = 'freesapce : {}'.format(self.freespace_acc)
            
            if obj_center != 'False' and obj_left != 'False' and obj_right != 'False':
                if direction_of_cam == 'front':
                    if ref_lane_left == 'False' and ref_lane_right == 'False':
                        self.fc_img = draw_result_on_center_img(self.fc_img, txt_lane, txt_obj, txt_freespace, 'N/A') 
                    elif det_lane_left == 'True' and det_lane_right == 'True':
                        self.fc_img = draw_result_on_center_img(self.fc_img, txt_lane, txt_obj, txt_freespace, 'GOOD')
                    else: 
                        self.fc_img = draw_result_on_center_img(self.fc_img, txt_lane, txt_obj, txt_freespace, 'BAD')
            
                else:
                    if ref_lane_left == 'False' and ref_lane_right == 'False':
                        self.rc_img = draw_result_on_center_img(self.rc_img, txt_lane, txt_obj, None, 'N/A')
                    elif det_lane_left == 'True' and det_lane_right == 'True':
                        self.rc_img = draw_result_on_center_img(self.rc_img, txt_lane, txt_obj, None, 'GOOD')
                    else:
                        self.rc_img = draw_result_on_center_img(self.rc_img, txt_lane, txt_obj, None, 'BAD')
            else:
                if direction_of_cam == 'front':
                    self.fc_img = draw_result_on_center_img(self.fc_img, txt_lane, txt_obj, txt_freespace, 'BAD')    
                else:
                    self.rc_img = draw_result_on_center_img(self.rc_img, txt_lane, txt_obj, None, 'BAD')
      

    def draw_lane(self, viz_result, timestamp):
        # draw front ground truth
        front_gt_lane = viz_result.result['front']['lane']['center']['2d_ref_lanes']
        self.fc_img = draw_gt_lane(front_gt_lane, self.fc_img, self.cfg.center_interest_lines)
        self.fl_img = draw_gt_lane(self.fl_ann[timestamp], self.fl_img, self.cfg.left_interest_lines)
        self.fr_img = draw_gt_lane(self.fr_ann[timestamp], self.fr_img, self.cfg.right_interest_lines)
        
        # draw rear ground truth
        rear_gt_lane = viz_result.result['rear']['lane']['center']['2d_ref_lanes']
        self.rc_img = draw_gt_lane(rear_gt_lane, self.rc_img, self.cfg.center_interest_lines)
        #self.rl_img = draw_gt_lane(self.rl_ann[timestamp], self.rl_img, self.cfg.left_interest_lines)
        #self.rr_img = draw_gt_lane(self.rr_ann[timestamp], self.rr_img, self.cfg.right_interest_lines)

        # draw lidar ground truth
        front_ref_lane = viz_result.result['front']['lane']['center']['3d_ref_lanes']
        for idx, ref_lane in front_ref_lane.items():
            if idx in self.cfg.center_interest_lines:
                color_format = 'b'
            else:
                color_format = 'k'
            self.ax.plot(ref_lane[:, 0], ref_lane[:, 1], color_format+'.')
            text_x = int(ref_lane.shape[0] / 2)
            self.ax.text(ref_lane[text_x, 0], ref_lane[text_x, 1], idx, color=color_format, fontsize=50) 

        # draw detection result
        front_lane    = viz_result.result['front']['lane']['center']['lanes']
        front_dt_lane = viz_result.result['front']['lane']['center']['det_lanes']
        self.fc_img, self.ax = draw_all_lane(self.ax, front_lane, self.fc_img)
        self.fc_img, self.ax = draw_dt_lane(self.ax, front_dt_lane, self.fc_img)
        
        rear_lane     = viz_result.result['rear']['lane']['center']['lanes']
        rear_dt_lane  = viz_result.result['rear']['lane']['center']['det_lanes']
        self.rc_img, self.ax = draw_all_lane(self.ax, rear_lane, self.rc_img)
        self.rc_img, self.ax = draw_dt_lane(self.ax, rear_dt_lane, self.rc_img)
     

    def show(self, eval_result, viz_result, timestamp, on_save=False, filename='TEST.png', title='TEST'):
        self.draw_bbox(viz_result, timestamp)
        self.draw_lane(viz_result, timestamp)
        self.draw_eval_result(eval_result)
        self.canvas.draw()
        s, (width, height) = self.canvas.print_to_buffer()
        # Option 2a: Convert to a NumPy array.
        img = np.fromstring(s, np.uint8).reshape((height, width, 4))
        fl_lane_acc_str = '[F]lane_acc_left(unit:m): {}'.format(self.fl_lane_acc)
        fr_lane_acc_str = '[F]lane_acc_right       : {}'.format(self.fr_lane_acc)
        rl_lane_acc_str = '[R]lane_acc_left        : {}'.format(self.rl_lane_acc)
        rr_lane_acc_str = '[R]lane_acc_right       : {}'.format(self.rr_lane_acc)
        lane_timestamp_from_reference = 'lidar timestamp : {}'.format(self.ref_lane_timestamp)
        cv2.putText(img, fl_lane_acc_str, (1000, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0))
        cv2.putText(img, fr_lane_acc_str, (1000, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0))
        cv2.putText(img, rl_lane_acc_str, (  30, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0))
        cv2.putText(img, rr_lane_acc_str, (  30, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0))
        cv2.putText(img, lane_timestamp_from_reference, (30, 258), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0))
        self.bird_eye_view = img[:, :, 0:3]
        row1        = np.hstack((self.fl_img, self.fc_img, self.fr_img))
        row1_concat = np.concatenate((self.fl_img, self.fc_img, self.fr_img), axis=1)
        row3        = np.hstack((self.rl_img, self.rc_img, self.rr_img))
        row3_concat = np.concatenate((self.rl_img, self.rc_img, self.rr_img), axis=1)
        image       = np.vstack((row1, self.bird_eye_view, row3))
        image       = np.concatenate((row1, self.bird_eye_view, row3))
           
        if on_save == True:
            out_folder = './out/images'
            if not os.path.isdir(out_folder):
                os.makedirs(out_folder)
            filepath = os.path.join(out_folder, filename)
               
            cv2.imwrite(filepath, image)
        
        #cv2.imshow(title, image)
        #cv2.waitKey(0)
        #if key in [27, ord('q')]:
        #    exit()
        
if __name__ == '__main__':
    print('Visualize Test')
    viz = Visualize()
    viz.show()
