import copy

class FordVizResult:
    def __init__(self):
        
        obj = {
            '2d_det_boxes' : [],
            '2d_ref_boxes' : {},
            '3d_det_boxes' : {}, 
            '3d_ref_boxes_for_det' : {}
        }

        lane = {
            'lanes'        : {},
            'det_lanes'    : {},
            '2d_ref_lanes' : {},
            '3d_ref_lanes' : {}
        }
        
        self.result = {
            'front' : {
                'obj' : {
                    'left'   : copy.deepcopy(obj),
                    'center' : copy.deepcopy(obj),
                    'right'  : copy.deepcopy(obj)
                },
                'lane' : {
                    'left'   : copy.deepcopy(lane),
                    'center' : copy.deepcopy(lane),
                    'right'  : copy.deepcopy(lane)
                },
            },
            'rear' : {
                'obj' : {
                    'left'   : copy.deepcopy(obj),
                    'center' : copy.deepcopy(obj),
                    'right'  : copy.deepcopy(obj)
                },
                'lane' : {
                    'left'   : copy.deepcopy(lane),
                    'center' : copy.deepcopy(lane),
                    'right'  : copy.deepcopy(lane)
                }
            }
        }
    
