import copy
import os

class FordEvalResult:
    def __init__(self, eval_result_directory):
        self.eval_path = eval_result_directory
        if os.path.isdir(self.eval_path) == False:
            os.mkdir(self.eval_path)
        
        obj = {
            'Ref_Area'  : None,
            'Det_Area'  : None,
            'Ref'       : False,
            'Det'       : False,
            'IoU'       : None,
            'Ref_XY'    : None,
            'Det_XY'    : None,
            'Err_XY'    : None,
            'Cam_ID'    : None, 
            'Ref_Class' : None,
            'Det_Class' : None,
            'T/F_Class' : False
        }

        lane = {
            'Ref'       : False,
            'Det'       : False,
            'Acc_Image' : None,
            'Acc_World' : None
        }

        self.result = {
            'obj' : {
                'front_center' : copy.deepcopy(obj),
                'front_left'   : copy.deepcopy(obj), 
                'front_right'  : copy.deepcopy(obj),
                'rear_center'  : copy.deepcopy(obj),
                'rear_left'    : copy.deepcopy(obj), 
                'rear_right'   : copy.deepcopy(obj)
            },
            'lane' : {
                'front_left'   : copy.deepcopy(lane),
                'front_right'  : copy.deepcopy(lane),
                'rear_left'    : copy.deepcopy(lane),
                'rear_right'   : copy.deepcopy(lane)
            },
            'freespace' : {
                'Acc'          : None
            },
            'location_info'    : None,
            'pitch_rads'       : 0.0
        }
    
