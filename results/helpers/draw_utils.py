import cv2
import numpy as np

def draw_result_on_center_img(img, lane_result, obj_result, freespace_result, status):
    new_img = img.copy()
    cv2.rectangle(new_img, (0, 0), (512, 60), (0, 0, 0), -1)
    cv2.putText(new_img, lane_result, (15, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))
    cv2.putText(new_img, obj_result, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))
    cv2.putText(new_img, freespace_result, (15, 45), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))
    if status == 'GOOD':
        cv2.putText(new_img, status, (400, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    else:
        cv2.putText(new_img, status, (400, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)   
    return new_img

def draw_dt_bbox_from_lidar(ax, front_dt_world_boxes, rear_dt_world_boxes):
    # coord : [x, y, w, h]
    # Draw bbox from simulation
    #############################
    # (x_1, y_1)-------(x_2, y_1)
    #     |                |
    # (x_1, y_2)-------(x_2, y_2)
    for idx, coord in front_dt_world_boxes.items():
        if idx == 'left':
            ax.text(coord.X, coord.Y, -1, color='r', fontsize=30)
        if idx == 'center':
            ax.text(coord.X, coord.Y,  0, color='r', fontsize=30)
        if idx == 'right':
            ax.text(coord.X, coord.Y,  1, color='r', fontsize=30)
        x_1 = coord.X
        #y_1 = coord[1] - coord[3] / 2
        #x_2 = coord[0] + coord[2]
        #y_2 = coord[1] + coord[3] / 2
        y_1 = coord.Y - 1
        x_2 = coord.X + 5 # use the real car lenght
        y_2 = coord.Y + 1
        
        x = [x_1, x_2, x_2, x_1, x_1]
        y = [y_1, y_1, y_2, y_2, y_1]
        ax.plot(x, y, 'r')
    
    for idx, coord in rear_dt_world_boxes.items():
        if idx == 'left':
            ax.text(coord.X, coord.Y, -1, color='r', fontsize=30)
        if idx == 'center':
            ax.text(coord.X, coord.Y,  0, color='r', fontsize=30)
        if idx == 'right':
            ax.text(coord.X, coord.Y,  1, color='r', fontsize=30)
        #x_1 = coord[0] - coord[2]
        #y_1 = coord[1] - coord[3] / 2
        x_1 = coord.X - 5
        y_1 = coord.Y - 1
        x_2 = coord.X
        #y_2 = coord[1] + coord[3] / 2
        y_2 = coord.Y + 1
        
        x = [x_1, x_2, x_2, x_1, x_1]
        y = [y_1, y_1, y_2, y_2, y_1]
        ax.plot(x, y, 'r')

    return ax


def draw_ref_bbox_from_lidar(ax, ref_bboxes, min_ref_bboxes):
    for ref_bbox in ref_bboxes:
        accuracy_txt = '[{}]'.format(ref_bbox['dist'])
        if ref_bbox['dist'][0] == None:
            accuracy_txt = 'None'
        x_1 = ref_bbox['coord'].X - ref_bbox['coord'].W / 2
        y_1 = ref_bbox['coord'].Y - ref_bbox['coord'].H / 2
        x_2 = ref_bbox['coord'].X + ref_bbox['coord'].W / 2
        y_2 = ref_bbox['coord'].Y + ref_bbox['coord'].H / 2
        ############################
        # (x_1, y_1)--------(x_2, y_1)
        #      |
        # (x_1, y_2)--------(x_2, y_2)
        ax.text(x_1, y_1, accuracy_txt, color='b', fontsize=21)
        x = [x_1, x_2, x_2, x_1, x_1]
        y = [y_1, y_1, y_2, y_2, y_1]
        ax.plot(x, y, 'b')
    
    for bbox_obj in min_ref_bboxes:
        for key, bbox in bbox_obj.items():
            accuracy_txt = bbox['dist_xy']
            x_1 = bbox['coord'].X - bbox['coord'].W / 2
            y_1 = bbox['coord'].Y - bbox['coord'].H / 2
            x_2 = bbox['coord'].X + bbox['coord'].W / 2
            y_2 = bbox['coord'].Y + bbox['coord'].H / 2
            
            ax.text(x_1, y_1 - 1, accuracy_txt, color='r', fontsize=28, weight='bold')
 
            ############################
            # (x_1, y_1)--------(x_2, y_1)
            #      |
            # (x_1, y_2)--------(x_2, y_2)
            x = [x_1, x_2, x_2, x_1, x_1]
            y = [y_1, y_1, y_2, y_2, y_1]
            ax.plot(x, y, 'b')

    return ax


def draw_gt_bbox(img, pt1, pt2, obj_loc):
    new_img = img.copy()
    cv2.rectangle(new_img, pt1, pt2, (0, 0, 255), 1)
    if obj_loc == 'left':
        cv2.putText(new_img, '-1', pt1, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255)) 
    elif obj_loc == 'center':
        cv2.putText(new_img, '0', pt1, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
    elif obj_loc == 'right':
        cv2.putText(new_img, '1', pt1, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
    
    return new_img


def draw_gt_lane(gt_lines, img, relation_ids):
    new_img = img.copy()
    for relation_id, gt in gt_lines.items():
        coords = np.array(gt['coords'])
        if relation_id in relation_ids:
            # change the relation_id based the direction of cam
            if gt['direction_of_cam'] == 'front':
                relation_id = str(int(relation_id) * -1)
            line_img = np.ones(new_img.shape, np.uint8) * 128
            cv2.putText(new_img, relation_id, tuple(coords[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
            for idx in range(0, coords.shape[0]-1):
                cv2.line(line_img, tuple(coords[idx]), tuple(coords[idx+1]), (0, 0, 255), 5)
            new_img = cv2.addWeighted(new_img, 0.7, line_img, 0.3, 0)
    
    return new_img


def draw_all_lane(ax, dt_lines, img):
    new_img = img.copy()
    for key, dt in dt_lines.items():
        # coordinate on image
        coords = np.array([dt['y_px'], dt['x_px']]).astype(int)
        coords = coords.transpose()
        coords[:, [0, 1]] = coords[:, [1, 0]]
        text_loc = int(coords.shape[0] / 2)
        #cv2.putText(new_img, rmse_txt,  tuple(coords[text_loc].tolist()), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
        for idx in range(0, coords.shape[0]-1):
            cv2.line(new_img, tuple(coords[idx]), tuple(coords[idx+1]), (125, 125, 0), 2)
        
        # coordinate on bird's eye view
        c = [dt['c0'], dt['c1'], dt['c2'], dt['c3']]
        start_x = dt['view_range_start_m']
        end_x = dt['view_range_end_m']
        x = np.array(range(int(start_x), int(end_x)))
        y = c[3] * x ** 3 + c[2] * x ** 2 + c[1] * x + c[0]
        text_y = c[3] * (start_x/2) ** 3 + c[2] * (start_x/2) ** 2 + c[1] * (start_x/2) + c[0]
        if dt['camera_name'] == 'front_center':
            key = str(int(key) * -1)
        ax.text(int(start_x / 2), int(text_y), str(dt['location']), color='g', fontsize=50) 
        ax.plot(x, y, 'g')

    return new_img, ax
 

def draw_dt_lane(ax, dt_lines, img):
    new_img = img.copy()
    for key, dt in dt_lines.items():
        # coordinate on image
        coords = np.array([dt['y_px'], dt['x_px']]).astype(int)
        coords = coords.transpose()
        coords[:, [0, 1]] = coords[:, [1, 0]]
        rmse_txt = '{0:.2f}'.format(dt['rmse'])
        text_loc = int(coords.shape[0] / 2)
        #cv2.putText(new_img, rmse_txt,  tuple(coords[text_loc].tolist()), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
        for idx in range(0, coords.shape[0]-1):
            cv2.line(new_img, tuple(coords[idx]), tuple(coords[idx+1]), (255, 0, 0), 2)

        # coordinate on bird's eye view
        c = [dt['c0'], dt['c1'], dt['c2'], dt['c3']]
        start_x = dt['view_range_start_m']
        end_x = dt['view_range_end_m']
        x = np.array(range(int(start_x), int(end_x)))
        y = c[3] * x ** 3 + c[2] * x ** 2 + c[1] * x + c[0]
        text_y = c[3] * (start_x/2) ** 3 + c[2] * (start_x/2) ** 2 + c[1] * (start_x/2) + c[0]
        if dt['camera_name'] == 'front_center':
            key = str(int(key) * -1)
        ax.text(int(start_x / 2), int(text_y), str(key), color='r', fontsize=50) 
        ax.plot(x, y, 'r')
    return new_img, ax
        
