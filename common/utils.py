import numpy as np
import os
import sys

from PIL import Image, ImageDraw

class Bbox():
    def __init__(self, coord):
        self.X = coord[0]
        self.Y = coord[1]
        self.W = coord[2]
        self.H = coord[3]

def bbox_iou(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    
    # compute the area of intersection rectangle
    interArea = abs(max((xB - xA, 0)) * max((yB - yA), 0))
    if interArea == 0:
        return 0
    
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = abs((boxA[2] - boxA[0]) * (boxA[3] - boxA[1]))
    boxBArea = abs((boxB[2] - boxB[0]) * (boxB[3] - boxB[1]))
    
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)
    
    # return the intersection over union value
    return iou

def draw_box(image, box, color):
    image = Image.fromarray(np.uint8(image))
    draw = ImageDraw.Draw(image, 'RGBA')

    xmin, ymin, xmax, ymax = box
    (left, right, top, bottom) = (xmin, xmax, ymin, ymax)
    # draw bounding box
    draw.line([(left, top), (left, bottom), (right, bottom), (right, top), (left, top)],
              width=2, fill=color)

    return np.asarray(image)

def coordinates_to_pts(coordinates):
    # [[x1, y1], [x2, y2]]
    pts = np.array([(coord['x'], coord['y']) for coord in coordinates])
    pts = pts.round().astype(np.int32)
    return pts

def find_the_closest_filename_from_timestamp(timestamp, in_files):
    min = sys.maxsize
    closest_timestamp = None
    
    for file in in_files:
        timestamp_from_file = os.path.basename(file)
        msec_from_file = int(timestamp_from_file.split('_')[0]) * 1000
        nsec_from_file = int(timestamp_from_file.split('_')[1][0:3])
        if len(timestamp.split('_')) == 2:
            temp = abs(int(nsec_from_file[0:3]) - int(timestamp.split('_')[1][0:3]))
        else:
            timestamp_from_file = msec_from_file + nsec_from_file
            temp = abs(timestamp_from_file - int(timestamp))
            #temp = abs(int(nsec_from_file[0:3]) - int(timestamp[10:13]))
        
        if temp < min:
            min = temp
            closest_timestamp_filename = file
    
    return closest_timestamp_filename

def xywh_to_coord(box, src_img_size=(1, 1), dst_img_size=(1, 1)):
    x1 = box[0] / src_img_size[0] * dst_img_size[0]
    y1 = box[1] / src_img_size[1] * dst_img_size[1]
    x2 = (box[0] + box[2]) / src_img_size[0] * dst_img_size[0]
    y2 = (box[1] + box[3]) / src_img_size[1] * dst_img_size[1]
    
    return [x1, y1, x2, y2]


