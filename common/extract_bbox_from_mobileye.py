import cv2
import glob
import json
import onnx
import onnxruntime as ort
import numpy as np
import os
import yaml

from PIL import Image, ImageDraw
from tqdm import tqdm

# need to compile from cython folder
import camera_model

from utils import draw_box

# relate to the camera model
FRONT_CENTER_CAMERA = 0

class ExtractBoxFromMobileye:
    def __init__(self, cfg, model_dir='/mnt/common/vision/model_archive/model-2019-12-27-torch-horizon/horizon_estimation_augmented.onnx'):
        self.ort_session = ort.InferenceSession(model_dir)
        # load horizon onnx model 
        model = onnx.load(model_dir)
        inputs = [node.name for node in model.graph.input]
        output = [node.name for node in model.graph.output]
        print('INPUT ::', inputs[0], 'OUTPUT ::', output[0])

        img_size = cfg.input_img_size

        img_w = img_size[0]
        img_h = img_size[1]
        
        if (img_w/2) < img_h:
            calib_img_size = img_size
        else:
            calib_img_size = [img_w, img_h]
        print('[{}]_IMG SIZE FOR CALIBRATION :: {}'.format(cfg.vehicle_name, calib_img_size))
        # need to figure out the shared_ptr
        self.camera_model = camera_model.PyCameraModelList(0) # fix me..
        self.camera_model.initialize(cfg.calibraion_yaml_file.encode(),
                                     cfg.vehicle_name.encode(),
                                     calib_img_size,
                                     [FRONT_CENTER_CAMERA])

    def get_bbox_with_mobileye_coord(self, img, w_coord):
        img_w = img.shape[1]
        img_h = img.shape[0]
        
        if (img_w/2) < img_h:
            padding = int((img_h - (img_w / 2)) / 2)
            crop_img = img[padding:padding + int(img_w / 2), :, :]
        else:
            padding = int(((img_w / 2) - img_h) / 2)
            crop_img = np.pad(img, ((padding, int(img_w / 2) - padding - img_h), (0, 0), (0, 0)), 'constant')
        
        horizon_img = cv2.resize(crop_img, (256, 128), interpolation=cv2.INTER_CUBIC)
        horizon_img = np.transpose(horizon_img, (2, 0, 1)).astype(np.float32)
        horizon_img = np.expand_dims(horizon_img, axis=0)
        
        outputs = self.ort_session.run(None, input_feed={self.ort_session.get_inputs()[0].name : horizon_img})[0]
        
        if (img_w/2) < img_h:
            h = padding + float(outputs[0][0])*crop_img.shape[1]/2/128
            mobileye_bbox = self.camera_model.get_coord_from_world(0, h, w_coord[0], w_coord[1], w_coord[2])
            box = np.array([mobileye_bbox[0], mobileye_bbox[1], mobileye_bbox[2], mobileye_bbox[3]])
        else:
            h = float(outputs[0][0])*crop_img.shape[1]/2/128 - padding 
            mobileye_bbox = self.camera_model.get_coord_from_world(0, h, w_coord[0], w_coord[1], w_coord[2])
            box = np.array([mobileye_bbox[0], mobileye_bbox[1], mobileye_bbox[2], mobileye_bbox[3]])
            #box = np.array([mobileye_bbox[0] / 960 * 1920, mobileye_bbox[1], mobileye_bbox[2] / 960 * 1920, mobileye_bbox[3]])
            #img = cv2.line(img, (0, int(h)), (img_w, int(h)), (0, 255, 0), 1)
            #cv2.imshow('TEST', img)
            #cv2.waitKey(0)

        return box
              
    def draw_box(self, img, box):
        img = draw_box(img, box, (0, 255, 0, 90))
        return img

# main for test
if __name__ == '__main__':
    from configuration.config import (Config)
    
    config_file = '../examples/mobileye_test_setting.json'
    cfg = Config(config_file)
    
    img = cv2.imread('../resources/test_1920_540.png')
    
    extract_box_from_mobileye = ExtractBoxFromMobileye(cfg)
    
    '''
    "position_y_m": 3.6875,
    "range_m": 56.0,
    "width_m": 1.7000000476837158
    '''
    box = extract_box_from_mobileye.get_bbox_with_mobileye_coord(img, [1.7000000476837158, 3.6875, 56.0])
    test_from_coor = extract_box_from_mobileye.draw_box(img, box)
    test = test_from_coor
    cv2.imshow('DEBUG', test)
    cv2.waitKey(0)
