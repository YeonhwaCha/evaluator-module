from scipy import ndimage

import cv2
import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import shapely.geometry as geom
import sys

from ref_from_lidar import (RefFromLidar)
from results.ford_eval import (FordEvalResult)

# heuristic definition
det_threshold = 6.4
min_x_axis = 80

#############################################################################
# Reference : https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
def shortest_distance(x1, y1, a, b, c):
    d = abs((a * x1 + b * y1 + c)) / (math.sqrt(a * a + b * b))
    return d

class MetricForLane:
    def __init__(self, cfg, gt_src_path, simulation_result):
        with open(gt_src_path, 'r') as f:
            gt_from_json = json.load(f)
        self.annotations = gt_from_json['annotations']['center']
        self.simulation = simulation_result
        self.ref_from_lidar = RefFromLidar(cfg)
        self.tr_im_size = cfg.train_img_size
    
       
    def evaluation_for_single_image(self, eval_result, viz_result, visualize, relation_ids, timestamp, direction_of_cam):
        def create_gt_image(coords, im_size):
            gt_image = np.ones(im_size, np.uint8) * 255
            coords = coords.astype(int)
            for idx in range(0, coords.shape[0]-1):
                cv2.line(gt_image, tuple(coords[idx]), tuple(coords[idx+1]), 0, 1)
            return gt_image
        
        def calc_rmse_for_detection(gt_img, pred_coords):
            gt_img[pred_coords[:, 0], pred_coords[:, 1]] = 1
            dist_map, inds = ndimage.distance_transform_edt(gt_img, return_indices=True)
            num_of_pred_coords = len(pred_coords)
            true_coords = np.where(gt_img==0)
            num_of_true_coords = len(true_coords[0])
            rmse = np.sqrt(dist_map[pred_coords[:, 0], pred_coords[:, 1]].mean())
            if num_of_true_coords < num_of_pred_coords:
                rmse = rmse * num_of_true_coords / num_of_pred_coords
        
            return rmse

        def calc_rmse_for_accuracy(coef, start, end, ref_pts, direction_of_cam):
            accuracy = sys.maxsize
            c = coef
            sum_of_dist = 0
            num_of_pts = 0
            s_ref = [0, 0]
            e_ref = [0, 0]

            for idx, pt in enumerate(ref_pts):
                # pass through the points which is out of detection line
                if pt[0] < start or pt[0] > end or pt[0] > 80: continue
                if direction_of_cam == 'front': 
                    if pt[0] < 0: continue
                else: 
                    if pt[0] > 0: continue
                if num_of_pts == 0: s_ref = pt
             
                x = pt[0]
                detect_y = c[3] * x ** 3 + c[2] * x ** 2 + c[1] * x + c[0]
                dist = np.sqrt(pow(pt[1] - detect_y, 2))
                sum_of_dist = sum_of_dist + dist
                num_of_pts = num_of_pts + 1
                e_ref = pt

            # range of reference / range of detection > 50
            square_ref_x = np.square(s_ref[0] - e_ref[0])
            square_ref_y = np.square(s_ref[1] - e_ref[1])
            range_of_ref = np.sqrt(square_ref_x + square_ref_y)
            end = np.minimum(end, min_x_axis)
            square_det_x = np.square(end - start)
            e_y = c[3] * end   ** 3 + c[2] * end   ** 2 + c[1] * end   + c[0]
            s_y = c[3] * start ** 3 + c[2] * start ** 2 + c[1] * start + c[0]
            square_det_y = np.square(e_y - s_y)
            range_of_det = np.sqrt(square_det_x + square_det_y)
            
            if num_of_pts > 10 and range_of_det > 0:
                if range_of_ref / range_of_det > 0.5 or range_of_ref > 40:
                    accuracy = sum_of_dist / num_of_pts
            
            return accuracy
        
        timestamp_from_simulation = None
        timestamp_from_reference = None
        obj_viz_result = viz_result.result[direction_of_cam]['lane']['center']
        for gt_relation_id, gt in self.annotations[timestamp].items():
            for relation_id in relation_ids:
                if relation_id == gt_relation_id:
                    if direction_of_cam == 'front':
                        if relation_id == '-1':
                            assignment = '{}_{}'.format(direction_of_cam, 'left')
                        else:
                            assignment = '{}_{}'.format(direction_of_cam, 'right')
                    else:
                        # rear camera need to be mirrored between left and right camera
                        if relation_id == '-1':
                            assignment = '{}_{}'.format(direction_of_cam, 'right')
                        else:
                            assignment = '{}_{}'.format(direction_of_cam, 'left')
                    
                    obj_result = eval_result.result['lane'][assignment]
                    pitch_rads = eval_result.result['pitch_rads']
                    #obj_viz_result = viz_result.result[direction_of_cam]['lane']['center']

                    obj_result['Ref'] = True # Image Annotation Exist
                    # GT 
                    gt_coords = np.array(gt['coords'])
                    dist_array = create_gt_image(gt_coords, self.tr_im_size)
                    # RMSE
                    min_rmse = sys.maxsize
                    detection = False
                    min_dt = {}
                    #try:
                    lanes, timestamp_from_simulation = self.simulation.get_lane_info_from_simulation(str(timestamp), direction_of_cam)
                    
                    for dt in lanes:
                        if dt['pitch_rads'] != 0.0 and pitch_rads == 0.0: 
                            eval_result.result['pitch_rads'] = dt['pitch_rads']
                        
                        idx = dt['location']
                        obj_viz_result['lanes'].update({str(idx): dt})
                        dt_coords = np.array([dt['y_px'], dt['x_px']]).astype(int)
                        dt_coords = np.transpose(dt_coords)
                        rmse = calc_rmse_for_detection(dist_array, dt_coords)
                        if rmse < min_rmse:
                            min_rmse = rmse
                            min_dt   = dt
                            min_dt_coords = dt_coords
                            location = dt['location']
                    
                    min_dt.update({'rmse' : min_rmse})
                    if min_rmse < det_threshold:
                        obj_result['Acc_Image'] = min_rmse
                        obj_viz_result['det_lanes'].update({relation_id: min_dt})
                        obj_result['Det'] = True
                        ref_lane, timestamp_from_reference = self.ref_from_lidar.get_lane_info_from_lidar(str(timestamp))
                        line_coef = [min_dt['c0'], min_dt['c1'], min_dt['c2'], min_dt['c3']]
                        start_x = min_dt['view_range_start_m']
                        end_x = min_dt['view_range_end_m']
                        min_ref_rmse = sys.maxsize
                        for key, item in ref_lane.items():
                            if key == relation_id:
                                if direction_of_cam == 'front':
                                    key = str(int(key) * -1)
                                if key in ref_lane.keys():
                                    ref_lane_pts = ref_lane[key]
                                    obj_viz_result['3d_ref_lanes'].update({key : ref_lane_pts})
                                    ref_rmse = calc_rmse_for_accuracy(line_coef, start_x, end_x, ref_lane_pts, direction_of_cam)
                                    if ref_rmse < min_ref_rmse:
                                        min_ref_rmse = ref_rmse
                        
                        if min_ref_rmse != sys.maxsize:
                            obj_result['Acc_World'] = min_ref_rmse
        
        # for visualization
        if direction_of_cam == 'front':
            obj_viz_result['2d_ref_lanes'] = self.annotations[timestamp]
            visualize.simulation_lane_timestamp = timestamp_from_simulation
            visualize.ref_lane_timestamp = timestamp_from_reference
        else:
            obj_viz_result['2d_ref_lanes'] = self.annotations[timestamp]
    

if __name__ == '__main__':
    json_filepath = '/home/yeonhwa/phantom/phantom-data-pipeline/evaluator-module/examples/ford_settings.json'
    gt_src_path = '/home/yeonhwa/phantom/phantom-data-pipeline/evaluator-module/examples/out/front_lane_gt.json'

    relation_ids = ['1', '-1']

    from configuration.config import (Config)
    from data_center import (DataCenter)
    
    cfg = Config(json_filepath)

