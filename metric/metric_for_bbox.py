import cv2
import json
import numpy as np
import os
import sys

from common.utils import (Bbox, 
                          bbox_iou, 
                          xywh_to_coord)
from helpers.bbox_utils import (get_coord_from_lut,
                                get_distance_between_bbox)
from ref_from_lidar import (RefFromLidar)
from simulation import (Simulation)

CLASSIFICATION = [
    'UNKNOWN', 'CAR', 'TRUCK', 'MOTORCYCLE', 'PEDESTRIAN',
    'OTHER', 'WHEEL', 'CART', 'STOPPER', 'POLE', 
    'SEDAN', 'SUV', 'VAN', 'TRAILER', 'BUS', 
    'GLARE', 'GENERAL', 'BICYCLE', 'BICYCLIST', 'MOTORCYCLIST'
]

# need to chage to ENUM
LANE_ASSIGNMENT_FOR_REF = {
    'front_left'   :  1,
    'front_center' :  0,
    'front_right'  : -1,
    'rear_left'    :  1,
    'rear_center'  :  0,
    'rear_right'   : -1
 }

class MetricForObj:
    def __init__(self, cfg, gt_src_path, simulation_result):
        with open(gt_src_path, 'r') as f:
            gt_from_json = json.load(f)
        self.annotations = gt_from_json['annotations']
        self.cfg = cfg
        self.simulation = simulation_result
        self.ref_from_lidar = RefFromLidar(cfg)      

    def evaluation_for_single_image(self, eval_result, viz_result, visualize, timestamp, direction_of_cam):
        objects, timestamp_from_simulation = self.simulation.get_obj_info_from_simulation(str(timestamp), direction_of_cam)
        ########################################################################
        # self.annotations[timestampe]['bbox']
        # you can get bboxes annotation based on timestamp
        try:
            for key_of_loc, ann_bboxes in self.annotations[timestamp]['bbox'].items():
                dt_bbox = []
                for gt in ann_bboxes:
                    assignment = '{}_{}'.format(direction_of_cam, gt['lane_assignment'])     
                       
                    obj_result = eval_result.result['obj'][assignment]
                    obj_result_viz = viz_result.result[direction_of_cam]['obj']
                    
                    if gt['category_id'] != 'BG': 
                        obj_result['Ref'] = True
                        obj_result['Ref_Area'] = gt['bbox'][2] * gt['bbox'][3]
                    max_iou = 0
                    min_ref = {}
                    for dt in objects:
                        camera_name = dt['camera_name']
                        dt_bbox = [dt['x_px'], dt['y_px'], dt['width_px'], dt['height_px']]
                        if 'crop' in camera_name:
                            # [This is temporary code..]
                            # it should be changed in later. 
                            # w_roi and c_roi could be changed on the different situation. 
                            # That's why we are supposed to use the Look-Up Table. 
                            # But Truck is huge and cropped image gives us the minus number for the truck. 
                            # this is a hard coding and not good. 
                            if dt['x_px'] < 0 or dt['y_px'] < 0:
                                img_size  = [512, 288]
                                w_roi = [0, 90, 1920, 1080]
                                c_roi = [704, 486, 512, 288]

                                x_scale = img_size[0] / w_roi[2]
                                y_scale = img_size[1] / w_roi[3]
                                x1 = (dt['x_px'] + c_roi[0] - w_roi[0]) * x_scale
                                y1 = (dt['y_px'] + c_roi[1] - w_roi[1]) * y_scale
                                x2 = ((dt['x_px'] + dt['width_px']) + c_roi[0] - w_roi[0]) * x_scale
                                y2 = ((dt['y_px'] + dt['height_px']) + c_roi[1] - w_roi[1]) * y_scale
                                dt_bbox = [x1, y1, x2-x1, y2-y1]
                            else:
                                dt_bbox = get_coord_from_lut(self.cfg, dt_bbox, direction_of_cam)
                        iou = bbox_iou(xywh_to_coord(gt['bbox']), xywh_to_coord(dt_bbox))
                        
                        ref_objs, timestamp_from_reference = self.ref_from_lidar.get_obj_info_from_lidar(str(timestamp))
                        if not gt['cam_id'] in camera_name:
                            continue
                        if iou > max_iou:
                            max_iou = iou
                            det_world_box = Bbox([dt['position_x_m'], dt['position_y_m'], dt['length_m'], dt['width_m']])
                            obj_result['Det'] = True
                            obj_result['Det_Area'] = dt['width_px'] * dt['height_px']
                            min_dist = sys.maxsize
                            min_dist_xy = sys.maxsize
                            min_ref_xy = sys.maxsize
                            min_det_xy = sys.maxsize
                            # need sorting for ref_objs                       
                            for ref_obj in ref_objs:
                                if ref_obj['lane_assignment'] == None: 
                                    continue
                                key = '{}_{}'.format(direction_of_cam, gt['lane_assignment'])
                            
                                if ref_obj['lane_assignment'] != LANE_ASSIGNMENT_FOR_REF[key]: 
                                    continue
                                
                                if (direction_of_cam == 'front' and ref_obj['lane_long_order'] > 0) or \
                                        (direction_of_cam == 'rear' and ref_obj['lane_long_order'] < 0):
                                    ref_world_bbox = Bbox([ref_obj['x'], ref_obj['y'], ref_obj['length'], ref_obj['width']])
                                    
                                    dist, dist_xy, ref_xy, det_xy = get_distance_between_bbox(direction_of_cam, det_world_box, ref_world_bbox)
                                    # I need to change the architecture
                                    # This architecture is wholly bad!!! :(
                                    if dist < min_dist:
                                        ref_key = '{}, {}'.format(ref_obj['x'], ref_obj['y'])
                                        if ref_key in min_ref.keys(): 
                                            min_ref[ref_key].update({'coord' : ref_world_bbox})
                                            min_ref[ref_key].update({'dist' : dist})
                                            min_ref[ref_key].update({'dist_xy' : dist_xy})
                                            min_dist    = dist
                                            min_dist_xy = dist_xy
                                            min_ref_xy  = ref_xy
                                            min_det_xy  = det_xy
                                        elif not ref_key in min_ref.keys():
                                            key_list = list(min_ref.keys())
                                            if len(key_list) > 0:
                                                for k, v in min_ref.items():
                                                    if dist < v['dist']:
                                                        min_ref = {}
                                                        min_ref.update({ref_key : {}})
                                                        min_ref[ref_key].update({'coord' : ref_world_bbox})
                                                        min_ref[ref_key].update({'dist' : dist})
                                                        min_ref[ref_key].update({'dist_xy' : dist_xy})
                                                        min_dist    = dist
                                                        min_dist_xy = dist_xy
                                                        min_ref_xy  = ref_xy
                                                        min_det_xy  = det_xy
                                            else:   
                                                min_ref.update({ref_key : {}})
                                                min_ref[ref_key].update({'coord' : ref_world_bbox})
                                                min_ref[ref_key].update({'dist' : dist})
                                                min_ref[ref_key].update({'dist_xy' : dist_xy})
                                                min_dist    = dist
                                                min_dist_xy = dist_xy
                                                min_ref_xy  = ref_xy
                                                min_det_xy  = det_xy

                            if min_dist != sys.maxsize:
                                obj_result['Err_XY'] = min_dist_xy
                                obj_result['Ref_XY'] = min_ref_xy
                                obj_result['Det_XY'] = min_det_xy
                                min_ref_keys = min_ref.keys()
                                if len(min_ref_keys) > 0:
                                    min_ref_key = list(min_ref_keys)[0]
                                    if not min_ref_key in obj_result_viz[key_of_loc]['3d_ref_boxes_for_det'].keys():
                                        obj_result_viz[key_of_loc]['3d_ref_boxes_for_det'].update(min_ref)
                                    else:
                                        old_dist = obj_result_viz[key_of_loc]['3d_ref_boxes_for_det'][min_ref_key]['dist']
                                        if min_ref[min_ref_key]['dist'] < old_dist:
                                            obj_result_viz[key_of_loc]['3d_ref_boxes_for_det'][min_ref_key]['dist'] = min_ref[min_ref_key]['dist']
                                            obj_result_viz[key_of_loc]['3d_ref_boxes_for_det'][min_ref_key]['dist_xy'] = min_ref[min_ref_key]['dist_xy']
                                            obj_result_viz[key_of_loc]['3d_ref_boxes_for_det'][min_ref_key]['coord'] = min_ref[min_ref_key]['coord']
                            
                            obj_result['Cam_ID'] = camera_name
                            obj_result['Ref_Class'] = gt['category_id']
                            obj_result['Det_Class'] = CLASSIFICATION[dt['classification']]
                            obj_result['IoU'] = max_iou
                            if obj_result['Det_Class'].lower() in obj_result['Ref_Class'].lower():
                                obj_result['T/F_Class'] = True
                            obj_result_viz[key_of_loc]['2d_det_boxes'].append(dt_bbox)
                            obj_result_viz[key_of_loc]['3d_det_boxes'].update({gt['lane_assignment'] : det_world_box})
            
            # for visualization
            if direction_of_cam == 'front':
                obj_result_viz['center']['2d_ref_boxes'] = self.annotations[timestamp]
            else:
                obj_result_viz['center']['2d_ref_boxes'] = self.annotations[timestamp]
        except:
            print('bbox doesn not exist')
