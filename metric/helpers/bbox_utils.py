import numpy as np

def get_coord_from_lut(cfg, coord, direction_of_cam):
    xmin = coord[0] 
    ymin = coord[1] 
    xmax = xmin + coord[2]  
    ymax = ymin + coord[3]  
    new_coord = []
    
    if direction_of_cam == 'front': 
        cameraMatrix = cfg.front_center_crop_cam_matrix
    elif direction_of_cam == 'rear':    
        cameraMatrix = cfg.rear_center_crop_cam_matrix
    
    rows = cameraMatrix['rows']
    cols = cameraMatrix['cols'] 
    
    # need to be checked... look up table doesn't match with the previous version
    lut = np.array(cameraMatrix['data']).reshape(rows, cols, 2) 
    xmin, ymin = lut[int(ymin), int(xmin)]  
    xmax, ymax = lut[int(ymax), int(xmax)]  
    new_coord = [xmin, ymin, xmax-xmin, ymax-ymin]  
    
    return new_coord   

def get_distance_between_bbox(direction_of_cam, dt_bbox, ref_bbox):
    if direction_of_cam == 'front':
        ref_bbox_x = ref_bbox.X - ref_bbox.W / 2
        ref_bbox_y = ref_bbox.Y
    else:
        ref_bbox_x = ref_bbox.X + ref_bbox.W / 2
        ref_bbox_y = ref_bbox.Y
    
    dist = np.sqrt(pow((ref_bbox_x - dt_bbox.X), 2) + pow((ref_bbox_y - dt_bbox.Y), 2))
    dist_xy = (float('{0:.2f}'.format((ref_bbox_x - dt_bbox.X))), float('{0:.2f}'.format((ref_bbox_y - dt_bbox.Y)))) 
    ref_xy = (ref_bbox_x, ref_bbox_y)
    det_xy = (dt_bbox.X, dt_bbox.Y)

    return dist, dist_xy, ref_xy, det_xy
