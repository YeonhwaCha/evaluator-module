from scipy import ndimage

import cv2
import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import sys

from common.utils import coordinates_to_pts
from results.ford_eval import (FordEvalResult)
from simulation import (Simulation)

class MetricForFreespace:
    def __init__(self, cfg, src_path, center_ann_files, simulation_result):
        self.annotations = center_ann_files
        self.simulation = simulation_result
        self.src_path = src_path
        self.im_size = cfg.input_img_size
        self.tr_im_size = cfg.train_img_size

    def evaluation_for_single_image(self, eval_result, visualize, timestamp):
        ann_filename = self.annotations[timestamp]
        center_ann_filepath = os.path.join(self.src_path, 'phantom_annotations/phantom_{}'.format(ann_filename))
        if os.path.exists(center_ann_filepath):   
            # read a phantom annotation file
            with open(center_ann_filepath) as f:
                ann = json.loads(str(f.read()))
 
            annotation_entities = ann['annotation_entities']
            annotated_object_id = ann['annotated_object_id']
        else:
            annotation_entities = []
            annotated_object_id = None
        pts = []
        
        dt_pts, timestamp_from_simulation = self.simulation.get_freespace_info_from_simulation(str(timestamp))
        
        gt_freespace = np.zeros(self.tr_im_size, dtype=np.uint8)
        for obj in annotation_entities:
            if obj['annotation_type'] == 'polygon' and obj['label'] == 'free_space':
                coordinates = obj['coordinates']
                pts = coordinates_to_pts(coordinates)
                pts = pts / self.im_size * self.tr_im_size
                cv2.fillPoly(gt_freespace, np.array([pts.astype(int)]), 1)
        
        for obj in annotation_entities:
            if obj['annotation_type'] == 'polygon' and obj['label'] == 'free-space-hole':
                coordinates = obj['coordinates']
                hole_pts = coordinates_to_pts(coordinates)
                hole_pts = hole_pts / self.im_size * self.tr_im_size
                cv2.fillPoly(gt_freespace, np.array([hole_pts.astype(int)]), 0)
        
        if len(dt_pts) != 0:   
            dt_freespace = np.zeros(self.tr_im_size, dtype=np.uint8)
            cv2.fillPoly(dt_freespace, np.array([dt_pts.astype(int)]), 1)
        
            union = 0
            intersection = 0
            for col in range(0, self.tr_im_size[1]):
                gt_row = self.tr_im_size[0]
                dt_row = self.tr_im_size[0]
                for row in range(0, self.tr_im_size[0]):
                    if gt_freespace[row, col] == 1 and gt_row == self.tr_im_size[0]:
                        gt_row = row
                    if dt_freespace[row, col] == 1 and dt_row == self.tr_im_size[0]:
                        dt_row = row
                    if gt_row != self.tr_im_size[0] and dt_row != self.tr_im_size[0]:
                        break
                if gt_row > dt_row:
                    union = union + gt_row
                    intersection = intersection + dt_row
                else:
                    union = union + dt_row
                    intersection = intersection + gt_row
            
            iou = float(intersection / union)
            eval_result.result['freespace']['Acc'] = iou

            visualize.draw_freespace(gt_freespace, dt_pts, iou) 
        

if __name__ == '__main__':
    json_filepath = '/home/yeonhwa/phantom/phantom-data-pipeline/evaluator-module/examples/ford_settings.json'  
    
    from configuration.config import (Config)   
    from data_center import (DataCenter)                                                    
    
    cfg = Config(json_filepath) 
    
    data_center      = DataCenter() 
    data_center.load_dataset(cfg.master_json_path)     
    
    front_center_ann_files = data_center.front_center_ann_files
    
    metric_for_lane = MetricForFreespace(cfg, src.annotation_path, front_center_ann_files)
    metric_for_lane.evaluation_for_single_image()
